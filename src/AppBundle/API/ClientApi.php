<?php

namespace AppBundle\API;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientApi extends GuzzleClient
{

    public function __construct(array $dataToSend = [], array $config = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptionResolver($resolver);

        $options = $resolver->resolve($dataToSend);

        $client = new Client([
            "defaults" => [
                "headers" => [
                    "User-Agent" => sprintf("%s", $options["app_name"]),
                    "Content-type" => "application/json"
                ],
                "auth" => [$options["api_login"], $options["api_password"]],
            ],
            "base_url" => [
                $options["base_url"],
                []
            ]
        ]);

        $client->setDefaultOption('verify', false);

        $description = new Description([
            "name" => $options["app_name"],
            "description" => $options["description"],
            "operations" => [
                "getRequestedApprovals" => [
                    "httpMethod" => "GET",
                    "responseModel" => "jsonResponse",
                    "additionalParameters" => [
                        "location" => "query"
                    ]
                ],
                "getApprover" => [
                    "httpMethod" => "GET",
                    "responseModel" => "jsonResponse",
                    "additionalParameters" => [
                        "location" => "query"
                    ]
                ],
                "getOpenedBy" => [
                    "httpMethod" => "GET",
                    "responseModel" => "jsonResponse",
                    "additionalParameters" => [
                        "location" => "query"
                    ]
                ],
                "getApproval" => [
                    "httpMethod" => "GET",
                    "responseModel" => "jsonResponse",
                    "additionalParameters" => [
                        "location" => "query"
                    ]
                ],
                "SendAction" => [
                    "httpMethod" => "POST",
                    "responseModel" => "jsonResponse",
                    "parameters" => [
                        "instance" => [
                            "required" => false,
                            "location" => "json",
                        ]
                    ],
                    "additionalParameters" => [
                        "location" => "json"
                    ]
                ],
            ],
            "models" => [
                "jsonResponse" => [
                    "type" => "object",
                    "additionalProperties" => [
                        "location" => "json"
                    ]
                ]
            ]
        ]);

        parent::__construct($client, $description, $config);
    }

    protected function configureOptionResolver(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setRequired([
                'description',
                'base_url',
                'api_login',
                'api_password',
                'app_name',
            ]);
    }
}
