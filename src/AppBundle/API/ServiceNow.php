<?php

namespace AppBundle\API;

use AppBundle\Document\InteractionInstance;
use AppBundle\Services\InteractionInstanceService;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;

class ServiceNow
{

    private $odm;
    private $instanceService;

    public function __construct(ODM $odm, InteractionInstanceService $instanceService)
    {
        $this->odm = $odm;
        $this->instanceService = $instanceService;
    }

    public function getApplicationRepository()
    {
        return $this->odm->getRepository('AppBundle:Application');
    }

    public function getInteractionRepository()
    {
        return $this->odm->getRepository('AppBundle:InteractionDefinition');
    }

    public function getInstanceRepository()
    {
        return $this->odm->getRepository('AppBundle:InteractionInstance');
    }

    public function bindConfiguration($interactionDefinitionObject, $applicationObject)
    {
        return array(
            "description" => $interactionDefinitionObject['description'],
            "base_url" => $interactionDefinitionObject['callbackRestService'],
            "app_name" => $applicationObject['name'],
            "api_login" => $applicationObject['username'],
            "api_password" => $applicationObject['password'],
        );
    }

    public function getRequestedApprovals($configuration)
    {
        $clientApi = new ClientApi($configuration);
        $response = $clientApi->getRequestedApprovals();

        return $response['result'];
    }

    public function getApprover($config, $link)
    {
        $config["base_url"] = $link;
        $clientApi = new ClientApi($config);
        $response = $clientApi->getApprover();
        $result = $response['result'];

        return $result;
    }

    public function getApproval($config, $link)
    {
        $config["base_url"] = $link;
        $clientApi = new ClientApi($config);
        $response = $clientApi->getApproval();
        $result = $response['result'];

        return $result;
    }

    public function getOpenedBy($config, $link)
    {
        $config["base_url"] = $link;
        $clientApi = new ClientApi($config);
        $response = $clientApi->getOpenedBy();
        $result = $response['result'];

        return $result;
    }

    public function getLastRequestedApprovals()
    {
        $serviceNowApplication = $this->getApplicationRepository()->findOneBy(array('application.name' => 'service now'));
        $interactionDefinition = $this->getInteractionRepository()->findOneBy(array('interaction.application' => $serviceNowApplication->getId()));

        $applicationObject = $serviceNowApplication->getApplication();
        $interactionDefinitionObject = $interactionDefinition->getInteraction();

        $lastInstance = $this->getInstanceRepository()->getLastServiceNowApproval();

        $configuration = $this->bindConfiguration($interactionDefinitionObject, $applicationObject);

        $result = $this->getRequestedApprovals($configuration);

        // this variable is just to display in the console the number of approvals retrieved
        $count = 0;

        foreach ($result as $approval) {
            if ($lastInstance) {
                if ($lastInstance->getCreationTimestamp()->sec >= strtotime($approval["sys_created_on"]["value"])) {
                    break;
                }
            }

            if (isset($approval["approver"]["link"]) && isset($approval["sysapproval"]["link"])) {
                try {
                    $instanceObject = $this->createInstance($approval, $configuration, $interactionDefinition);
                    $instance = new InteractionInstance();
                    $instance->setInstance($instanceObject);
                    $instance->setCreationTimestamp(strtotime($approval["sys_created_on"]["value"]));
                    $this->instanceService->saveInteraction($instance);
                } catch (\Exception $e) {}
                $count++;
            }
        }

        return $count;
    }

    public function createInstance($approval, $config, $interactionDefinition)
    {
        $linkApprover = $approval["approver"]["link"];
        $linkApproval = $approval["sysapproval"]["link"];
        $approver = $this->getApprover($config, $linkApprover);
        $approvalInformations = $this->getApproval($config, $linkApproval);
        $openedBy = $this->getOpenedBy($config, $approvalInformations['opened_by']['link']);

        // Remove empty cells
        $pattern = "/<tr[^>]*><\\/tr[^>]*>/";
        $formatHtml = preg_replace($pattern, '', $approval["u_variables"]["value"]);
        $contentHtml = preg_replace("/<table/", '<table class="serviceNow"', $formatHtml);

        // FOR TESTS
        if (strtolower($approver['email']) === "gaetan.holfeltz@ubisoft.com") {
            $emailTo = $approver['email'];
        } else if(strtolower($approver['email']) === "younes.sebti@ubisoft.com") {
            $emailTo = $approver['email'];
        } else if(strtolower($approver['email']) === "alexandru.moise@ubisoft.com") {
            $emailTo = $approver['email'];
        } else {
            $emailTo = "nassim.afrete@ubisoft.com";
        }

        $instanceObject = array(
            'interactionDefinitionId' => $interactionDefinition->getId(),
            'body' => $contentHtml,
            'due_date' => $approval['due_date']['value'],
            'opened_at' => $approvalInformations['opened_at'],
            'short_description' => $approvalInformations['short_description'],
            'requested_for' => $approver['email'],
            'opened_by' => $openedBy['user_name'],
            'externalParams' => array(
                'userId' => $approver["employee_number"],
                'approvalNumber' => $approval["sysapproval"]["display_value"],
                'reference' => $approval["u_watermark"]["value"],
            ),
            'userEmail' => $approval["sys_created_by"]["value"],
            'email' => array(
                'subject' => 'Requested Item ' . $approval["sysapproval"]["display_value"] . ' requires your approval',
                'body' => $contentHtml,
                'to' => array(
                    $emailTo,
                ),
                'from' => $approval["sys_created_by"]["value"],
            ),
        );

        return $instanceObject;
    }

}