<?php

namespace AppBundle\CisException;

use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CisException extends Exception
{

    public static function InvalidJsonException($message)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            throw new BadRequestHttpException("HttpError : " . $message);
        }
        throw new Exception("Error : " . $message);
    }

    public static function NotFoundException($message)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            throw new NotFoundHttpException("HttpError : " . $message);
        }
        throw new Exception("Error : " . $message);
    }

    public static function UniquenessException($message)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            throw new BadRequestHttpException("HttpError : " . $message);
        }
        throw new Exception("Error : " . $message);
    }

    public static function InvalidObjectException($message)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            throw new BadRequestHttpException("HttpError : " . $message);
        }
        throw new Exception("Error : " . $message);
    }
}