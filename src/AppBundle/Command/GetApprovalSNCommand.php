<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class GetApprovalSNCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('cis:getApprovalsSN')->setDescription('Get requested approvals from service now.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbApprovals = $this->getContainer()->get('service_now')->getLastRequestedApprovals();

        $file = 'app/logs/emails.log';
        $action = 'a+';
        $handle = fopen($file, '' . $action . '');
        $outputFile = new StreamOutput($handle);

        $text = "success : " . $nbApprovals . " approval(s) retrieved";
        $outputFile->writeln('[' . date("j-m-Y g:i a") . '] approval(s) retrieved: ' . $nbApprovals);

        fclose($handle);
        $output->writeln($text);
    }
}
