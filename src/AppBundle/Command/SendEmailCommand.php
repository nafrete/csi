<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SendEmailCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('cis:sendEmail')->setDescription('Send Emails to Users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbEmailSent = $this->getContainer()->get('mail')->sendMailNotification();

        $file = 'app/logs/emails.log';
        $action = 'a+';
        $handle = fopen($file, '' . $action . '');
        $outputFile = new StreamOutput($handle);

        $text = "success : " . $nbEmailSent . " email(s) sent";
        $outputFile->writeln('[' . date("j-m-Y g:i a") . '] emails sent: ' . $nbEmailSent);

        fclose($handle);
        $output->writeln($text);
    }
}
