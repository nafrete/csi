<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SendEmailRabbitMQCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rabbitmq:sendEmail')
            ->setDescription('Send emails to RabbitMQ')
            ->addArgument('emails', InputArgument::REQUIRED, 'How many emails do you want to send?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbEmailSent = 0;
        $nbEmails = $input->getArgument('emails');

        for ($i = 0; $i < $nbEmails; $i++) {
            $this->getContainer()->get('rabbitMQ')->applicationSendMail();

            $nbEmailSent++;
            $text = "success : " . $nbEmailSent . " emails sent to rabbitMQ";
            $output->writeln($text);
        }

        $text = "success : " . $nbEmailSent . " emails sent to rabbitMQ";
        $output->writeln($text);
    }
}
