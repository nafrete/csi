<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SendInteractionRabbitMQCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rabbitmq:sendInteraction')
            ->setDescription('Send interactions to RabbitMQ')
            ->addArgument('instances', InputArgument::REQUIRED, 'How many instances do you want to send?')
            ->addArgument('email', InputArgument::REQUIRED, 'Email address to send approval requests');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbInteractionSent = 0;
        $nbInstances = $input->getArgument('instances');
        $email = $input->getArgument('email');

        for ($i = 0; $i < $nbInstances; $i++) {
            $this->getContainer()->get('rabbitMQ')->sendInteractionInstance($email);

            $nbInteractionSent++;
            $text = "success : " . $nbInteractionSent . " interactions sent to rabbitMQ";
            $output->writeln($text);
        }

        $text = "success : " . $nbInteractionSent . " interactions sent to rabbitMQ";
        $output->writeln($text);
    }
}
