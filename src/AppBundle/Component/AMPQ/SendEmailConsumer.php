<?php

namespace AppBundle\Component\AMPQ;

use MongoConnectionException;
use AppBundle\Services\MailService;
use AppBundle\CisException\CisException;
use PhpAmqpLib\Message\AMQPMessage;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;

class SendEmailConsumer implements ConsumerInterface
{

    private $mailService;

    public function __construct(MailService $mailer)
    {
        $this->mailService = $mailer;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $emailObject = json_decode($msg->body, true);
            $this->isJsonValid($emailObject);
            $this->mailService->validateEmailObject($emailObject['email']);
            $this->mailService->createEmail($emailObject['email']);
        } catch (MongoConnectionException $e) {
            exit;
        } catch (\Exception $e) {
        }
    }

    public function isJsonValid($emailObject)
    {
        if (!isset($emailObject['email'])) {
            CisException::InvalidJsonException('Invalid Json Format');
        }
    }
}
