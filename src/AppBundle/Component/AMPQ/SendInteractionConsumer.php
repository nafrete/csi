<?php

namespace AppBundle\Component\AMPQ;

use MongoConnectionException;
use AppBundle\CisException\CisException;
use PhpAmqpLib\Message\AMQPMessage;
use AppBundle\Document\InteractionInstance;
use AppBundle\Services\InteractionInstanceService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;

class SendInteractionConsumer implements ConsumerInterface
{

    private $interactionService;

    public function __construct(InteractionInstanceService $_interactionService)
    {
        $this->interactionService = $_interactionService;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $instanceObject = json_decode($msg->body, true);
            $this->isJsonValid($instanceObject);

            $instance = new InteractionInstance();
            $instance->setInstance($instanceObject['instance']);
            $this->interactionService->saveInteraction($instance);
        } catch (MongoConnectionException $e) {
            exit;
        } catch (\Exception $e) {
        }
    }

    public function isJsonValid($instanceObject)
    {
        if (!isset($instanceObject['instance'])) {
            CisException::InvalidJsonException('Invalid Json Format');
        }
    }
}
