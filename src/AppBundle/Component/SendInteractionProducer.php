<?php

namespace AppBundle\Component;

use OldSound\RabbitMqBundle\RabbitMq\Producer;

class SendInteractionProducer
{

    private $producer;

    public function __construct(Producer $sendMessageProducer)
    {
        $this->producer = $sendMessageProducer;
    }

    public function sendMessage($message)
    {
        $this->producer->publish($message);
    }
}
