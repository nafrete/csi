<?php

namespace AppBundle\Controller;

use AppBundle\Document\Application;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/{_locale}/application", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
 */
class ApplicationController extends BaseController implements BaseControllerInterface
{

    /**
     * @DI\Inject("application")
     */
    public $applicationService;

    /**
     * @DI\Inject("user")
     */
    public $userService;

    /**
     * @DI\Inject("security.token_storage")
     */
    private $securityContext;

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @Route("/", name="applications")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->templating->renderResponse('application/layout.html.twig', array(
            'applications' => $this->applicationService->getApplications(),
        ));
    }

    /**
     * @Route("/create", name="application_create")
     * @Method("POST")
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $user = $this->userService->checkIdentityUser($this->securityContext->getToken()->getUser());
        $this->isJsonValid($request, 'application');
        $application = $this->createApplicationDocument($request->request->get('application'), $user);
        $this->applicationService->saveApplication($application);

        return $this->getTemplate();
    }

    /**
     * @Route("/update/{id}", name="application_update")
     * @Method("PUT")
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function updateAction(Request $request, $id)
    {
        $this->isJsonValid($request, 'application');
        $application = $this->applicationService->getApplication($id);
        $application->setApplication($request->request->get('application'));
        $this->applicationService->updateApplication($application);

        return $this->getTemplate();
    }

    /**
     * @Route("/delete/{id}", name="application_delete")
     * @Method("DELETE")
     * @param $id
     * @return mixed
     */
    public function deleteAction($id)
    {
        $this->applicationService->deleteApplication($id);

        return $this->getTemplate();
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->templating->renderResponse('application/listApplications.html.twig', array(
            'applications' => $this->applicationService->getApplications(),
        ));
    }

    /**
     * @param $applicationObject
     * @param $user
     * @return Application
     */
    private function createApplicationDocument($applicationObject, $user)
    {
        $application = new Application();
        $application->setApplication($applicationObject);
        $application->setUser($user);

        return $application;
    }
}
