<?php

namespace AppBundle\Controller;

use AppBundle\CisException\CisException;


abstract class BaseController
{
    const JSON_HEADER_CHROME = 'application/json';
    const JSON_HEADER_FIREFOX = 'application/json; charset=UTF-8';

    /**
     * @param $request
     * @param $param
     * @throws \Exception
     */
    public function isJsonValid($request, $param)
    {
        $parameters = $request->request->get($param);
        $informations = (json_encode($parameters));
        if (!$this->isJSONResponse($request) || strlen($informations) === 0 || (!is_array($parameters) && json_decode($parameters) === null)) {
            CisException::InvalidJsonException('Invalid Json Format');
        }
    }

    /**
     * @param $request
     * @return bool
     */
    protected function isJSONResponse($request)
    {
        if ($request->headers->get('Content-Type') === self::JSON_HEADER_CHROME || $request->headers->get('Content-Type') === self::JSON_HEADER_FIREFOX) {
            return true;
        }

        return false;
    }
}