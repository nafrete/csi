<?php

namespace AppBundle\Controller;

interface BaseControllerInterface
{
    public function getTemplate();
}
