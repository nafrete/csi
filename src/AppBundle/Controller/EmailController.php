<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/{_locale}/email", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
 */
class EmailController
{

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @DI\Inject("mail")
     */
    private $mailService;

    /**
     * @Route("/", name="email_overview")
     * @Method("GET")
     */
    public function emailOverviewAction()
    {
        return $this->templating->renderResponse('email/index.html.twig');
    }

    /**
     * @Route("/template/send", name="email_service_now")
     * @Method("GET")
     */
    public function sendEmailToServiceNowAction()
    {
        $data = $this->mailService->sendTestMailNotification();
        return new Response($data);
    }
}
