<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Document\InteractionDefinition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/{_locale}/interactiondefinition", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
 */
class InteractionDefinitionController extends BaseController implements BaseControllerInterface
{

    /**
     * @DI\Inject("interaction.definition")
     */
    private $interactionDefinitionService;

    /**
     * @DI\Inject("application")
     */
    private $applicationService;

    /**
     * @DI\Inject("user")
     */
    public $userService;

    /**
     * @DI\Inject("security.token_storage")
     */
    private $securityContext;

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @Route("/", name="interactiondefinition")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->templating->renderResponse('interactionDefinition/layout.html.twig', array(
            'interactionsDefinitions' => $this->interactionDefinitionService->getInteractionsDefinitions(),
            'applications' => $this->applicationService->getApplicationsName(),
        ));
    }

    /**
     * @Route("/create", name="interactiondefinition_create")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $user = $this->userService->checkIdentityUser($this->securityContext->getToken()->getUser());
        $this->isJsonValid($request, 'interaction');
        $interaction = $this->createInteractionDocument($request->request->get('interaction'), $user);
        $this->interactionDefinitionService->saveInteractionDefinition($interaction);

        return $this->getTemplate();
    }

    /**
     * @Route("/update/{id}", name="interactiondefinition_update")
     * @Method("PUT")
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function updateAction(Request $request, $id)
    {
        $this->isJsonValid($request, 'interaction');
        $interaction = $this->interactionDefinitionService->getInteractionDefinition($id);
        $interaction->setInteraction($request->request->get('interaction'));
        $this->interactionDefinitionService->updateInteractionDefinition($interaction);

        return $this->getTemplate();
    }

    /**
     * @Route("/delete/{id}", name="interactiondefinition_delete")
     * @Method("DELETE")
     * @param $id
     * @return mixed
     */
    public function deleteAction($id)
    {
        $this->interactionDefinitionService->deleteInteractionDefinition($id);

        return $this->getTemplate();
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->templating->renderResponse('interactionDefinition/listInteractionsDefinitions.html.twig', array(
            'interactionsDefinitions' => $this->interactionDefinitionService->getInteractionsDefinitions(),
            'applications' => $this->applicationService->getApplicationsName(),
        ));
    }

    private function createInteractionDocument($interaction, $user)
    {
        $object = new InteractionDefinition();
        $object->setInteraction($interaction);
        $object->setUser($user);

        return $object;
    }
}
