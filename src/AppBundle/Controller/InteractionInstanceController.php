<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/{_locale}/interactioninstance", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
 */
class InteractionInstanceController
{

    /**
     * @DI\Inject("interaction.instance")
     */
    private $interactionInstanceService;

    /**
     * @DI\Inject("application")
     */
    private $applicationService;

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @DI\Inject("security.token_storage")
     */
    private $securityContext;

    /**
     * @Route("/", name="interactioninstance")
     */
    public function indexAction()
    {
        return $this->templating->renderResponse('interactionInstance/layout.html.twig');
    }

    /**
     * @Route("/update/{id}/{status}", name="interactioninstance_update")
     * @Method("PUT")
     * @param $id
     * @param $status
     * @return Response
     */
    public function updateAction($id, $status)
    {
        $instance = $this->interactionInstanceService->getInteractionInstanceByID($id);
        $instanceObject = $instance->getInstance();
        $instanceObject['status'] = $status;
        $currentUser = $this->securityContext->getToken()->getUser();
        $instanceObject['executed_by'] = $currentUser->getEmail();
        $instance->setInstance($instanceObject);
        $this->interactionInstanceService->updateInteractionInstance($instance);

        return new Response('success', 200, array('content-type' => 'application/json'));
    }

    /**
     * @Route("/{id}", name="interaction")
     * @Method("GET")
     * @param $id
     * @return
     */
    public function showAction($id)
    {
        $interactions = $this->interactionInstanceService->getInteractionsForResponse($id);
        $applicationName = $this->applicationService->getApplicationName($interactions['interactionDefinition']);

        return $this->templating->renderResponse('responses/interaction.html.twig', array(
            'interactionInstance' => $interactions['interactionInstance'],
            'interactionDefinition' => $interactions['interactionDefinition'],
            'application' => $applicationName,
        ));
    }

    /**
     * @Route("/{id}/response", name="interaction_userview")
     * @Method("GET")
     * @param $id
     * @return
     */
    public function showInstanceAction($id)
    {
        $interactions = $this->interactionInstanceService->getInteractionsForResponse($id);
        $applicationName = $this->applicationService->getApplicationName($interactions['interactionDefinition']);

        return $this->templating->renderResponse('responses/templateInstance.html.twig', array(
            'interactionInstance' => $interactions['interactionInstance'],
            'interactionDefinition' => $interactions['interactionDefinition'],
            'application' => $applicationName,
        ));
    }

    /**
     * @Route("/{id}/overview", name="interaction_overview")
     * @Method("GET")
     * @param $id
     * @return
     */
    public function getInstanceOverviewAction($id) {
        $interactions = $this->interactionInstanceService->getInteractionsForResponse($id);
        $applicationName = $this->applicationService->getApplicationName($interactions['interactionDefinition']);

        return $this->templating->renderResponse('interactionInstance/templateInstanceOverview.html.twig', array(
            'interactionInstance' => $interactions['interactionInstance'],
            'interactionDefinition' => $interactions['interactionDefinition'],
            'application' => $applicationName,
        ));
    }

    /**
     * @Route("/execute/{id}", name="interaction_response")
     * @Method("GET")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function executeAction(Request $request, $id)
    {
        $optionsValues = $request->query->get('options');
        $currentUser = $this->securityContext->getToken()->getUser();
        $result = $this->interactionInstanceService->executeInteraction($id, $optionsValues, $currentUser->getEmail());

        return new Response('{"message":"' . $result["message"] . '"}', 200, array('content-type' => 'application/json'));
    }

    /**
     * @Route("/{id}/actions", name="interactioninstance_actions")
     * @Method("GET")
     * @param $id
     * @return
     */
    public function getInteractionActionsAction($id)
    {
        $interactions = $this->interactionInstanceService->getInteractionInstance($id);

        return $this->templating->renderResponse('interactionInstance/actions.html.twig', array(
            'interactionInstance' => $interactions['interactionInstance'],
            'interactionDefinition' => $interactions['interactionDefinition'],
            'applications' => $this->applicationService->getApplicationsName(),
        ));
    }
}
