<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MainController
{

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @Route("/{_locale}", name="homepage", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->templating->renderResponse('homepage/homepage.html.twig');
    }
}
