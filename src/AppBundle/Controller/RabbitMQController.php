<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class RabbitMQController implements BaseControllerInterface
{

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @DI\Inject("rabbitMQ")
     */
    private $rabbitMQService;

    /**
     * @Route("/{_locale}/messages", name="messages", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->rabbitMQService->applicationSendMail();
        return $this->getTemplate();
    }

    public function getTemplate()
    {
        return $this->templating->renderResponse('queue/messages.html.twig');
    }
}
