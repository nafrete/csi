<?php

namespace AppBundle\Controller\Rest;

use AppBundle\Document\Application;
use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Controller\BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApplicationRestController extends BaseController
{

    /**
     * @DI\Inject("application")
     */
    private $applicationService;

    /**
     * Display all applications
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     */
    public function getApplicationsAction()
    {
        return $this->applicationService->getApplications();
    }

    /**
     * Display one application by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param $id
     * @return
     */
    public function getApplicationAction($id)
    {
        return $this->applicationService->getApplication($id);
    }

    /**
     * Display name of applications
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @return
     */
    public function getNameApplicationsAction()
    {
        return $this->applicationService->getApplicationsName();
    }

    /**
     * Post a new application
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\ApplicationType",
     *  output="AppBundle\Document\Application"
     * )
     * @param Request $request
     * @return Response
     */
    public function postApplicationAction(Request $request)
    {
        $this->isJsonValid($request, 'application');
        $application = $this->createApplicationDocument(json_decode($request->request->get('application'), true));
        $this->applicationService->saveApplication($application);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    /**
     * @param $applicationObject
     * @return Application
     */
    private function createApplicationDocument($applicationObject)
    {
        $application = new Application();
        $application->setApplication($applicationObject);

        return $application;
    }

    /**
     * Update an application
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\ApplicationType",
     *  output="AppBundle\Document\Application"
     * )
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function putApplicationAction(Request $request, $id)
    {
        $this->isJsonValid($request, 'application');
        $application = $this->applicationService->getApplication($id);
        $application->setApplication(json_decode($request->request->get('application'), true));
        $this->applicationService->updateApplication($application);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    /**
     * Delete one application by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *  }
     * )
     * @param $id
     * @return Response
     */
    public function deleteApplicationAction($id)
    {
        $this->applicationService->deleteApplication($id);

        return new Response('', 200, array('content-type' => 'application/json'));
    }
}
