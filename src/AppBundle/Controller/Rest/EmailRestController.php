<?php

namespace AppBundle\Controller\Rest;

use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class EmailRestController
{

    /**
     * @DI\Inject("mail")
     */
    private $mailService;

    /**
     * Display all emails
     *
     * @ApiDoc(
     *  resource=true,
     * parameters={
     *      {"name"="pageSize", "dataType"="integer", "required"=true, "description"="number of emails"}
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param Request $request
     * @return
     */
    public function getEmailsAction(Request $request)
    {
        $page = ($request->query->get('page') === null) ? 1 : intval($request->query->get('page'));
        $pageSize = ($request->query->get('pageSize') === null) ? 1 : intval($request->query->get('pageSize'));
        $sort = ($request->query->get('sort') === null) ? null : $request->query->get('sort');
        $filter = ($request->query->get('filter') === null) ? null : $request->query->get('filter');

        $parameters = array(
            "page" => $page,
            "pageSize" => $pageSize,
            "sort" => $sort,
            "filters" => $filter,
        );

        return $this->mailService->getEmails($parameters);
    }

    /**
     * Display all emails pending
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     */
    public function getEmailsPendingAction()
    {
        return $this->mailService->getEmailsPending();
    }

    /**
     * Display all emails sent
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     */
    public function getEmailsSentAction()
    {
        return $this->mailService->getEmailsSent();
    }
}
