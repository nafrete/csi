<?php

namespace AppBundle\Controller\Rest;

use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Controller\BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Document\InteractionDefinition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InteractionDefinitionRestController extends BaseController
{

    /**
     * @DI\Inject("interaction.definition")
     */
    private $interactionDefinitionService;

    /**
     * Display all interaction definition
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction definition is not found",
     *         401="Unauthorized access"
     *     }
     * )
     */
    public function getInteractionsDefinitionsAction()
    {
        return $this->interactionDefinitionService->getInteractionsDefinitions();
    }

    /**
     * Display one interaction definition by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction definition is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param $id
     * @return
     */
    public function getInteractionDefinitionAction($id)
    {
        return $this->interactionDefinitionService->getInteractionDefinition($id);
    }

    /**
     * Post a new interaction definition
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\InteractionDefinitionType",
     *  output="AppBundle\Document\InteractionDefinition"
     * )
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function postPublishInteractionDefinitionAction(Request $request)
    {
        $this->isJsonValid($request, 'interaction');
        $interaction = $this->createInteractionDocument(json_decode($request->request->get('interaction'), true));
        $this->interactionDefinitionService->saveInteractionDefinition($interaction);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    private function createInteractionDocument($interaction)
    {
        $object = new InteractionDefinition();
        $object->setInteraction($interaction);

        return $object;
    }

    /**
     * Update an interaction definition
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\InteractionDefinitionType",
     *  output="AppBundle\Document\InteractionDefinition"
     * )
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function putInteractionDefinitionAction(Request $request, $id)
    {
        $this->isJsonValid($request, 'interaction');
        $interaction = $this->interactionDefinitionService->getInteractionDefinition($id);
        $interaction->setInteraction(json_decode($request->request->get('interaction'), true));
        $this->interactionDefinitionService->updateInteractionDefinition($interaction);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    /**
     * Delete one interaction definition by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction definition is not found",
     *         401="Unauthorized access"
     *  }
     * )
     * @param $id
     * @return Response
     */
    public function deleteInteractionDefinitionAction($id)
    {
        $this->interactionDefinitionService->deleteInteractionDefinition($id);

        return new Response('', 200, array('content-type' => 'application/json'));
    }
}
