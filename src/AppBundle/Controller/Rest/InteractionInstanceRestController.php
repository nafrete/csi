<?php

namespace AppBundle\Controller\Rest;

use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Controller\BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Document\InteractionInstance;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InteractionInstanceRestController extends BaseController
{

    /**
     * @DI\Inject("interaction.instance")
     */
    private $interactionInstanceService;

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @DI\Inject("user")
     */
    public $userService;

    /**
     * @DI\Inject("application")
     */
    private $applicationService;

    /**
     * @DI\Inject("security.token_storage")
     */
    private $securityContext;

    /**
     * Display all interaction instance
     *
     * @ApiDoc(
     *  resource=true,
     * parameters={
     *      {"name"="pageSize", "dataType"="integer", "required"=true, "description"="number of emails"}
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction instance is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param Request $request
     * @return
     */
    public function getInteractionsInstancesAction(Request $request)
    {
        $page = ($request->query->get('page') === null) ? 1 : intval($request->query->get('page'));
        $pageSize = ($request->query->get('pageSize') === null) ? 1 : intval($request->query->get('pageSize'));
        $sort = ($request->query->get('sort') === null) ? null : $request->query->get('sort');
        $filter = ($request->query->get('filter') === null) ? null : $request->query->get('filter');

        $parameters = array(
            "page" => $page,
            "pageSize" => $pageSize,
            "sort" => $sort,
            "filters" => $filter,
        );

        return $this->interactionInstanceService->getInteractionsInstances($parameters);
    }

    /**
     * Display all interaction instance of the current user
     *
     * @ApiDoc(
     *  resource=true,
     * parameters={
     *      {"name"="pageSize", "dataType"="integer", "required"=true, "description"="number of emails"}
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction instance is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param Request $request
     * @return
     */
    public function getInteractionsInstancesUserAction(Request $request)
    {
        $page = ($request->query->get('page') === null) ? 1 : intval($request->query->get('page'));
        $pageSize = ($request->query->get('pageSize') === null) ? 1 : intval($request->query->get('pageSize'));
        $sort = ($request->query->get('sort') === null) ? null : $request->query->get('sort');
        $filter = ($request->query->get('filter') === null) ? null : $request->query->get('filter');

        $user = $this->userService->checkIdentityUser($this->securityContext->getToken()->getUser());
        $userEmail = ($user !== null) ? $user->getEmail() : null;

        $parameters = array(
            "userEmail" => $userEmail,
            "page" => $page,
            "pageSize" => $pageSize,
            "sort" => $sort,
            "filters" => $filter,
        );

        return $this->interactionInstanceService->getInteractionsInstancesForCurrentUser($parameters);
    }

    /**
     * Display one interaction instance by his Id
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the interaction instance is not found",
     *         401="Unauthorized access"
     *     }
     * )
     * @param $id
     * @return
     */
    public function getInteractionInstanceAction($id)
    {
        return $this->interactionInstanceService->getInteractionInstanceByID($id);
    }

    /**
     * Post a new interaction instance
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\InteractionInstanceType",
     *  output="AppBundle\Document\InteractionInstance"
     * )
     * @param Request $request
     * @return Response
     */
    public function postPublishInteractionInstanceAction(Request $request)
    {
        $this->isJsonValid($request, 'instance');
        $instance = $this->createInstance(json_decode($request->request->get('instance'), true));
        $this->interactionInstanceService->saveInteraction($instance);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    /**
     * Execute an approval request
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  }
     * )
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function getExecuteAction(Request $request, $id)
    {
        $optionsValues = $request->query->get('options');
        $currentUser = $this->securityContext->getToken()->getUser();
        $result = $this->interactionInstanceService->executeInteraction($id, $optionsValues, $currentUser->getEmail());

        return $result["message"];
    }

    /**
     * Get the template for a specific instance
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  }
     * )
     * @param $id
     * @return Response
     * @internal param Request $request
     */
    public function getInstanceViewAction($id)
    {
        $interactions = $this->interactionInstanceService->getInteractionsForResponse($id);
        $applicationName = $this->applicationService->getApplicationName($interactions['interactionDefinition']);

        return $this->templating->renderResponse('responses/templateInstance.html.twig', array(
            'interactionInstance' => $interactions['interactionInstance'],
            'interactionDefinition' => $interactions['interactionDefinition'],
            'application' => $applicationName,
        ));
    }

    /**
     * Update an interaction instance
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\InteractionInstanceType",
     *  output="AppBundle\Document\InteractionInstance"
     * )
     * @param $id
     * @param $status
     * @return Response
     * @throws \Exception
     */
    public function putInteractionInstanceAction($id, $status)
    {
        $instance = $this->interactionInstanceService->getInteractionInstanceByID($id);
        $instanceObject = $instance->getInstance();
        $instanceObject['status'] = $status;
        $instance->setInstance($instanceObject);
        $this->interactionInstanceService->updateInteractionInstance($instance);

        return new Response('', 200, array('content-type' => 'application/json'));
    }

    public function createInstance($instanceObject)
    {
        $instance = new InteractionInstance();
        $instance->setInstance($instanceObject);

        return $instance;
    }
}
