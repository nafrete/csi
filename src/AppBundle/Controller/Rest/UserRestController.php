<?php

namespace AppBundle\Controller\Rest;

use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserRestController
{

    /**
     * @DI\Inject("user")
     */
    private $userService;

    /**
     * Display all users
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the application is not found",
     *         401="Unauthorized access"
     *     }
     * )
     */
    public function getUsersAction()
    {
        return $this->userService->getUsers();
    }
}
