<?php

namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/{_locale}/users", requirements={"_locale" = "en|fr"}, defaults={"_locale" = "en"})
 */
class UserController
{

    /**
     * @DI\Inject("templating")
     */
    private $templating;

    /**
     * @Route("/", name="users")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->templating->renderResponse('user/layout.html.twig');
    }
}
