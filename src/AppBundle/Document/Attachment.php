<?php

namespace AppBundle\Document;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\AttachmentRepository", collection="Attachment")
 */
class Attachment
{

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field
     */
    private $name;

    /**
     * @MongoDB\String
     *
     */
    private $filename;

    /**
     * @MongoDB\String
     */
    private $mimeType;

    /**
     * @MongoDB\File
     */
    private $file;

    /**
     * @MongoDB\Field
     */
    private $uploadDate;

    /**
     * @MongoDB\Field
     */
    private $length;

    /**
     * @MongoDB\Field
     */
    private $chunkSize;

    /**
     * @MongoDB\Field
     */
    private $md5;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\EmailQueue", inversedBy="attachments")
     */
    private $emailQueue;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    public function setChunkSize($chunkSize)
    {
        $this->chunkSize = $chunkSize;

        return $this;
    }

    public function getMd5()
    {
        return $this->md5;
    }

    public function setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    public function getEmailQueue()
    {
        return $this->emailQueue;
    }

    public function setEmailQueue(EmailQueue $emailQueue)
    {
        $this->emailQueue = $emailQueue;

        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }
}
