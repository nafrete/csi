<?php

namespace AppBundle\Document;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\EmailQueueRepository", collection="EmailQueue")
 */
class EmailQueue
{

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $instanceId;

    /**
     * @MongoDB\String
     */
    private $applicationId;

    /**
     * @MongoDB\String
     */
    private $application;

    /**
     * @MongoDB\String
     */
    private $subject;

    /**
     * @MongoDB\String
     */
    private $body;

    /**
     * @MongoDB\Collection
     */
    private $cc;

    /**
     * @MongoDB\Collection
     */
    private $bcc;

    /**
     * @MongoDB\Collection
     */
    private $to;

    /**
     * @MongoDB\String
     */
    private $from;

    /**
     * @MongoDB\String
     */
    private $status;

    /**
     * @MongoDB\Int
     */
    private $priority;

    /**
     * @MongoDB\Int
     */
    private $nbOfRetry;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="create")
     */
    private $creationTimestamp;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="change", field={"status"})
     */
    private $lastUpdateTimestamp;

    /**
     * @MongoDB\Timestamp
     */
    private $sendTimestamp;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Attachment", mappedBy="emailQueue")
     */
    private $attachments;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="emailQueue")
     */
    private $user;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
        $this->priority = 3;
        $this->to = array();
        $this->cc = array();
        $this->bcc = array();
        /*
         * highest = 1
         * hight = 2
         * normal = 3
         * low = 4
         * lowest = 5
         */
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNbOfRetry()
    {
        return $this->nbOfRetry;
    }

    public function setNbOfRetry($nbOfRetry)
    {
        $this->nbOfRetry = $nbOfRetry;

        return $this;
    }

    public function getCreationTimestamp()
    {
        return $this->creationTimestamp;
    }

    public function setCreationTimestamp($creationTimestamp)
    {
        $this->creationTimestamp = $creationTimestamp;

        return $this;
    }

    public function getSendTimestamp()
    {
        return $this->sendTimestamp;
    }

    public function setSendTimestamp($sendTimestamp)
    {
        $this->sendTimestamp = $sendTimestamp;

        return $this;
    }

    public function addAttachment(Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    }

    public function removeAttachment(Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getApplicationId()
    {
        return $this->applicationId;
    }

    public function setApplicationId($applicationId)
    {
        $this->applicationId = $applicationId;

        return $this;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;

        return $this;
    }

    public function getApplication()
    {
        return $this->application;
    }

    public function setApplication($application)
    {
        $this->application = $application;

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    public function getCc()
    {
        return $this->cc;
    }

    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    public function getBcc()
    {
        return $this->bcc;
    }

    public function setBcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    public function getInstanceId()
    {
        return $this->instanceId;
    }

    public function setInstanceId($instanceId)
    {
        $this->instanceId = $instanceId;

        return $this;
    }
}
