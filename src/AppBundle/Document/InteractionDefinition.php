<?php

namespace AppBundle\Document;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\InteractionDefinitionRepository", collection="InteractionDefinition")
 */
class InteractionDefinition
{

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="create")
     */
    private $creationTimestamp;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="change", field={"interaction"})
     */
    private $lastUpdateTimestamp;

    /**
     * @MongoDB\Hash
     */
    private $interaction;

    /**
     * @MongoDB\Boolean
     */
    private $archive;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="interactionsDefinition")
     */
    private $user;

    public function __construct()
    {
        $this->archive = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationTimestamp()
    {
        return $this->creationTimestamp;
    }

    public function setCreationTimestamp($creationTimestamp)
    {
        $this->creationTimestamp = $creationTimestamp;

        return $this;
    }

    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;

        return $this;
    }

    public function getInteraction()
    {
        return $this->interaction;
    }

    public function setInteraction($interaction)
    {
        $this->interaction = $interaction;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    public function getMainFields()
    {
        return array("type", "application", "description", "alreadyExecutedBody", "callbackRestService", "options");
    }

    public function getFieldsOptions()
    {
        return array("externalId", "type", "css", "position", "label", "feedbackResource");
    }

    public function getFieldsLanguage()
    {
        return array("language", "text");
    }
}
