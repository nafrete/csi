<?php

namespace AppBundle\Document;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\InteractionInstanceRepository", collection="InteractionInstance")
 */
class InteractionInstance
{

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $application;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="create")
     */
    private $creationTimestamp;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="change", field={"instance"})
     */
    private $lastUpdateTimestamp;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="interactionsInstance")
     */
    private $user;

    /**
     * @MongoDB\Hash
     */
    private $instance;

    /**
     * @MongoDB\Boolean
     */
    private $archive;

    public function __construct()
    {
        $this->archive = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationTimestamp()
    {
        return $this->creationTimestamp;
    }

    public function setCreationTimestamp($creationTimestamp)
    {
        $this->creationTimestamp = $creationTimestamp;

        return $this;
    }

    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    public function getApplication()
    {
        return $this->application;
    }

    public function setApplication($application)
    {
        $this->application = $application;

        return $this;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    public function getMainFields()
    {
        return array("interactionDefinitionId", "body", "externalParams", "userEmail");
    }
}
