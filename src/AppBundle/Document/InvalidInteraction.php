<?php

namespace AppBundle\Document;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\InvalidInteractionRepository", collection="InvalidInteraction")
 */
class InvalidInteraction
{

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Hash
     */
    private $interaction;

    /**
     * @MongoDB\String
     */
    private $errorMessage;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="create")
     */
    private $creationTimestamp;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="change", field={"status"})
     */
    private $lastUpdateTimestamp;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="invalidEmail")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    public function getCreationTimestamp()
    {
        return $this->creationTimestamp;
    }

    public function setCreationTimestamp($creationTimestamp)
    {
        $this->creationTimestamp = $creationTimestamp;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;

        return $this;
    }

    public function getInteraction()
    {
        return $this->email;
    }

    public function setInteraction($interaction)
    {
        $this->interaction = $interaction;

        return $this;
    }
}
