<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\OptionDefinitionsRepository", collection="OptionDefinitions")
 */
class OptionDefinitions {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $externalOptionId;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable(on="create")
     */
    private $creationTimestamp;

    /**
     * @MongoDB\Timestamp
     * @Gedmo\Timestampable
     */
    private $lastUpdateTimestamp;

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\InteractionDefinition", mappedBy="optionDefinitions")
     */
    private $interactionsDefinition;

    /**
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Resources")
     */
    private $resources = array();

    /**
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="optionDefinitions")
     */
    private $user;

    public function __construct() {
        $this->resources = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set externalOptionId
     *
     * @param string $externalOptionId
     * @return self
     */
    public function setExternalOptionId($externalOptionId) {
        $this->externalOptionId = $externalOptionId;
        return $this;
    }

    /**
     * Get externalOptionId
     *
     * @return string $externalOptionId
     */
    public function getExternalOptionId() {
        return $this->externalOptionId;
    }

    /**
     * Set creationTimestamp
     *
     * @param timestamp $creationTimestamp
     * @return self
     */
    public function setCreationTimestamp($creationTimestamp) {
        $this->creationTimestamp = $creationTimestamp;
        return $this;
    }

    /**
     * Get creationTimestamp
     *
     * @return timestamp $creationTimestamp
     */
    public function getCreationTimestamp() {
        return $this->creationTimestamp;
    }

    /**
     * Set lastUpdateTimestamp
     *
     * @param timestamp $lastUpdateTimestamp
     * @return self
     */
    public function setLastUpdateTimestamp($lastUpdateTimestamp) {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;
        return $this;
    }

    /**
     * Get lastUpdateTimestamp
     *
     * @return timestamp $lastUpdateTimestamp
     */
    public function getLastUpdateTimestamp() {
        return $this->lastUpdateTimestamp;
    }

    /**
     * Add resource
     *
     * @param AppBundle\Document\Resources $resource
     */
    public function addResource(\AppBundle\Document\Resources $resource) {
        $this->resources[] = $resource;
    }

    /**
     * Remove resource
     *
     * @param AppBundle\Document\Resources $resource
     */
    public function removeResource(\AppBundle\Document\Resources $resource) {
        $this->resources->removeElement($resource);
    }

    /**
     * Get resources
     *
     * @return Doctrine\Common\Collections\Collection $resources
     */
    public function getResources() {
        return $this->resources;
    }

    /**
     * Set user
     *
     * @param AppBundle\Document\User $user
     * @return self
     */
    public function setUser(\AppBundle\Document\User $user) {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return AppBundle\Document\User $user
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set interactionsDefinition
     *
     * @param AppBundle\Document\InteractionDefinition $interactionsDefinition
     * @return self
     */
    public function setInteractionsDefinition(\AppBundle\Document\InteractionDefinition $interactionsDefinition) {
        $this->interactionsDefinition = $interactionsDefinition;
        return $this;
    }

    /**
     * Get interactionsDefinition
     *
     * @return AppBundle\Document\InteractionDefinition $interactionsDefinition
     */
    public function getInteractionsDefinition() {
        return $this->interactionsDefinition;
    }

}
