<?php

namespace AppBundle\Document;

use JMS\Serializer\Annotation\Exclude;
use FOS\UserBundle\Model\User as BaseUser;
use IMAG\LdapBundle\User\LdapUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\UserRepository", collection="User")
 */
class User extends BaseUser implements LdapUserInterface
{

    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $dn;

    protected $attributes;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\InteractionDefinition", mappedBy="user")
     * @Exclude
     */
    private $interactionsDefinition;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\InteractionInstance", mappedBy="user")
     * @Exclude
     */
    private $interactionsInstance;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\InteractionInstance", mappedBy="receivers")
     * @Exclude
     */
    private $instance;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Application", mappedBy="user")
     * @Exclude
     */
    private $applications;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\EmailQueue", mappedBy="user")
     * @Exclude
     */
    private $emailQueue;

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\InvalidEmail", mappedBy="user")
     * @Exclude
     */
    private $invalidEmail;

    public function __construct()
    {
        parent::__construct();
        $this->enabled = true;
        $this->roles = array("ROLE_USER");
    }

    public function getDn()
    {
        return $this->dn;
    }

    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

    public function getCn()
    {
        return $this->username;
    }

    public function setCn($cn)
    {
        $this->username = $cn;

        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    public function getAttribute($name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface || $user->getUsername() !== $this->username || $user->getEmail() !== $this->email || count(array_diff($user->getRoles(), $this->getRoles())) > 0 || $user->getDn() !== $this->dn
        ) {
            return false;
        }

        return true;
    }

    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
            ) = unserialize($serialized);
    }

    public function addInteractionsDefinition(InteractionDefinition $interactionsDefinition)
    {
        $this->interactionsDefinition[] = $interactionsDefinition;
    }

    public function removeInteractionsDefinition(InteractionDefinition $interactionsDefinition)
    {
        $this->interactionsDefinition->removeElement($interactionsDefinition);
    }

    public function getInteractionsDefinition()
    {
        return $this->interactionsDefinition;
    }

    public function addInteractionsInstance(InteractionInstance $interactionsInstance)
    {
        $this->interactionsInstance[] = $interactionsInstance;
    }

    public function removeInteractionsInstance(InteractionInstance $interactionsInstance)
    {
        $this->interactionsInstance->removeElement($interactionsInstance);
    }

    public function getInteractionsInstance()
    {
        return $this->interactionsInstance;
    }

    public function addApplication(Application $application)
    {
        $this->applications[] = $application;
    }

    public function removeApplication(Application $application)
    {
        $this->applications->removeElement($application);
    }

    public function getApplications()
    {
        return $this->applications;
    }

    public function addInstance(InteractionInstance $instance)
    {
        $this->instance[] = $instance;
    }

    public function removeInstance(InteractionInstance $instance)
    {
        $this->instance->removeElement($instance);
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function addEmailQueue(EmailQueue $emailQueue)
    {
        $this->emailQueue[] = $emailQueue;
    }

    public function removeEmailQueue(EmailQueue $emailQueue)
    {
        $this->emailQueue->removeElement($emailQueue);
    }

    public function getEmailQueue()
    {
        return $this->emailQueue;
    }

    public function addInvalidEmail(InvalidEmail $invalidEmail)
    {
        $this->invalidEmail[] = $invalidEmail;
    }

    public function removeInvalidEmail(InvalidEmail $invalidEmail)
    {
        $this->invalidEmail->removeElement($invalidEmail);
    }

    public function getInvalidEmail()
    {
        return $this->invalidEmail;
    }

}
