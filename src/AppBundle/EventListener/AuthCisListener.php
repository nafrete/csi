<?php

namespace AppBundle\EventListener;

use AppBundle\Controller\MainController;
use AppBundle\Controller\ApplicationController;
use AppBundle\Controller\EmailController;
use AppBundle\Controller\InteractionDefinitionController;
use AppBundle\Controller\InteractionInstanceController;
use AppBundle\Controller\RabbitMQController;
use AppBundle\Controller\UserController;
use AppBundle\Controller\Rest\ApplicationRestController;
use AppBundle\Controller\Rest\EmailRestController;
use AppBundle\Controller\Rest\InteractionDefinitionRestController;
use AppBundle\Controller\Rest\InteractionInstanceRestController;
use AppBundle\Controller\Rest\UserRestController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Security\User\Provider\LdapUserProvider;
use Symfony\Component\HttpKernel\Exception\HttpException;


class AuthCisListener
{
    public $ldap;
    public $securityToken;

    public function __construct(LdapUserProvider $ldap, TokenStorage $securityToken)
    {
        $this->ldap = $ldap;
        $this->securityToken = $securityToken;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof MainController || $controller[0] instanceof ApplicationController ||
            $controller[0] instanceof EmailController || $controller[0] instanceof InteractionDefinitionController ||
            $controller[0] instanceof InteractionInstanceController || $controller[0] instanceof RabbitMQController ||
            $controller[0] instanceof UserController || $controller[0] instanceof ApplicationRestController ||
            $controller[0] instanceof InteractionDefinitionRestController || $controller[0] instanceof InteractionInstanceRestController ||
            $controller[0] instanceof UserRestController || $controller[0] instanceof AccessDeniedHttpException ||
            $controller[0] instanceof EmailRestController) {

            $user = $this->ldap->loadUserByUsername($_SERVER['REMOTE_USER']);

            if(!$user) {
                throw new HttpException('Unauthorized access.', 401);
            }

            $providerKey = 'secured_area'; // your firewall name
            $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
            $this->securityToken->setToken($token);
        }
    }
}