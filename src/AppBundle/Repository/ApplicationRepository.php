<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ApplicationRepository extends DocumentRepository
{

    public function getApplications()
    {
        $qb = $this->createQueryBuilder('a')
            ->sort('creationTimestamp', 'desc')
            ->field('archive')->equals(false)
            ->eagerCursor(true);

        $query = $qb->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function getApplicationsName()
    {
        $qb = $this->createQueryBuilder('a')
            ->select('id', 'application.name')
            ->field('archive')->equals(false)
            ->sort('name', 'asc')
            ->eagerCursor(true);

        $query = $qb->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function getApplication($id)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('id', 'application.name', 'application.username', 'application.password', 'creationTimestamp', 'lastUpdateTimestamp')
            ->field('id')->equals($id)
            ->field('archive')->equals(false);

        $query = $qb->getQuery()->getSingleResult();

        return $query;
    }
}
