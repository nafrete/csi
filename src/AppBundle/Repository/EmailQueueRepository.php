<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class EmailQueueRepository extends DocumentRepository
{

    public function getEmails($parameters)
    {
        $qb = $this->createQueryBuilder('e');

        $queryFiltered = $this->queryFiltered($qb, $parameters);
        $queryFiltered->limit($parameters['pageSize']);
        $queryFiltered->skip(($parameters['page'] - 1) * ($parameters['pageSize']));

        $query = $queryFiltered->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function queryFiltered($qb, $parameters)
    {
        if (isset($parameters['filters']['filters'][0]['filters'][0]['value'])) {
            $keyword = $parameters['filters']['filters'][0]['filters'][0]['value'];
            $qb->addOr($qb->expr()->field('application')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('subject')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('body')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('from')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('status')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('to')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('cc')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('bcc')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
        }

        $querySorted = $this->sortQuery($qb, $parameters);

        return $querySorted;
    }

    public function sortQuery($qb, $parameters)
    {
        if ($parameters['sort'] !== null) {
            $arraySort = array();
            foreach ($parameters['sort'] as $key => $sort) {
                $sort['field'] = ($sort['field'] === "last_update_timestamp") ? "lastUpdateTimestamp" : $sort['field'];
                $arraySort = $this->array_push_after($arraySort, array($sort['field'] => $sort['dir']), $key);
            }
            $qb->sort($arraySort);
        } else {
            $qb->sort("creationTimestamp", "desc");
        }

        return $qb;
    }

    function array_push_after($src, $in, $pos)
    {
        if (is_int($pos)) $R = array_merge(array_slice($src, 0, $pos + 1), $in, array_slice($src, $pos + 1));
        else {
            foreach ($src as $k => $v) {
                $R[$k] = $v;
                if ($k == $pos) $R = array_merge($R, $in);
            }
        }

        return $R;
    }

    public function getEmailsForCurrentUser($parameters)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->field('status')->equals('SENT');
        $qb->field('to')->equals($parameters['userEmail']);

        $queryFiltered = $this->queryFilteredForCurrentUser($qb, $parameters);
        $queryFiltered->limit($parameters['pageSize']);
        $queryFiltered->skip(($parameters['page'] - 1) * ($parameters['pageSize']));

        $query = $queryFiltered->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function queryFilteredForCurrentUser($qb, $parameters)
    {
        if (isset($parameters['filters']['filters'][0]['value'])) {
            $filters = $parameters['filters']['filters'];
            foreach ($filters as $filter) {
                if ($filter['field'] !== "instance.status") {
                    $qb->addAnd($qb->expr()->field($filter['field'])->equals(new \MongoRegex('/.*' . $filter['value'] . '.*/i')));
                }
            }
        }

        if (isset($parameters['filters']['filters'][0]['filters'][0]['value'])) {
            $keyword = $parameters['filters']['filters'][0]['filters'][0]['value'];
            $qb->addOr($qb->expr()->field('application')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('subject')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('body')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('from')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
        }

        $querySorted = $this->sortQuery($qb, $parameters);

        return $querySorted;
    }

    public function countEmailsForCurrentUser($parameters)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->field('status')->equals('SENT');
        $qb->field('to')->equals($parameters['userEmail']);
        $queryFiltered = $this->queryFilteredForCurrentUser($qb, $parameters);

        $count = $queryFiltered->count()
            ->getQuery()
            ->execute();

        return $count;
    }

    public function countEmails($parameters)
    {
        $qb = $this->createQueryBuilder('e');
        $queryFiltered = $this->queryFiltered($qb, $parameters);

        $count = $queryFiltered->count()
            ->getQuery()
            ->execute();

        return $count;
    }

    public function countEmailPending()
    {
        $count = $this->createQueryBuilder('e')
            ->field('status')->equals('PENDING')
            ->count()
            ->getQuery()
            ->execute();

        return $count;
    }

    public function countEmailSent()
    {
        $count = $this->createQueryBuilder('e')
            ->field('status')->equals('SENT')
            ->count()
            ->getQuery()
            ->execute();

        return $count;
    }

    public function getEmailsByStatus($status)
    {
        $applications = $this->createQueryBuilder('e')
            ->field('status')->equals($status)
            ->select('applicationId')
            ->getQuery()
            ->execute();

        return $applications->toArray();
    }
}
