<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class InteractionDefinitionRepository extends DocumentRepository
{

    public function getInteractionsDefinitions()
    {
        $qb = $this->createQueryBuilder('idef')
            ->field('user')->prime(true)
            ->field('archive')->equals(false)
            ->sort('creationTimestamp', 'desc');

        $query = $qb->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function getApplicationIdDescriptionById($id)
    {
        $qb = $this->createQueryBuilder('idef')
            ->select('id', 'interaction.application', 'interaction.description')
            ->field('id')->equals($id)
            ->field('archive')->equals(false);

        $query = $qb->getQuery()->getSingleResult();

        return $query;
    }

    public function getInteractionDefinition($id)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('id', 'creationTimestamp', 'lastUpdateTimestamp', 'interaction')
            ->field('id')->equals($id)
            ->field('archive')->equals(false);

        $query = $qb->getQuery()->getSingleResult();

        return $query;
    }
}
