<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class InteractionInstanceRepository extends DocumentRepository
{

    public function getInteractionsInstances($parameters)
    {
        $qb = $this->createQueryBuilder('i')->field('archive')->equals(false);

        $queryFiltered = $this->queryFiltered($qb, $parameters);
        $queryFiltered->limit($parameters['pageSize']);
        $queryFiltered->skip(($parameters['page'] - 1) * ($parameters['pageSize']));

        $query = $queryFiltered->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function queryFiltered($qb, $parameters)
    {
        if (isset($parameters['filters']['filters'][0]['filters'][0]['value'])) {
            $keyword = $parameters['filters']['filters'][0]['filters'][0]['value'];
            $qb->addOr($qb->expr()->field('application')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('instance.interactionDefinitionId')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('instance.body')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('instance.userEmail')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
            $qb->addOr($qb->expr()->field('instance.status')->equals(new \MongoRegex('/.*' . $keyword . '.*/i')));
        }

        $querySorted = $this->sortQuery($qb, $parameters);

        return $querySorted;
    }

    public function sortQuery($qb, $parameters)
    {
        if ($parameters['sort'] !== null) {
            $arraySort = array();
            foreach ($parameters['sort'] as $key => $sort) {
                $sort['field'] = ($sort['field'] === "last_update_timestamp") ? "lastUpdateTimestamp" : $sort['field'];
                $arraySort = $this->array_push_after($arraySort, array($sort['field'] => $sort['dir']), $key);
            }
            $qb->sort($arraySort);
        } else {
            $qb->sort("creationTimestamp", "desc");
        }

        return $qb;
    }

    function array_push_after($src, $in, $pos)
    {
        if (is_int($pos)) $R = array_merge(array_slice($src, 0, $pos + 1), $in, array_slice($src, $pos + 1));
        else {
            foreach ($src as $k => $v) {
                $R[$k] = $v;
                if ($k == $pos) $R = array_merge($R, $in);
            }
        }

        return $R;
    }

    public function countInstances($parameters)
    {
        $qb = $this->createQueryBuilder('i')->field('archive')->equals(false);
        $queryFiltered = $this->queryFiltered($qb, $parameters);

        $count = $queryFiltered->count()
            ->getQuery()
            ->execute();

        return $count;
    }

    public function getInteractionInstance($id)
    {
        $qb = $this->createQueryBuilder('i')
            ->field('archive')->equals(false)
            ->field('id')->equals($id);

        $query = $qb->getQuery();
        $cursor = $query->execute();

        return $cursor;
    }

    public function getLastServiceNowApproval()
    {
        $qb = $this->createQueryBuilder('i')
            ->field('archive')->equals(false)
            ->field('application')->equals("service now")
            ->sort("creationTimestamp", "desc")
            ->limit(1);

        $query = $qb->getQuery()->getSingleResult();

        return $query;
    }
}
