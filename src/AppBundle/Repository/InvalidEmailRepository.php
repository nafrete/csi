<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class InvalidEmailRepository extends DocumentRepository
{

    public function countInvalidEmail()
    {
        $count = $this->createQueryBuilder('e')
            ->count()
            ->getQuery()
            ->execute();

        return $count;
    }
}
