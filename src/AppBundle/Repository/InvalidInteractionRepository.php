<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class InvalidInteractionRepository extends DocumentRepository
{

    public function countInvalidInteraction()
    {
        $count = $this->createQueryBuilder('i')
            ->count()
            ->getQuery()
            ->execute();

        return $count;
    }
}
