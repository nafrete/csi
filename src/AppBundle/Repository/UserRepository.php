<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class UserRepository extends DocumentRepository
{

    public function getUsers()
    {
        $qb = $this->createQueryBuilder('u')
            ->sort('username', 'asc')
            ->eagerCursor(true);

        $query = $qb->getQuery();
        $cursor = $query->execute();

        return $cursor->toArray();
    }

    public function getUser($id)
    {
        $qb = $this->createQueryBuilder('u')
            ->field('id')->equals($id);

        $query = $qb->getQuery()->getSingleResult();

        return $query;
    }
}
