<?php

namespace AppBundle\Security\User\Provider;

use IMAG\LdapBundle\User\LdapUserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use IMAG\LdapBundle\Manager\LdapManagerUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class LdapUserProvider implements UserProviderInterface
{

    protected $userManager;
    protected $validator;
    private $ldapManager;

    /**
     * Constructor
     *
     * @param LdapManagerUserInterface $ldapManager
     * @param UserManagerInterface $userManager
     * @param Validator $validator
     */
    public function __construct(LdapManagerUserInterface $ldapManager, UserManagerInterface $userManager, $validator)
    {
        $this->ldapManager = $ldapManager;
        $this->userManager = $userManager;
        $this->validator = $validator;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function loadUserByUsername($username)
    {
        if (empty($username)) {
            throw new UsernameNotFoundException('The username is not provided.');
        }

        $user = $this->userManager->findUserBy(array("username" => $username));

        if (empty($user) && !$this->ldapManager->exists($username)) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found', $username));
        }

        $lm = $this->ldapManager
            ->setUsername($username)
            ->doPass();

        if(empty($user)) {
            $user = $this->userManager->findUserBy(array("email" => $lm->getEmail()));
        }

        if (empty($user)) {
            $user = $this->userManager->createUser();
            $user
                ->setUsername($lm->getUsername())
                ->setPassword("")
                ->setDn($lm->getDn())
                ->setEmail(strtolower($lm->getEmail()));

            $this->userManager->updateUser($user);
        }

        return $user;
    }

    public function supportsClass($class)
    {
        return $this->userManager->supportsClass($class);
    }
}