<?php

namespace AppBundle\Services;

use AppBundle\Document\Application;
use AppBundle\CisException\CisException;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;

class ApplicationService extends Utils
{

    private $odm;

    public function __construct(ODM $odm)
    {
        $this->odm = $odm;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:Application');
    }

    public function getApplications()
    {
        $results = array();
        $applications = $this->getRepository()->getApplications();

        foreach ($applications as $application) {
            array_push($results, $application);
        }

        return $results;
    }

    public function getApplication($id)
    {
        $application = $this->getRepository()->getApplication($id);

        if (!$application) {
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return $application;
    }

    public function getApplicationsName()
    {
        return $this->getRepository()->getApplicationsName();
    }

    public function getApplicationName($interactionDefinition)
    {
        $interaction = $interactionDefinition->getInteraction();
        $application = $this->getRepository()->findOneById($interaction['application']);

        return $application;
    }

    public function saveApplication(Application $application)
    {
        $this->validateObject($application);
        $this->validateApplicationUniqueness($application);
        $this->odm->persist($application);
        $this->odm->flush();
    }

    public function updateApplication(Application $application)
    {
        $this->validateObject($application);
        $this->validateApplicationUniqueness($application);
        $this->odm->flush();
    }

    public function deleteApplication($id)
    {
        $application = $this->getApplication($id);
        $application->setArchive(true);
        $this->odm->flush();
    }

    public function validateObject(Application $application)
    {
        $applicationObject = $application->getApplication();
        foreach ($application->getMainFields() as $field) {
            if (!isset($applicationObject[$field])) {
                CisException::InvalidObjectException(self::OBJECT_INVALID . " Field '" . $field . "' is missing.");
            }
        }
    }

    public function validateApplicationUniqueness(Application $application)
    {
        $applicationObject = $application->getApplication();
        $object = $this->getRepository()->findOneBy(array(
            'application.name' => strtolower($applicationObject['name']),
            'archive' => false,
        ));

        if ($object && $object->getId() !== $application->getId()) {
            CisException::UniquenessException(self::OBJECT_ALREADY_EXISTS);
        }
    }
}
