<?php

namespace AppBundle\Services;

use AppBundle\CisException\CisException;
use AppBundle\Document\InteractionDefinition;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;

class InteractionDefinitionService extends Utils
{

    private $odm;

    public function __construct(ODM $odm)
    {
        $this->odm = $odm;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:InteractionDefinition');
    }

    public function getInteractionsDefinitions()
    {
        return $this->getRepository()->getInteractionsDefinitions();
    }

    public function getInteractionDefinition($id)
    {
        $interactionDefinition = $this->getRepository()->getInteractionDefinition($id);

        if (!$interactionDefinition) {
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return $interactionDefinition;
    }

    public function saveInteractionDefinition(InteractionDefinition $interactionDefinition)
    {
        $this->validateObject($interactionDefinition);
        $this->validateApplicationExistence($interactionDefinition);
        $this->validateInteractionUniqueness($interactionDefinition);
        $this->odm->persist($interactionDefinition);
        $this->odm->flush();
    }

    public function updateInteractionDefinition(InteractionDefinition $interactionDefinition)
    {
        $this->validateObject($interactionDefinition);
        $this->validateApplicationExistence($interactionDefinition);
        $this->validateInteractionUniqueness($interactionDefinition);
        $this->odm->flush();
    }

    public function deleteInteractionDefinition($id)
    {
        $interactionDefinition = $this->getInteractionDefinition($id);
        $interactionDefinition->setArchive(true);
        $this->odm->flush();
        $this->archiveInstances($interactionDefinition);
    }

    public function archiveInstances(InteractionDefinition $interactionDefinition)
    {
        $interactionInstances = $this->odm->getRepository('AppBundle:InteractionInstance')->findBy(array(
            'instance.interactionDefinitionId' => $interactionDefinition->getId(),
        ));

        foreach ($interactionInstances as $interactionInstance) {
            $interactionInstance->setArchive(true);
        }
        $this->odm->flush();
    }

    public function validateInteractionUniqueness(InteractionDefinition $interactionDefinition)
    {
        $interactionObject = $interactionDefinition->getInteraction();
        $object = $this->getRepository()->findOneBy(array(
            'interaction.type' => strtolower($interactionObject['type']),
            'interaction.application' => strtolower($interactionObject['application']),
            'archive' => false,
        ));

        if ($object && $object->getId() !== $interactionDefinition->getId()) {
            CisException::UniquenessException(self::OBJECT_ALREADY_EXISTS);
        }
    }

    public function validateApplicationExistence(InteractionDefinition $interactionDefinition)
    {
        $interactionObject = $interactionDefinition->getInteraction();
        $application = $this->odm->getRepository('AppBundle:Application')->findOneById($interactionObject['application']);

        if (!$application) {
            CisException::InvalidObjectException(self::OBJECT_NOT_FOUND . ' : Application');
        }
    }

    public function validateObject(InteractionDefinition $interaction)
    {
        $interactionObject = $interaction->getInteraction();

        $this->validateFields($interactionObject, $interaction->getMainFields());
        $this->validateFields($interactionObject['alreadyExecutedBody'], $interaction->getFieldsLanguage());

        if ($interactionObject['type'] === 'interaction') {
            foreach ($interactionObject['options'] as $option) {
                $this->validateFields($option, $interaction->getFieldsOptions());
                $this->validateFields($option['label'][0], $interaction->getFieldsLanguage());
                $this->validateFields($option['feedbackResource'][0], $interaction->getFieldsLanguage());
            }
        }
    }

    public function validateFields($objectToCheck, $fieldsToCheck)
    {
        foreach ($fieldsToCheck as $field) {
            if (!isset($objectToCheck[$field])) {
                CisException::InvalidObjectException(self::OBJECT_INVALID . " Field '" . $field . "' is missing.");
            }
        }
    }
}
