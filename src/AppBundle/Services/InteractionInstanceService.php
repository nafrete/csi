<?php

namespace AppBundle\Services;

use AppBundle\API\ClientApi;
use JMS\Serializer\SerializerBuilder;
use AppBundle\CisException\CisException;
use AppBundle\Document\InvalidInteraction;
use AppBundle\Document\InteractionInstance;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;

class InteractionInstanceService extends Utils
{

    private $odm;
    private $mailService;

    public function __construct(ODM $odm, MailService $mailService)
    {
        $this->odm = $odm;
        $this->mailService = $mailService;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:InteractionInstance');
    }

    public function getApplicationRepository()
    {
        return $this->odm->getRepository('AppBundle:Application');
    }

    public function getInteractionDefinitionRepository()
    {
        return $this->odm->getRepository('AppBundle:InteractionDefinition');
    }

    public function getInteractionsInstances($parameters)
    {
        $result = array();
        $instances = $this->getRepository()->getInteractionsInstances($parameters);

        foreach ($instances as $instance) {
            array_push($result, $instance);
        }

        return array(
            "instances" => $result,
            "total" => $this->getRepository()->countInstances($parameters)
        );
    }

    public function getInteractionsInstancesForCurrentUser($parameters)
    {
        $results = $this->mailService->getEmailsForCurrentUser($parameters);
        $instanceArray = array();

        foreach ($results['emails'] as $email) {
            if ($email->getInstanceId()) {
                $interactions = $this->getInteractionsForResponse($email->getInstanceId());
                $instance = $this->getInstancesByStatus($interactions['interactionInstance'], $parameters);
                if ($instance) {
                    $serializer = SerializerBuilder::create()->build();
                    $emailObject = $serializer->serialize($email, 'json');
                    $instanceObject = $serializer->serialize($instance, 'json');
                    $definitionObject = $serializer->serialize($interactions['interactionDefinition'], 'json');
                    $mergeObject = array_merge(json_decode($emailObject, true), json_decode($definitionObject, true), json_decode($instanceObject, true));
                    array_push($instanceArray, $mergeObject);
                }
            }
        }

        return array(
            "instances" => $instanceArray,
            "total" => $results['total']
        );
    }

    public function getInstancesByStatus($instance, $parameters)
    {
        $instanceObject = $instance->getInstance();
        if (isset($parameters['filters']['filters'][0]['value'])) {
            $filters = $parameters['filters']['filters'];
            foreach ($filters as $filter) {
                if ($filter['field'] === "instance.status" && (strtolower($instanceObject['status']) !== strtolower($filter['value']) && strtolower($filter['value']) !== "all")) {
                    return array();
                }
            }
        }

        return $instance;
    }

    public function getInteractionInstanceByID($id)
    {
        $interactionInstance = $this->getRepository()->findOneById($id);

        if (!$interactionInstance) {
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return $interactionInstance;
    }

    public function saveInteraction(InteractionInstance $interactionInstance)
    {
        $this->validateObject($interactionInstance);
        $this->validateEmail($interactionInstance);

        $interactionDefinition = $this->retrieveInteractionDefinition($interactionInstance);
        $interactionDefinitionObject = $interactionDefinition->getInteraction();

        $application = $this->getApplicationRepository()->findOneBy(array('id' => $interactionDefinitionObject['application'],'archive' => false));
        $applicationObject = $application->getApplication();

        $interactionInstance->setApplication($applicationObject['name']);
        $instanceObject = $interactionInstance->getInstance();
        $instanceObject['status'] = 'PUBLISHED';
        $interactionInstance->setInstance($instanceObject);

        $this->odm->persist($interactionInstance);
        $this->odm->flush();

        $this->replacePlaceholderToCreateEmail($interactionInstance, $interactionDefinitionObject, $application);
    }

    public function updateInteractionInstance(InteractionInstance $interactionInstance)
    {
        $this->validateStatus($interactionInstance);
        $this->odm->flush();
    }

    public function validateObject(InteractionInstance $interactionInstance)
    {
        $interactionObject = $interactionInstance->getInstance();
        foreach ($interactionInstance->getMainFields() as $field) {
            if (!isset($interactionObject[$field])) {
                $this->createInvalidInteraction($interactionObject, self::OBJECT_INVALID . " Field '" . $field . "' is missing.");
                CisException::InvalidObjectException(self::OBJECT_INVALID . " Field '" . $field . "' is missing.");
            }
        }
    }

    public function validateStatus(InteractionInstance $interactionInstance)
    {
        $instanceObject = $interactionInstance->getInstance();
        $statusAvailable = array("PUBLISHED", "EXECUTED", "ARCHIVED", "CANCELLED");

        if (!in_array(strtoupper($instanceObject['status']), $statusAvailable, true)) {
            CisException::InvalidObjectException(self::OBJECT_INVALID . " Value '" . $instanceObject['status'] . "' for the field status is not possible.");
        }
    }

    public function validateEmail(InteractionInstance $interactionInstance)
    {
        $instanceObject = $interactionInstance->getInstance();

        if (isset($instanceObject['email'])) {
            $this->mailService->validateEmailObject($instanceObject['email']);
        }
    }

    public function retrieveInteractionDefinition(InteractionInstance $interactionInstance)
    {
        $instanceObject = $interactionInstance->getInstance();
        $interactionDefinition = $this->getInteractionDefinitionRepository()->findOneBy(
            array(
                'id' => $instanceObject['interactionDefinitionId'],
                'archive' => false
            ));

        if (!$interactionDefinition) {
            $this->createInvalidInteraction($instanceObject, self::INVALID_INTERACTION_ID);
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return $interactionDefinition;
    }

    public function createInvalidInteraction($interaction, $errorMessage)
    {
        $invalidInteraction = new InvalidInteraction();
        $invalidInteraction->setInteraction($interaction);
        $invalidInteraction->setErrorMessage($errorMessage);

        $this->odm->persist($invalidInteraction);
        $this->odm->flush();
    }

    public function replacePlaceholderToCreateEmail($interactionInstance, $interactionObject, $application)
    {
        $instanceObject = $interactionInstance->getInstance();

        if (isset($instanceObject['email'])) {
            $email = $instanceObject['email'];
            $email['subject'] = preg_replace(self::PLACEHOLDER, self::DOMAIN . $interactionInstance->getId(), $email['subject']);
            $email['body'] = preg_replace(self::PLACEHOLDER, self::DOMAIN . $interactionInstance->getId(), $email['body']);

            $instanceObject = $interactionInstance->getInstance();
            $email['applicationId'] = $interactionObject['application'];
            $email['instanceId'] = $interactionInstance->getId();
            $this->mailService->createEmail($email, $application);

            unset($instanceObject['email']);

            $interactionInstance->setInstance($instanceObject);
            $this->odm->flush();
        }
    }

    public function getInteractionsForResponse($id)
    {
        $interactionInstance = $this->getInteractionInstanceByID($id);
        $instance = $interactionInstance->getInstance();
        $interactionDefinition = $this->getInteractionDefinitionRepository()->getInteractionDefinition($instance['interactionDefinitionId']);

        if (!$interactionInstance || !$interactionDefinition) {
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return array(
            'interactionInstance' => $interactionInstance,
            'interactionDefinition' => $this->sortOptions($interactionDefinition),
        );
    }

    public function executeInteraction($id, $optionsValues, $emailApprover)
    {
        $interaction = $this->getInteractionsForResponse($id);

        $instance = $interaction['interactionInstance']->getInstance();
        $definition = $interaction['interactionDefinition']->getInteraction();

        $messageReturned = $definition['alreadyExecutedBody']['text'];
        if ($instance['status'] === self::STATUS_PUBLISHED) {
            $instance['status'] = self::STATUS_EXECUTED;
            $instance['executed_by'] = $emailApprover;
            $messageReturned = self::REQUEST_SUBMITED;
        }

        if ($definition['type'] === 'interaction') {
            if (isset($instance['actions'])) {
                array_push($instance['actions'], $this->createNewAction($optionsValues, $instance['status']));
            } else {
                $instance['actions'] = array($this->createNewAction($optionsValues, $instance['status']));
            }
            $interaction['interactionInstance']->setInstance($instance);
        }

        if($messageReturned === self::REQUEST_SUBMITED) {
            if ($interaction['interactionInstance']->getApplication() === "service now") {
                $this->mailService->sendTestMailNotification($interaction['interactionInstance']);
            } else {
                $this->sendInformationsToClientAPI($definition, $instance);
            }
        }

        $this->odm->flush();

        return array(
            'message' => $messageReturned,
            'interactionDefinition' => $interaction['interactionDefinition']
        );
    }

    public function sendInformationsToClientAPI($interactionDefinition, $instance)
    {
        $application = $this->getApplicationRepository()->getApplication($interactionDefinition["application"]);
        $applicationObject = $application->getApplication();

        $config = array(
            "description" => $interactionDefinition['description'],
            "base_url" => $interactionDefinition['callbackRestService'],
            "app_name" => $applicationObject['name'],
            "api_login" => $applicationObject['username'],
            "api_password" => $applicationObject['password'],
        );

        $clientApi = new ClientApi($config);

        unset($instance['body']);
        unset($instance['userEmail']);
        unset($instance['status']);
        unset($instance['actions'][0]['status']);

        $request = $clientApi->getHttpClient();
        $result = $request->post($interactionDefinition['callbackRestService'], [ 'body' => json_encode(array("instance" => $instance)) ]);
    }

    private function cmp($a, $b)
    {
        return strcmp(intval($a["position"]), intval($b["position"]));
    }

    public function sortOptions($interactionDefinition)
    {
        $interactionObject = $interactionDefinition->getInteraction();
        usort($interactionObject['options'], array($this, "cmp"));
        $interactionDefinition->setInteraction($interactionObject);

        return $interactionDefinition;
    }

    public function createNewAction($optionsValues, $status)
    {
        return array(
            'executionTimestamp' => date("Y-m-d H:i", time()),
            'parameters' => $optionsValues,
            'status' => $status,
        );
    }
}