<?php

namespace AppBundle\Services;

use Swift_Mailer as Mailer;
use AppBundle\Document\Attachment;
use AppBundle\Document\EmailQueue;
use AppBundle\Document\InvalidEmail;
use AppBundle\CisException\CisException;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;

class MailService
{

    const CONTENT_TYPE = "text/html";
    const STATUS_SENT = "SENT";
    const STATUS_PENDING = "PENDING";
    const STATUS_FAILED = "FAILED";
    const INVALID_EMAIL = "invalid email: ";
    const NB_OF_RETRY_INITIAL = 0;
    const NB_OF_EMAIL_SENT_INITIAL = 0;
    const MAX_LINE_LENGTH = 1000;
    const OBJECT_INVALID = "This object is invalid.";
    const SOS_EMAIL = "sosdev@ubisoft.com";

    private $mailer;
    private $templating;
    private $odm;
    private $securityContext;

    public function __construct(Mailer $mailer, Templating $templating, ODM $odm, SecurityContext $securityContext)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->odm = $odm;
        $this->securityContext = $securityContext;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:EmailQueue');
    }

    public function getApplicationRepository()
    {
        return $this->odm->getRepository('AppBundle:Application');
    }

    public function getEmails($parameters)
    {
        $result = array();
        $emails = $this->getRepository()->getEmails($parameters);

        foreach ($emails as $email) {
            array_push($result, $email);
        }

        $countEmails = $this->getRepository()->countEmails($parameters);

        return array(
            "emails" => $result,
            "total" => $countEmails
        );
    }

    public function getEmailsForCurrentUser($parameters)
    {
        $result = array();
        $emails = $this->getRepository()->getEmailsForCurrentUser($parameters);
        $countEmails = $this->getRepository()->countEmailsForCurrentUser($parameters);

        foreach ($emails as $email) {
            array_push($result, $email);
        }

        return array(
            "emails" => $result,
            "total" => $countEmails
        );
    }

    public function getEmailsPending()
    {
        $emails = $this->getRepository()->getEmailsByStatus(self::STATUS_PENDING);

        return $this->getStatisticsEmails($emails);
    }

    public function getEmailsSent()
    {
        $emails = $this->getRepository()->getEmailsByStatus(self::STATUS_SENT);

        return $this->getStatisticsEmails($emails);
    }

    public function getStatisticsEmails($emails)
    {
        $namesApplication = array();

        foreach ($emails as $email) {
            $application = $this->getApplicationRepository()->getApplication($email->getApplicationId());
            if ($application) {
                $applicationObject = $application->getApplication();
                if (array_key_exists($applicationObject['name'], $namesApplication)) {
                    $namesApplication[$applicationObject['name']] = $namesApplication[$applicationObject['name']] + 1;
                } else {
                    $namesApplication[$applicationObject['name']] = 1;
                }
            }
        }

        return $this->getResultStatistics($namesApplication);
    }

    public function getResultStatistics($namesApplication)
    {
        $arrayOfStatistics = array();
        foreach ($namesApplication as $key => $value) {
            array_push($arrayOfStatistics, array(
                'application' => $key,
                'emails' => $value
            ));
        }

        return $arrayOfStatistics;
    }

    public function createAttchment($file, $email)
    {
        $path_parts = pathinfo($file);

        $attachment = new Attachment();
        $attachment->setName("Attachment");
        $attachment->setFilename($path_parts['basename']);
        $attachment->setFile($file);
        $attachment->setEmailQueue($email);

        $this->odm->persist($attachment);

        return $attachment;
    }

    public function createInvalidEmail($email, $errorMessage)
    {
        $invalidEmail = new InvalidEmail();
        $invalidEmail->setStatus(self::STATUS_FAILED);
        $invalidEmail->setEmail($email);
        $invalidEmail->setErrorMessage($errorMessage);

        $this->odm->persist($invalidEmail);
        $this->odm->flush();
    }

    private function sendMail($_content)
    {
        $email = $_content['email'];
        $mail = \Swift_Message::newInstance()
            ->setContentType(self::CONTENT_TYPE)
            ->setMaxLineLength(self::MAX_LINE_LENGTH)
            ->setPriority($email->getPriority())
            ->setSubject($email->getSubject())
            ->setFrom($email->getFrom())
            ->setTo($email->getTo())
            ->setBody($_content['body']);

        if ($email->getCc() !== null) {
            $mail->setCc($email->getCc());
        }

        if ($email->getBcc() !== null) {
            $mail->setBcc($email->getBcc());
        }

        if ($email->getAttachments() !== null) {
            foreach ($email->getAttachments() as $attachment) {
                $mf = $attachment->getFile()->getMongoGridFSFile();
                $attachment = \Swift_Attachment::newInstance($mf->getBytes(), $mf->getFilename(), 'application');
                $mail->attach($attachment);
            }
        }

        $this->mailer->send($mail);
    }

    public function sendMailNotification()
    {
        $emailsToSend = $this->getRepository()->findBy(array('status' => self::STATUS_PENDING));
        $countEmailSent = self::NB_OF_EMAIL_SENT_INITIAL;

        foreach ($emailsToSend as $email) {
            $application = $this->getApplicationRepository()->getApplication($email->getApplicationId());
            $this->sendMail(array(
                'email' => $email,
                'body' => $this->templating->render('email/layout.html.twig', array(
                    'instanceId' => $email->getInstanceId(),
                    'subject' => $email->getSubject(),
                    'content' => $email->getBody(),
                    'application' => $application,
                )),
            ));
            $countEmailSent++;
            $email->setSendTimestamp(time());
            $email->setStatus(self::STATUS_SENT);
        }

        $this->odm->flush();

        return $countEmailSent;
    }

    public function createEmail($emailObject, $application = null)
    {
        $email = new EmailQueue();
        $email->setApplicationId($emailObject['applicationId']);
        $email->setSubject($emailObject['subject']);
        $email->setBody($emailObject['body']);
        $email->setFrom($emailObject['from']);
        $email->setTo($emailObject['to']);
        $email->setStatus(self::STATUS_PENDING);
        $email->setNbOfRetry(self::NB_OF_RETRY_INITIAL);

        if (isset($emailObject['instanceId'])) {
            $email->setInstanceId($emailObject['instanceId']);
        }

        if (isset($emailObject['cc'])) {
            $email->setCc($emailObject['cc']);
        }

        if (isset($emailObject['bcc'])) {
            $email->setBcc($emailObject['bcc']);
        }

        if (isset($emailObject['priority'])) {
            $email->setPriority($this->setPriority($emailObject['priority']));
        }

        if (isset($emailObject['attachment'])) {
            foreach ($emailObject['attachment'] as $attachment) {
                $email->addAttachment($this->createAttchment($attachment, $email));
            }
        }

        if ($application === null) {
            $application = $this->getApplicationRepository()->findOneBy(array('id' => $emailObject['applicationId'],'archive' => false));
        }

        $applicationObject = $application->getApplication();
        $email->setApplication($applicationObject['name']);

        $this->odm->persist($email);
        $this->odm->flush();
    }

    public function setPriority($emailPriority)
    {
        $options = array(
            'options' => array(
                'default' => 3,
                'min_range' => 1,
                'max_range' => 5,
            ),
            'flags' => FILTER_FLAG_ALLOW_OCTAL,
        );
        $priority = filter_var($emailPriority, FILTER_VALIDATE_INT, $options);

        return $priority;
    }

    public function validateEmailObject($emailObject)
    {
        $fieldsToCheck = array("subject", "body", "to", "from");

        foreach ($fieldsToCheck as $field) {
            if (!isset($emailObject[$field])) {
                CisException::InvalidObjectException(self::OBJECT_INVALID . " Field '" . $field . "' is missing.");
            }
        }

        $this->validateEmailAdresses($emailObject);
    }

    public function validateEmailAdresses($emailInformation)
    {
        $invalidEmail = array();
        $fieldsToCheck = array("to", "cc", "bcc");

        foreach ($fieldsToCheck as $field) {
            if (isset($emailInformation[$field])) {
                foreach ($emailInformation[$field] as $mailAddress) {
                    if (!filter_var($mailAddress, FILTER_VALIDATE_EMAIL)) {
                        array_push($invalidEmail, $field);
                    }
                }
            }
        }

        if (!filter_var($emailInformation['from'], FILTER_VALIDATE_EMAIL)) {
            array_push($invalidEmail, 'from');
        }

        if (count($invalidEmail) > 0) {
            $errorMessage = self::INVALID_EMAIL . implode(",", $invalidEmail);
            $this->createInvalidEmail($emailInformation, $errorMessage);
            CisException::InvalidObjectException(self::OBJECT_INVALID . " : " . $errorMessage);
        }
    }

    public function sendTestMailNotification($instance)
    {
        $instanceObject = $instance->getInstance();
        $subject = $instanceObject["actions"][0]["parameters"][0]["value"];
        $comment = "-";

        if(isset($instanceObject["actions"][0]["parameters"][1]["value"])) {
            $comment = $instanceObject["actions"][0]["parameters"][1]["value"];
        }

        $to = $this->securityContext->getToken()->getUser();

        if ($subject === "approve") {
            $messageBody = "<div>The user (" . $to->getEmail() . ") approved the approval.</div><div><br>Comment:" . $comment . " </div><br>" . $instanceObject["externalParams"]["reference"];
        } else {
            $messageBody = "<div>The user (" . $to->getEmail() . ") rejected the approval.</div><div><br>Comment:" . $comment . " </div><br>" . $instanceObject["externalParams"]["reference"];
        }

        $mail = \Swift_Message::newInstance()
            ->setContentType(self::CONTENT_TYPE)
            ->setSubject($subject)
            ->setFrom($to->getEmail())
            ->setTo(self::SOS_EMAIL)
            ->setBody($messageBody);

        $this->mailer->send($mail);
    }

}
