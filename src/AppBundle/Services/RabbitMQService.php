<?php

namespace AppBundle\Services;

use AppBundle\Component\SendEmailProducer as ProducerEmail;
use AppBundle\Component\SendInteractionProducer as ProducerInteraction;

class RabbitMQService
{

    private $producerMail;
    private $producerInteraction;

    public function __construct(ProducerEmail $sendEmailProducer, ProducerInteraction $sendInteractionProducer)
    {
        $this->producerMail = $sendEmailProducer;
        $this->producerInteraction = $sendInteractionProducer;
    }

    public function sendInteractionInstance($email)
    {
        $email = array('instance' => array(
            'interactionDefinitionId' => '552d104f7a99a104290f7a0c',
            'body' => '<div>Hello you receive a notification to approve or reject the absence.</div>',
            'externalParams' => array(
                'timesheetId' => 7971613,
                'approverId' => 72505,
            ),
            'userEmail' => 'msr-uat-cis@ubisoft.com',
            'email' => array(
                'subject' => 'Notification Test',
                'body' => '<div>Hello you receive a notification to approve or reject the absence.</div>',
                'to' => array(
                    $email,
                ),
                'from' => 'msr-uat-cis@ubisoft.org',
            ),
        ));

        $this->producerInteraction->sendMessage(json_encode($email));
    }

    public function sendEmailFromInstance($email)
    {
        $this->producerMail->sendMessage(json_encode($email));
    }

    public function applicationSendMail()
    {
        $email = array('email' => array(
            'applicationId' => '552f86751c4b5ab40e000034',
            'priority' => 2,
            'subject' => 'Approval',
            'body' => '<div>Hello you receive a notification to approve or reject the day.</div>',
            'to' => array(
                'test@ubisoft.fr'
            ),
            'cc' => array(
                'nass_5902@hotmail.fr',
                'nass5902@gmail.com',
            ),
            'bcc' => array(
                'nassim.afrete@ubi.fr',
            ),
            'from' => 'tony.test@ubisoft.org',
        ));

        $this->producerMail->sendMessage(json_encode($email));
    }
}
