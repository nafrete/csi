<?php

namespace AppBundle\Services;

use AppBundle\CisException\CisException;
use Doctrine\ODM\MongoDB\DocumentManager as ODM;

class UserService extends Utils
{

    private $odm;

    public function __construct(ODM $odm)
    {
        $this->odm = $odm;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:User');
    }

    public function getUsers()
    {
        $result = array();

        $users = $this->getRepository()->getUsers();
        foreach ($users as $user) {
            array_push($result, $user);
        }

        return $result;
    }

    public function getUser($id)
    {
        $user = $this->getRepository()->getUser($id);

        if (!$user) {
            CisException::NotFoundException(self::OBJECT_NOT_FOUND);
        }

        return $user;
    }

    public function checkIdentityUser($user)
    {
        if ($user === self::ANONYMOUS_USER) {
            return null;
        }

        return $user;
    }
}