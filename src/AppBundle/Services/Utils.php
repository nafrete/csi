<?php

namespace AppBundle\Services;

abstract class Utils
{

    const STATUS_PUBLISHED = 'PUBLISHED';
    const STATUS_EXECUTED = 'EXECUTED';
    const STATUS_FAILED = 'FAILED';
    const INVALID_INTERACTION_ID = 'Interaction ID not valid';
    const ANONYMOUS_USER = 'anon.';
    const DOMAIN = 'http://msr-uat-cis/en/interactioninstance/';
    const PLACEHOLDER = '/##LINK_API##/';
    const OBJECT_NOT_EXISTS = "This object does not exist.";
    const OBJECT_NOT_FOUND = "Object not found.";
    const OBJECT_ALREADY_EXISTS = "This object already exists.";
    const OBJECT_INVALID = "This object is invalid.";
    const REQUEST_SUBMITED = "Your request has been submited";
}
