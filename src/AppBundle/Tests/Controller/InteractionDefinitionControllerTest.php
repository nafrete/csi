<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionDefinitionControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/interactiondefinition');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $values = array('interaction' => array(
            'type' => 'interaction',
            'application' => '54e6029d1c4b5a740a00002b',
            'description' => 'Unit Test',
            'alreadyExecutedBody' => array(
                'language' => 'En',
                'text' => 'The request has already been executed.'
            ),
            'callbackRestService' => 'http://api/test/',
            'options' => array(
                array(
                    'type' => 'button',
                    'css' => 'approve',
                    'position' => '1',
                    'label' => array(
                        'language' => 'En',
                        'text' => 'Approve',
                    ),
                    'feedbackResource' => array(
                        'language' => 'En',
                        'text' => 'Your request has been approved',
                    )
                ),
                array(
                    'type' => 'button',
                    'css' => 'reject',
                    'position' => '2',
                    'label' => array(
                        'language' => 'En',
                        'text' => 'Reject',
                    ),
                    'feedbackResource' => array(
                        'language' => 'En',
                        'text' => 'Your request has been rejected',
                    )
                )
            )
        ));

        $client->request('POST', '/en/interactiondefinition/create', $values, array(), array('CONTENT_TYPE' => 'application/json'));
        $this->assertTrue(302 === $client->getResponse()->getStatusCode());
    }
}
