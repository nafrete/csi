<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionInstanceControllerTest extends WebTestCase
{

    private $password = "xxx";
    private $userName = "xxx.xxx";

    public function testCompleteScenario()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/interactioninstance');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form(array(
            '_username' => $this->userName,
            '_password' => $this->password,
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/en/interactioninstance');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function test_show()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/interactioninstance/5502f6b41c4b5a201f000031');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form(array(
            '_username' => $this->userName,
            '_password' => $this->password,
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/en/interactioninstance/5502f6b41c4b5a201f000031');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function test_show_instance()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/interactioninstance/557560b51c4b5a542e00003b/response');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form(array(
            '_username' => $this->userName,
            '_password' => $this->password,
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/en/interactioninstance/557560b51c4b5a542e00003b/response');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function test_show_instance_false()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/interactioninstance/zfe658z9d/response');
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form(array(
            '_username' => $this->userName,
            '_password' => $this->password,
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/en/interactioninstance/zfe658z9d/response');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
    }
}
