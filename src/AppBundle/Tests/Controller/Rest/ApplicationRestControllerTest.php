<?php

namespace AppBundle\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationRestControllerTest extends WebTestCase
{

    public function test_api_get_applications()
    {
        $client = static::createClient();
        $client->request('GET', '/api/applications');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_real_application()
    {
        $client = static::createClient();
        $client->request('GET', '/api/applications/54e5b87d1c4b5a001f00002b');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_false_application()
    {
        $client = static::createClient();
        $client->request('GET', '/api/applications/aaa777sss');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_name_applications()
    {
        $client = static::createClient();
        $client->request('GET', '/api/name/applications');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_delete_false_application()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/applications/5zeTestd78');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_update_false_application()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/applications/5zeTestd78');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_post_application_bad_format()
    {
        $client = static::createClient();
        $values = array('application' => array(
            'name' => 'test' . time(),
            'url' => 'http://test.com',
            'username' => 'test',
            'password' => 'test',
        ));

        $client->request('POST', '/api/applications', $values, array(), array('CONTENT_TYPE' => 'application/json'));
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }
}
