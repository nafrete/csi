<?php

namespace AppBundle\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmailRestControllerTest extends WebTestCase
{

    public function test_api_get_emails()
    {
        $client = static::createClient();
        $client->request('GET', '/api/emails');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_emails_pending()
    {
        $client = static::createClient();
        $client->request('GET', '/api/emails/pending');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_emails_sent()
    {
        $client = static::createClient();
        $client->request('GET', '/api/emails/sent');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }
}
