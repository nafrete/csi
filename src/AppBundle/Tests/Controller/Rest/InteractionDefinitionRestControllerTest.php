<?php

namespace AppBundle\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionDefinitionRestControllerTest extends WebTestCase
{

    public function test_api_get_interactions_definitions()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/definitions');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_real_interaction_definition()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/5502fa161c4b5a642b000029/definition');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_false_interaction_definition()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/5zeTestd78/definition');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_delete_false_interaction_definition()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/interactions/5zeTestd78/definition');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

}
