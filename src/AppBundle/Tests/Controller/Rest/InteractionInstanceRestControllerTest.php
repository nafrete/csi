<?php

namespace AppBundle\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionInstanceRestControllerRestTest extends WebTestCase
{

    public function test_api_get_interactions_instances()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/instances');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_interactions_instances_user()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/instances');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_false_interaction_instance()
    {
        $client = static::createClient();
        $client->request('GET', '/api/interactions/5zeTestd78/instance');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }
}
