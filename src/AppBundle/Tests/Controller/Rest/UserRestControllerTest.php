<?php

namespace AppBundle\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRestControllerTest extends WebTestCase
{

    public function test_api_get_emails()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }
}
