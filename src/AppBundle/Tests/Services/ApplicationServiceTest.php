<?php

namespace AppBundle\Tests\Services;

use AppBundle\Document\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationServiceTest extends WebTestCase
{

    const REAL_ID = "54ef24a01c4b5a6c2800002b";

    private static $applicationService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$applicationService = self::$container->get('application');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\ApplicationService');
    }

    public function test_get_repository()
    {
        $this->assertNotNull(self::$applicationService->getRepository());
    }

    public function test_get_applications()
    {
        $this->assertInternalType('array', self::$applicationService->getApplications());
    }

    public function test_get_application()
    {
        $application = self::$applicationService->getApplication(self::REAL_ID);
        $this->assertInternalType('object', $application);
        $this->assertNotNull($application);
    }

    public function test_get_false_application()
    {
        try {
            $this->assertInternalType('Exception', self::$applicationService->getApplication("64656"));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_get_applications_name()
    {
        $this->assertInternalType('array', self::$applicationService->getApplicationsName());
    }

    public function test_save_application()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "App" . time(),
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);

        $this->assertEmpty(self::$applicationService->saveApplication($applicationDocument));
    }

    public function test_save_false_application()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "App1429189207",
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);

        try {
            $this->assertEmpty(self::$applicationService->saveApplication($applicationDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object already exists.", $e->getMessage());
        }
    }

    public function test_update_application()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "App1429189207",
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);

        $this->assertEmpty(self::$applicationService->updateApplication($applicationDocument));
    }

    public function test_update_false_application()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "App1429189207",
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);

        try {
            $this->assertEmpty(self::$applicationService->updateApplication($applicationDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object already exists.", $e->getMessage());
        }
    }

    public function test_delete_false_application()
    {
        try {
            $this->assertEmpty(self::$applicationService->deleteApplication("ooooxxx"));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_is_object_valid()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "App1429189207",
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);

        $this->assertEmpty(self::$applicationService->validateObject($applicationDocument));
    }

    public function test_is_false_object_valid()
    {
        try {
            $applicationDocument = new Application();
            $application = array(
                "name" => "App1429189207",
                "url" => "http://app",
            );
            $applicationDocument->setApplication($application);
            $this->assertEmpty(self::$applicationService->validateObject($applicationDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType("string", $e->getMessage());
        }
    }

    public function test_validate_application_uniqueness()
    {
        $applicationDocument = new Application();
        $application = array(
            "name" => "Apps" . time(),
            "url" => "http://app",
            "username" => "test",
            "password" => "test"
        );
        $applicationDocument->setApplication($application);
        $this->assertEmpty(self::$applicationService->validateApplicationUniqueness($applicationDocument));
    }

    public function test_validate_false_new_application_name()
    {
        try {
            $applicationDocument = new Application();
            $application = array(
                "name" => "App1429189207",
                "url" => "http://app",
                "username" => "test",
                "password" => "test"
            );
            $applicationDocument->setApplication($application);
            $this->assertEmpty(self::$applicationService->validateApplicationUniqueness($applicationDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object already exists.", $e->getMessage());
        }
    }

    public function test_validate_false_application_name()
    {
        try {
            $applicationDocument = new Application();
            $application = array(
                "id" => "54ef24a01c4b5a6c2800002b",
                "name" => "App1429189207",
                "url" => "http://app",
                "username" => "test",
                "password" => "test"
            );
            $applicationDocument->setApplication($application);
            $this->assertEmpty(self::$applicationService->validateApplicationUniqueness($applicationDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object already exists.", $e->getMessage());
        }
    }
}