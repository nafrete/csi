<?php

namespace AppBundle\Tests\Services;

use AppBundle\Document\InteractionDefinition;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionDefinitionServiceTest extends WebTestCase
{

    const REAL_ID = "5502fa161c4b5a642b000029";

    private static $interactionService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$interactionService = self::$container->get('interaction.definition');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\InteractionDefinitionService');
    }

    public function test_get_repository()
    {
        $this->assertNotNull(self::$interactionService->getRepository());
    }

    public function test_get_interactions_definitions()
    {
        $this->assertInternalType('array', self::$interactionService->getInteractionsDefinitions());
    }

    public function test_get_interaction_definition()
    {
        $interaction = self::$interactionService->getInteractionDefinition(self::REAL_ID);
        $this->assertInternalType('object', $interaction);
        $this->assertNotNull($interaction);
    }

    public function test_get_false_interaction()
    {
        try {
            $this->assertInternalType('Exception', self::$interactionService->getInteractionDefinition("64656"));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_save_false_interaction_definition()
    {
        $interactionDocument = new InteractionDefinition();
        $interaction = array(
            'type' => 'interaction',
            'application' => '989879',
            'description' => 'Unit Test',
            'alreadyExecutedBody' => array(
                'language' => 'En',
                'text' => 'The request has already been executed.'
            ),
            'callbackRestService' => 'http://api/test/',
            'options' => array(
                array(
                    'type' => 'button',
                    'css' => 'approve',
                    'position' => '1',
                    'label' => array(
                        array(
                            'language' => 'En',
                            'text' => 'Reject',
                        )
                    ),
                    'feedbackResource' => array(
                        array(
                            'language' => 'En',
                            'text' => 'Reject',
                        )
                    )
                ),
                array(
                    'type' => 'button',
                    'css' => 'reject',
                    'position' => '2',
                    'label' => array(
                        array(
                            'language' => 'En',
                            'text' => 'Reject',
                        )
                    ),
                    'feedbackResource' => array(
                        array(
                            'language' => 'En',
                            'text' => 'Reject',
                        )
                    )
                )
            )
        );
        $interactionDocument->setInteraction($interaction);

        try {
            $this->assertEmpty(self::$interactionService->saveInteractionDefinition($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
        }
    }

    public function test_save_interaction_definition_already_exists()
    {
        try {
            $interactionDocument = new InteractionDefinition();
            $interaction = array(
                'id' => '5502fa161c4b5a642b000029',
                'type' => 'interaction',
                'application' => '54e6029d1c4b5a740a00002b',
                'description' => 'Unit Test' . time(),
                'alreadyExecutedBody' => array(
                    'language' => 'En',
                    'text' => 'The request has already been executed.'
                ),
                'callbackRestService' => 'http://api/test/',
                'options' => array(
                    array(
                        'externalId' => '55',
                        'type' => 'button',
                        'css' => 'approve',
                        'position' => '1',
                        'label' => array(
                            array(
                                'language' => 'En',
                                'text' => 'Reject',
                            )
                        ),
                        'feedbackResource' => array(
                            array(
                                'language' => 'En',
                                'text' => 'Reject',
                            )
                        )
                    ),
                    array(
                        'externalId' => '52',
                        'type' => 'button',
                        'css' => 'reject',
                        'position' => '2',
                        'label' => array(
                            array(
                                'language' => 'En',
                                'text' => 'Reject',
                            )
                        ),
                        'feedbackResource' => array(
                            array(
                                'language' => 'En',
                                'text' => 'Reject',
                            )
                        )
                    )
                )
            );
            $interactionDocument->setInteraction($interaction);
            $this->assertEmpty(self::$interactionService->updateInteractionDefinition($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object already exists.", $e->getMessage());
        }
    }

    public function test_delete_false_interaction_definition()
    {
        try {
            $this->assertEmpty(self::$interactionService->deleteInteractionDefinition("ooooxxx"));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_archive_instances()
    {
        $interaction = self::$interactionService->getInteractionDefinition(self::REAL_ID);
        $this->assertInternalType('object', $interaction);
        $this->assertNotNull($interaction);
        $this->assertEmpty(self::$interactionService->archiveInstances($interaction));
    }

    public function test_validate_application_existence()
    {
        $interaction = self::$interactionService->getInteractionDefinition(self::REAL_ID);
        $this->assertInternalType('object', $interaction);
        $this->assertNotNull($interaction);
        $this->assertEmpty(self::$interactionService->validateApplicationExistence($interaction));
    }

    public function test_validate__false_application_existence()
    {
        try {
            $interaction = self::$interactionService->getInteractionDefinition("5503068f1c4b5a081b00002d");
            $this->assertInternalType('object', $interaction);
            $this->assertNotNull($interaction);
            $this->assertEmpty(self::$interactionService->validateApplicationExistence($interaction));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found. : Application", $e->getMessage());
        }
    }

    public function test_validate_application_uniqueness()
    {
        $interaction = self::$interactionService->getInteractionDefinition(self::REAL_ID);
        $this->assertInternalType('object', $interaction);
        $this->assertNotNull($interaction);
        $this->assertEmpty(self::$interactionService->validateInteractionUniqueness($interaction));
    }

    public function test_is_object_valid()
    {
        $interactionDocument = new InteractionDefinition();
        $interaction = array(
            'type' => 'interaction',
            'application' => '54e6029d1c4b5a740a00002b',
            'description' => 'Unit Test' . time(),
            'alreadyExecutedBody' => array(
                'language' => 'En',
                'text' => 'The request has already been executed.'
            ),
            'callbackRestService' => 'http://api/test/',
            'options' => array()
        );
        $interactionDocument->setInteraction($interaction);
        $this->assertEmpty(self::$interactionService->validateObject($interactionDocument));
    }

    public function test_is_false_object_valid()
    {
        try {
            $interactionDocument = new InteractionDefinition();
            $interaction = array(
                'application' => '54e6029d1c4b5a740a00002b',
                'description' => 'Unit Test' . time(),
                'alreadyExecutedBody' => array(
                    'language' => 'En',
                    'text' => 'The request has already been executed.'
                ),
                'callbackRestService' => 'http://api/test/',
                'options' => array()
            );
            $interactionDocument->setInteraction($interaction);
            $this->assertEmpty(self::$interactionService->validateObject($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
        }
    }
}
