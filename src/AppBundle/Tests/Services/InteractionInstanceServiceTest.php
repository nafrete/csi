<?php

namespace AppBundle\Tests\Services;

use AppBundle\Document\InteractionInstance;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InteractionInstanceServiceTest extends WebTestCase
{

    const REAL_ID = "54e5f4a01c4b5a9c2400002b";

    private static $interactionService;
    private static $definitionService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$interactionService = self::$container->get('interaction.instance');
        self::$definitionService = self::$container->get('interaction.definition');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\InteractionInstanceService');
        $this->assertClassHasAttribute('mailService', 'AppBundle\Services\InteractionInstanceService');
    }

    public function test_get_repository()
    {
        $this->assertNotNull(self::$interactionService->getRepository());
    }

    public function test_get_application_repository()
    {
        $this->assertNotNull(self::$interactionService->getApplicationRepository());
    }

    public function test_get_interaction_repository()
    {
        $this->assertNotNull(self::$interactionService->getInteractionDefinitionRepository());
    }

    public function test_get_interactions_instances()
    {
        $parameters = array(
            "page" => 1,
            "pageSize" => 10,
            "sort" => null,
            "filters" => null,
        );
        $this->assertInternalType('array', self::$interactionService->getInteractionsInstances($parameters));
    }

    public function test_get_interactions_instances_current_user()
    {
        $parameters = array(
            "page" => 1,
            "pageSize" => 10,
            "sort" => null,
            "filters" => null,
            "userEmail" => "nassim.afrete@ubisoft.com",
        );
        $this->assertInternalType('array', self::$interactionService->getInteractionsInstancesForCurrentUser($parameters));
    }

    public function test_get_interaction_instance_by_id()
    {
        $interaction = self::$interactionService->getInteractionInstanceByID(self::REAL_ID);
        $this->assertInternalType('object', $interaction);
        $this->assertNotNull($interaction);
    }

    public function test_get_false_interaction_instance_by_id()
    {
        try {
            $this->assertInternalType('Exception', self::$interactionService->getInteractionInstanceByID("64656"));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_validate_object()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5534bc8a1c4b5af40d00002a",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "PUBLISHED"
        );

        $interactionDocument->setInstance($interaction);
        $this->assertEmpty(self::$interactionService->validateObject($interactionDocument));
    }

    public function test_validate_false_object()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5534bc8a1c4b5af40d00002a",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "PUBLISHED"
        );

        $interactionDocument->setInstance($interaction);
        try {
            $this->assertEmpty(self::$interactionService->validateObject($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
            $this->assertEquals("Error : This object is invalid. Field 'body' is missing.", $e->getMessage());
        }
    }

    public function test_validate_false_email_object()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5534bc8a1c4b5af40d00002a",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrebisoft.com"
                ),
                "from" => "cis@ubiso"
            ),
            "status" => "PUBLISHED"
        );

        $interactionDocument->setInstance($interaction);
        try {
            $this->assertEmpty(self::$interactionService->validateEmail($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
            $this->assertEquals("Error : This object is invalid. : invalid email: to,from", $e->getMessage());
        }
    }

    public function test_validate_status()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5534bc8a1c4b5af40d00002a",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "EXECUTED"
        );

        $interactionDocument->setInstance($interaction);
        $this->assertEmpty(self::$interactionService->validateStatus($interactionDocument));
    }

    public function test_validate_false_status()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5534bc8a1c4b5af40d00002a",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "TEST"
        );
        $interactionDocument->setInstance($interaction);

        try {
            $this->assertEmpty(self::$interactionService->validateStatus($interactionDocument));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : This object is invalid. Value 'TEST' for the field status is not possible.", $e->getMessage());
        }
    }

    public function test_check_interaction_definition()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5502fa161c4b5a642b000029",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "EXECUTED"
        );

        $interactionDocument->setInstance($interaction);
        $response = self::$interactionService->retrieveInteractionDefinition($interactionDocument);
        $this->assertInternalType('object', $response);
        $this->assertNotNull($response);
    }

    public function test_check_false_interaction_definition()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5498ezf",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "EXECUTED"
        );

        $interactionDocument->setInstance($interaction);

        try {
            $response = self::$interactionService->retrieveInteractionDefinition($interactionDocument);
            $this->assertInternalType('object', $response);
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_create_invalid_interaction()
    {
        $interaction = array(
            "interactionDefinitionId" => "5502fa161c4b5a642b000029",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "status" => "FALSE STATUS"
        );

        $this->assertEmpty(self::$interactionService->createInvalidInteraction($interaction, "error message"));
    }

    public function test_replace_placeholder()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5502fa161c4b5a642b000029",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => '<div>Hello you receive a notification to approve or reject the payment. <a href="/##LINK_API##/"></a></div>',
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "EXECUTED"
        );

        $interactionDocument->setInstance($interaction);
        $this->assertEmpty(self::$interactionService->replacePlaceholderToCreateEmail($interactionDocument));
    }

    public function test_get_interaction_for_response()
    {
        $arrayResponse = self::$interactionService->getInteractionsForResponse("552fad501c4b5ad81000002f");
        $this->assertInternalType('array', $arrayResponse);
        $this->assertArrayHasKey("interactionInstance", $arrayResponse);
        $this->assertArrayHasKey("interactionDefinition", $arrayResponse);
    }

    public function test_execute_false_interaction()
    {
        try {
            $arrayResponse = self::$interactionService->executeInteraction("az68d4a99", array("options"));
            $this->assertInternalType('array', $arrayResponse);
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_create_action()
    {
        $arrayResponse = self::$interactionService->createNewAction(array("options"), "EXECUTED");
        $this->assertInternalType('array', $arrayResponse);
        $this->assertArrayHasKey("executionTimestamp", $arrayResponse);
        $this->assertArrayHasKey("parameters", $arrayResponse);
        $this->assertArrayHasKey("status", $arrayResponse);
    }

    public function test_save_interaction()
    {
        $interactionDocument = new InteractionInstance();
        $interaction = array(
            "interactionDefinitionId" => "5502fa161c4b5a642b000029",
            "body" => "CIS - TEST",
            "externalParams" => array(
                "timesheetId" => "89zf78fefze8z9",
                "userId" => "89er9rger8re9ge"
            ),
            "userEmail" => "test@ubisoft.com",
            "email" => array(
                "subject" => "CIS - Notification",
                "body" => "<div>Hello you receive a notification to approve or reject the payment.</div>",
                "to" => array(
                    "nassim.afrete@ubisoft.com"
                ),
                "from" => "cis@ubisoft.org"
            ),
            "status" => "PUBLISHED"
        );

        $interactionDocument->setInstance($interaction);
        $this->assertEmpty(self::$interactionService->saveInteraction($interactionDocument));
    }
}
