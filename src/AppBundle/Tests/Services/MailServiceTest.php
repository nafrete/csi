<?php

namespace AppBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MailServiceTest extends WebTestCase
{

    private static $mailService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$mailService = self::$container->get('mail');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\MailService');
        $this->assertClassHasAttribute('templating', 'AppBundle\Services\MailService');
        $this->assertClassHasAttribute('mailer', 'AppBundle\Services\MailService');
    }

    public function test_check_email_validity()
    {
        $email = array(
            'to' => array(
                'nassim.afrete@ubisoft.com',
            ),
            'cc' => array(
                'nass_5902@hotmail.fr',
                'nass5902@gmail.com'
            ),
            'bcc' => array(
                'nassim.afrete@isep.fr'
            ),
            'from' => 'cis@ubisoft.org',
        );

        $numberOfErrors = self::$mailService->validateEmailAdresses($email);

        $this->assertEquals(0, sizeof($numberOfErrors));
    }

    public function test_check_email_validity_false()
    {
        $email = array(
            'to' => array(
                'nassim.afrete@ubisoft',
            ),
            'cc' => array(
                'nass_5902@hotmail.fr',
                'nass5902@gmail.com'
            ),
            'bcc' => array(
                'nassim.afrete@isep.fr'
            ),
            'from' => 'cis@ubisoft.org',
        );

        try {
            $this->assertEmpty(self::$mailService->validateEmailAdresses($email));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
            $this->assertEquals("Error : This object is invalid. : invalid email: to", $e->getMessage());
        }
    }

    public function test_check_email_validity_false2()
    {
        $email = array(
            'to' => array(
                'nassim.afrete@ubisoft',
            ),
            'cc' => array(
                'nass_5902@hotmail.fr',
                'nass5902gmail.com'
            ),
            'bcc' => array(
                'nassim.afrete@isep.fr'
            ),
            'from' => 'cis@ubisoft.org',
        );

        try {
            $this->assertEmpty(self::$mailService->validateEmailAdresses($email));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
            $this->assertEquals("Error : This object is invalid. : invalid email: to,cc", $e->getMessage());
        }
    }

    public function test_check_email_validity_false3()
    {
        $email = array(
            'to' => array(
                'nassim.afrete@ubisoft',
            ),
            'cc' => array(
                'nass_5902@hotmail.fr',
                'nass5902gmail.com'
            ),
            'bcc' => array(
                'nassim.afrete'
            ),
            'from' => 'cisubisoft.org',
        );

        try {
            $this->assertEmpty(self::$mailService->validateEmailAdresses($email));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertInternalType('string', $e->getMessage());
            $this->assertEquals("Error : This object is invalid. : invalid email: to,cc,bcc,from", $e->getMessage());
        }
    }

    public function test_validate_email_of_instance()
    {
        $email = array(
            'subject' => 'CIS - Notification',
            'body' => '<div>Hello you receive a notification to approve or reject the payment.</div>',
            'to' => array(
                'nassim.afrete@ubisoft.com'
            ),
            'from' => 'cis@ubisoft.org',
        );

        $this->assertEmpty(self::$mailService->validateEmailObject($email));
    }
}
