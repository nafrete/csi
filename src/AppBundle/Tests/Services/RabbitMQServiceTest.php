<?php

namespace AppBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RabbitMQServiceTest extends WebTestCase
{

    private static $rabbitMQService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$rabbitMQService = self::$container->get('rabbitMQ');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('producerMail', 'AppBundle\Services\RabbitMQService');
        $this->assertClassHasAttribute('producerInteraction', 'AppBundle\Services\RabbitMQService');
    }

}
