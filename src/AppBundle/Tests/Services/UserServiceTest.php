<?php

namespace AppBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Document\User;

class UserServiceTest extends WebTestCase
{

    private static $userService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$userService = self::$container->get('user');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\UserService');
    }

    public function test_get_repository()
    {
        $this->assertNotNull(self::$userService->getRepository());
    }

    public function test_get_users()
    {
        $this->assertInternalType('array', self::$userService->getUsers());
    }

    public function test_get_user()
    {
        $user = self::$userService->getUser("55268a161c4b5a4c1c000029");
        $this->assertInternalType('object', $user);
        $this->assertNotNull($user);
    }

    public function test_get_false_user()
    {
        try {
            $user = self::$userService->getUser("z8e9f");
            $this->assertInternalType('object', $user);
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
            $this->assertEquals("Error : Object not found.", $e->getMessage());
        }
    }

    public function test_check_identity_user_null()
    {
        $this->assertNull(self::$userService->checkIdentityUser(null));
    }
}