(function ($) {
    $.fn.interactionInstancePrototype = function (_interactionAPI) {

        'use strict';

        var interactionAPI = _interactionAPI;
        var container = this;

        var buttonCancel = container.find('.cancelInteraction');
        var buttonArchive = container.find('.archiveInteraction');

        function cssStatus() {
            $('.interaction_status').each(function () {
                var status = $(this).find('.label').text();

                if (status === 'PUBLISHED') {
                    $(this).find('.label').addClass('label-primary');
                } else if (status === 'CANCELLED') {
                    $(this).find('.label').addClass('label-danger');
                } else if (status === 'ARCHIVED') {
                    $(this).find('.label').addClass('label-warning');
                } else if (status === 'EXECUTED') {
                    $(this).find('.label').addClass('label-success');
                } else if (status === 'PROCESSED') {
                    $(this).find('.label').addClass('label-success');
                } else {
                    $(this).find('.label').addClass('label-default');
                }
            });
        }

        function initializeButtonCancel() {
            buttonCancel.removeClass("disabled");
            buttonCancel.html("Cancel");
        }

        function initializeButtonArchive() {
            buttonArchive.removeClass("disabled");
            buttonArchive.html("Archive");
        }

        function hideErrorMessages() {
            $('.errorSystem').addClass('hide');
        }

        function callbackUpdateStatus(data, elementParent, status) {
            initializeButtonCancel();
            initializeButtonArchive();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
            } else {
                htmlElementStatus(status, elementParent);
            }
        }

        function htmlElementStatus(status, elementParent) {
            if (status === 'CANCELLED') {
                var urlCancel = elementParent.find('.cancelInteraction').attr('data-url');
                elementParent.find('.archiveInteraction').remove();
                elementParent.find('.cancelInteraction').attr('data-url', urlCancel.replace('CANCELLED', 'ARCHIVED'));
                elementParent.find('.cancelInteraction').html('Archive');
                elementParent.find('.cancelInteraction').addClass('archiveInteraction');
                elementParent.find('.cancelInteraction').removeClass('cancelInteraction');
                elementParent.find('.interaction_status span').text('CANCELLED');
                elementParent.find('.interaction_status span').addClass('label-danger');
                elementParent.find('.interaction_status span').removeClass('label-warning');
                actionArchiveInteraction();
            } else {
                var urlArchive = elementParent.find('.archiveInteraction').attr('data-url');
                elementParent.find('.cancelInteraction').remove();
                elementParent.find('.archiveInteraction').attr('data-url', urlArchive.replace('ARCHIVED', 'CANCELLED'));
                elementParent.find('.archiveInteraction').text('Cancel');
                elementParent.find('.archiveInteraction').addClass('cancelInteraction');
                elementParent.find('.archiveInteraction').removeClass('archiveInteraction');
                elementParent.find('.interaction_status span').text('ARCHIVED');
                elementParent.find('.interaction_status span').addClass('label-warning');
                elementParent.find('.interaction_status span').removeClass('label-danger');
                actionCancelInteraction();
            }
        }

        function actionCancelInteraction() {
            $('.cancelInteraction').unbind('click');
            container.find('.cancelInteraction').click(function () {
                hideErrorMessages();
                var url = $(this).attr('data-url');
                var elementParent = $(this).parent().parent();
                $(this).addClass("disabled");
                $(this).html("Cancelling...");
                interactionAPI.updateStatusInteraction(url, callbackUpdateStatus, elementParent, 'CANCELLED');
            });
        }

        function actionArchiveInteraction() {
            $('.archiveInteraction').unbind('click');
            container.find('.archiveInteraction').click(function () {
                hideErrorMessages();
                var url = $(this).attr('data-url');
                var elementParent = $(this).parent().parent();
                $(this).addClass("disabled");
                $(this).html("Archiving...");
                interactionAPI.updateStatusInteraction(url, callbackUpdateStatus, elementParent, 'ARCHIVED');
            });
        }

        actionCancelInteraction();
        actionArchiveInteraction();
        cssStatus();

    };
})(jQuery);
function InteractionAPI() {

    this.saveInteraction = function (data, url, callback, method) {
        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $("#createInteraction").removeClass("disabled");
                $("#createInteraction").html("Create");
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                $('.errorSystem').removeClass('hide');
            }
        });
    };

    this.deleteInteraction = function (url, callback, elementHtmlToRemove) {
        $.ajax({
            type: "DELETE",
            url: url,
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $(".deleteInteraction").removeClass("disabled");
                $(".deleteInteraction").html("Delete");
                $('.table-responsive').find('.errorAlreadyDeleted').removeClass('hide');
                elementHtmlToRemove.parent().parent().remove();
            }
        });
    };
    
        this.updateStatusInteraction = function (url, callback, elementHtmlParent, status) {
        $.ajax({
            type: "PUT",
            url: url,
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data, elementHtmlParent, status);
            },
            error: function (resultat, statut, erreur) {
                $(".cancelInteraction").removeClass("disabled");
                $(".cancelInteraction").html("Cancel");
                $(".archiveInteraction").removeClass("disabled");
                $(".archiveInteraction").html("Archive");
            }
        });
    };
}
$(function () {

    'use strict';

    var interactionAPI = new InteractionAPI();
    $("#interactionInsContent").interactionInstancePrototype(interactionAPI);
});