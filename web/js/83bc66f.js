function InteractionDefinitionModel() {
    this.type;
    this.application;
    this.alreadyExecutedBody;
    this.callbackRestService;

    this.setType = function (_type) {
        this.type = _type;
    };

    this.setApplication = function (_application) {
        this.application = _application;
    };

    this.setAlreadyExecutedBody = function (_alreadyExecutedBody) {
        this.alreadyExecutedBody = _alreadyExecutedBody;
    };

    this.setCallbackRestService = function (_callbackRestService) {
        this.callbackRestService = _callbackRestService;
    };

    this.isFormValid = function () {
        if (this.type.length > 0
                && this.application.length > 0
                && this.alreadyExecutedBody.length > 0
                && this.callbackRestService.length > 0) {
            return true;
        } else {
            return false;
        }
    };
}
(function ($) {
    $.fn.interactionDefinitionPrototype = function (_interactionAPI, _interactionModel) {

        'use strict';

        var interactionAPI = _interactionAPI;
        var interactionModel = _interactionModel;
        var container = this;

        var tableInteractionss = container.find('.table-responsive');

        var buttonSubmit = container.find("#createInteraction");
        var buttonUpdate = container.find('#updateInteraction');

        var formInteraction = container.find('#formNewInteraction');
        var formUpdateInteraction = container.find('#formUpdateInteraction');

        var inputType_update = container.find('#appbundle_interactiondefinitiontype_type_update');
        var inputApplication_update = container.find('#appbundle_interactiondefinitiontype_application_update');
        var inputAlreadyExecutedBody_update = container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody_update');
        var inputCallbackRestService_update = container.find('#appbundle_interactiondefinitiontype_callbackRestService_update');

        initialize();
        function initialize() {
            actiondeleteInteraction();
            formInteraction.find('input').val('');
            removeFormButtonsAdded();
        }

        function initializeButtonCreate() {
            buttonSubmit.removeClass("disabled");
            buttonSubmit.html("Create new Interaction Definition");
        }

        function initializeButtonUpdate() {
            buttonUpdate.removeClass("disabled");
            buttonUpdate.html("Saving");
        }

        function hideModalFormUpdate() {
            container.find('#modalUpdate').modal('hide');
            container.find('body').removeClass('modal-open');
        }

        function hideErrorMessages() {
            formInteraction.find('.errorForm').addClass('hide');
            formInteraction.find('.errorAlreadyExists').addClass('hide');
            formUpdateInteraction.find('.errorForm_update').addClass('hide');
            formUpdateInteraction.find('.errorAlreadyExists_update').addClass('hide');
            $('.errorSystem').addClass('hide');
        }

        function removeFormButtonsAdded() {
            $(".contentButtons .contentDefinitionButtons").each(function (index) {
                if ($(this).find('.numeroPosition').text() !== '1') {
                    $(this).remove();
                }
            });
        }

        function callbackPostInteraction(data) {
            initializeButtonCreate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
            } else if (data === 'exists') {
                formInteraction.find('.errorAlreadyExists').removeClass('hide');
            } else {
                tableInteractionss.html(data);
                initialize();
            }
        }

        function callbackUpdateInteraction(data) {
            initializeButtonUpdate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
                hideModalFormUpdate();
            } else if (data === 'exists') {
                formUpdateInteraction.find('.errorAlreadyExists_update').removeClass('hide');
            } else {
                tableInteractionss.html(data);
                initialize();
                hideModalFormUpdate();
            }
        }

        function callbackDeleteInteraction(data) {
            tableInteractionss.html(data);
            initialize();
        }

        function actiondeleteInteraction() {
            $(".deleteInteraction").click(function () {
                var url = $(this).attr('data-url');
                var button = $(this);
                button.addClass("disabled");
                button.html("Deleting...");
                interactionAPI.deleteInteraction(url, callbackDeleteInteraction, button);
            });
        }

        function bindNewInteractionModel() {
            interactionModel.setType(container.find('#appbundle_interactiondefinitiontype_type').val().trim());
            interactionModel.setApplication(container.find('#appbundle_interactiondefinitiontype_application').val().trim());
            interactionModel.setAlreadyExecutedBody(container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody').val().trim());
            interactionModel.setCallbackRestService(container.find('#appbundle_interactiondefinitiontype_callbackRestService').val().trim());
        }

        function bindUpdateInteractionModel() {
            interactionModel.setType(inputType_update.val().trim());
            interactionModel.setApplication(inputApplication_update.val().trim());
            interactionModel.setAlreadyExecutedBody(inputAlreadyExecutedBody_update.val().trim());
            interactionModel.setCallbackRestService(inputCallbackRestService_update.val().trim());
        }

        function createOjectResources(toUpdate) {
            var resources = [];
            var elementRef = $("#formNewInteraction .contentButtons .contentDefinitionButtons");

            if (toUpdate) {
                elementRef = $("#formUpdateInteraction .contentButtons .contentDefinitionButtons");
            }

            $(elementRef).each(function (index) {
                var resource = {
                    language: $(this).find('.languageInput').val(),
                    text: $(this).find('.textInput').val(),
                    textFeedback: $(this).find('.textFeedbackInput').val(),
                    cssStyle: $(this).find('.cssStyleInput').val(),
                    position: $(this).find('.positionInput').val()
                };

                if (resource.language.length > 0 && resource.text.length > 0 && resource.textFeedback.length > 0 && resource.cssStyle.length > 0 && resource.position.length > 0) {
                    resources.push(resource);
                }
            });

            return resources;
        }

        function createInteractionDefinitionObject(toUpdate) {
            var interactionOject = {
                appbundle_interactiondefinitiontype: {
                    type: interactionModel.type,
                    application: interactionModel.application,
                    alreadyExecutedBody: interactionModel.alreadyExecutedBody,
                    callbackRestService: interactionModel.callbackRestService,
                    optionDefinitions: {
                        resources: createOjectResources(toUpdate)
                    }
                }
            };

            return interactionOject;
        }

        function bindFormUpdate(elementParent) {
            removeFormButtonsAdded();
            inputType_update.val(elementParent.find('.interaction_type').text());
            inputApplication_update.val(elementParent.find('.interaction_application').text());
            inputAlreadyExecutedBody_update.val(elementParent.find('.interaction_alreadyExecutedBody').text());
            inputCallbackRestService_update.val(elementParent.find('.interaction_callbackRestService').text());

            var count = 1;
            elementParent.find('.resourceOption').each(function (index) {
                var position = $('#formUpdateInteraction .contentButtons .contentDefinitionButtons').last().find('.numeroPosition').text();
                var element = $('#formUpdateInteraction .contentButtons .contentDefinitionButtons').last();
                var that = this;

                $(element).find('.numeroPosition').text(count);
                $(element).find('input').val('');

                $(element).find('input').each(function (index) {
                    $(this).attr('id', $(this).attr('id').replace(parseInt(position) - 1, parseInt(position) - 1));
                    $(this).attr('name', $(this).attr('name').replace(parseInt(position) - 1, parseInt(position) - 1));
                    $(this).parent().parent().parent().find('.cssStyleInput').val($(that).find('.interaction_cssStyle').text());
                    $(this).parent().parent().parent().find('.positionInput').val($(that).find('.interaction_position').text());
                    $(this).parent().parent().parent().find('.languageInput').val($(that).find('.interaction_language').text());
                    $(this).parent().parent().parent().find('.textInput').val($(that).find('.interaction_text').text());
                    $(this).parent().parent().parent().find('.textFeedbackInput').val($(that).find('.interaction_textFeedback').text());
                });

                if (count < elementParent.find('.resourceOption').length) {
                    var elementCloned = $('#formUpdateInteraction .contentButtons .contentDefinitionButtons').last().clone();
                    elementCloned.appendTo('#formUpdateInteraction .contentButtons');
                    $(elementCloned).find('.deleteButton').removeClass('hide');
                    removeFieldsNewButton();
                }

                count++;
            });
        }

        function removeFieldsNewButton() {
            $('.deleteButton').click(function () {
                $(this).parent().parent().remove();
            });
        }

        function bindClonedElement(position, cloneElement, toUpdate) {
            $(cloneElement).find('input').val('');
            $(cloneElement).find('.deleteButton').removeClass('hide');

            $(cloneElement).find('input').each(function (index) {
                $(this).attr('id', $(this).attr('id').replace(position - 1, position));
                $(this).attr('name', $(this).attr('name').replace(position - 1, position));
            });

            cloneElement.find('.numeroPosition').text(parseInt(position) + 1);

            if (toUpdate) {
                cloneElement.appendTo('#formUpdateInteraction .contentButtons');
            } else {
                cloneElement.appendTo('#formNewInteraction .contentButtons');
            }

            removeFieldsNewButton();
        }

        $('#modalUpdate').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var elementParent = button.parent().parent();

            formUpdateInteraction.find('input').val('');
            formUpdateInteraction.attr('data-id', '');
            formUpdateInteraction.attr('data-id', button.attr('data-id'));

            bindFormUpdate(elementParent);
        });

        buttonSubmit.click(function () {
            hideErrorMessages();
            bindNewInteractionModel();

            if (interactionModel.isFormValid()) {
                var url = $(this).parent().attr('data-url');
                var interactionOject = createInteractionDefinitionObject(false);
                buttonSubmit.addClass("disabled");
                buttonSubmit.html("Adding");
                interactionAPI.saveInteraction(interactionOject, url, callbackPostInteraction, 'POST');
            } else {
                formInteraction.find('.errorForm').removeClass('hide');
            }
        });

        buttonUpdate.click(function () {
            hideErrorMessages();
            bindUpdateInteractionModel();

            if (interactionModel.isFormValid()) {
                var url = formUpdateInteraction.attr('data-url') + 'update/' + formUpdateInteraction.attr('data-id');
                var interactionOject = createInteractionDefinitionObject(true);
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                interactionAPI.saveInteraction(interactionOject, url, callbackUpdateInteraction, 'PUT');
            } else {
                formUpdateInteraction.find('.errorForm_update').removeClass('hide');
            }
        });

        $('#formNewInteraction .addNewButton').click(function () {
            var position = $("#formNewInteraction .contentButtons .contentDefinitionButtons").last().find('.numeroPosition').text();
            var cloneElement = $("#formNewInteraction .contentButtons .contentDefinitionButtons").last().clone();
            bindClonedElement(position, cloneElement, false);
        });

        $('#formUpdateInteraction .addNewButton').click(function () {
            var position = $("#formUpdateInteraction .contentButtons .contentDefinitionButtons").last().find('.numeroPosition').text();
            var cloneElement = $("#formUpdateInteraction .contentButtons .contentDefinitionButtons").last().clone();
            bindClonedElement(position, cloneElement, true);
        });
    };
})(jQuery);
function InteractionAPI() {

    this.saveInteraction = function (data, url, callback, method) {
        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $("#createInteraction").removeClass("disabled");
                $("#createInteraction").html("Create");
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                $('.errorSystem').removeClass('hide');
            }
        });
    };

    this.deleteInteraction = function (url, callback, elementHtmlToRemove) {
        $.ajax({
            type: "DELETE",
            url: url,
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $(".deleteInteraction").removeClass("disabled");
                $(".deleteInteraction").html("Delete");
                $('.table-responsive').find('.errorAlreadyDeleted').removeClass('hide');
                elementHtmlToRemove.parent().parent().remove();
            }
        });
    };
    
        this.updateStatusInteraction = function (url, callback, elementHtmlParent, status) {
        $.ajax({
            type: "PUT",
            url: url,
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data, elementHtmlParent, status);
            },
            error: function (resultat, statut, erreur) {
                $(".cancelInteraction").removeClass("disabled");
                $(".cancelInteraction").html("Cancel");
                $(".archiveInteraction").removeClass("disabled");
                $(".archiveInteraction").html("Archive");
            }
        });
    };
}
$(function () {

    'use strict';

    var interactionAPI = new InteractionAPI();
    var interactionDefinitionModel = new InteractionDefinitionModel();
    
    $("#interactionDefContent").interactionDefinitionPrototype(interactionAPI, interactionDefinitionModel);
});