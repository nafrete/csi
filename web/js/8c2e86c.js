function InteractionInstanceModel() {
    this.name = 'instance';
    this.body;
    this.status;
    this.executionContext;

    this.getName = function () {
        return this.name;
    };
    
    this.getBody = function () {
        return this.type;
    };

    this.setBody = function (_body) {
        this.body = _body;
    };

    this.getStatus = function () {
        return this.status;
    };

    this.setStatus = function (_status) {
        this.status = _status;
    };

    this.getExecutionContext = function () {
        return this.executionContext;
    };

    this.setExecutionContext = function (_executionContext) {
        this.executionContext = _executionContext;
    };

    this.isFormValid = function () {
        if (this.body.length > 0 && this.status.length > 0 && this.executionContext.length > 0) {
            return true;
        } else {
            return false;
        }
    };
}
(function ($) {
    $.fn.interactionPrototype = function (_interactionAPI, _interactionModel) {

        'use strict';

        var interactionAPI = _interactionAPI;
        var interactionModel = _interactionModel;
        var container = this;

        var tableInteractionss = container.find('.table-responsive');

        var buttonSubmit = container.find("#createInteraction");
        var buttonUpdate = container.find('#updateInteraction');

        var formInteraction = container.find('#formNewInteraction');
        var formUpdateInteraction = container.find('#formUpdateInteraction');

        var inputType = container.find('#appbundle_interactiondefinitiontype_type');
        var inputApplication = container.find('#appbundle_interactiondefinitiontype_application');
        var inputAlreadyExecutedBody = container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody');
        var inputCallbackRestService = container.find('#appbundle_interactiondefinitiontype_callbackRestService');

        var inputBody = container.find('#appbundle_interactioninstancetype_body');
        var inputStatus = container.find('#appbundle_interactioninstancetype_status');
        var inputExecutionContext = container.find('#appbundle_interactioninstancetype_interactionInstanceActions_0_executionContext');

        var inputType_update = container.find('#appbundle_interactiondefinitiontype_type_update');
        var inputApplication_update = container.find('#appbundle_interactiondefinitiontype_application_update');
        var inputAlreadyExecutedBody_update = container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody_update');
        var inputCallbackRestService_update = container.find('#appbundle_interactiondefinitiontype_callbackRestService_update');

        var inputBody_update = container.find('#appbundle_interactioninstancetype_body_update');
        var inputStatus_update = container.find('#appbundle_interactioninstancetype_status_update');
        var inputExecutionContext_update = container.find('#appbundle_interactioninstancetype_interactionInstanceActions_0_executionContext_update');

        initialize();
        function initialize() {
            actiondeleteInteraction();
            formInteraction.find('input').val('');
        }

        function initializeButtonCreate() {
            buttonSubmit.removeClass("disabled");
            buttonSubmit.html("Create");
        }

        function initializeButtonUpdate() {
            buttonUpdate.removeClass("disabled");
            buttonUpdate.html("Saving");
        }

        function hideModalFormUpdate() {
            container.find('#modalUpdate').modal('hide');
            container.find('body').removeClass('modal-open');
        }

        function hideErrorMessages() {
            formInteraction.find('.errorForm').addClass('hide');
            formInteraction.find('.errorAlreadyExists').addClass('hide');
            formUpdateInteraction.find('.errorForm_update').addClass('hide');
            formUpdateInteraction.find('.errorAlreadyExists_update').addClass('hide');
            $('.errorSystem').addClass('hide');
        }

        function callbackPostInteraction(data) {
            initializeButtonCreate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
            } else if (data === 'exists') {
                formInteraction.find('.errorAlreadyExists').removeClass('hide');
            } else {
                tableInteractionss.html(data);
                initialize();
            }
        }

        function callbackUpdateInteraction(data) {
            initializeButtonUpdate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
                hideModalFormUpdate();
            } else if (data === 'exists') {
                formUpdateInteraction.find('.errorAlreadyExists_update').removeClass('hide');
            } else {
                tableInteractionss.html(data);
                initialize();
                hideModalFormUpdate();
            }
        }

        function actiondeleteInteraction() {
            $(".deleteInteraction").click(function () {
                var url = $(this).attr('data-url');
                var button = $(this);
                button.addClass("disabled");
                button.html("Deleting...");
                interactionAPI.deleteInteraction(url, callbackDeleteInteraction, button);
            });
        }

        function callbackDeleteInteraction(data) {
            tableInteractionss.html(data);
            initialize();
        }

        function bindNewInteractionModel() {
            if (interactionModel.getName() === 'definition') {
                interactionModel.setType(inputType.val().trim());
                interactionModel.setApplication(inputApplication.val().trim());
                interactionModel.setAlreadyExecutedBody(inputAlreadyExecutedBody.val().trim());
                interactionModel.setCallbackRestService(inputCallbackRestService.val().trim());
            }
            if (interactionModel.getName() === 'instance') {
                interactionModel.setBody(inputBody.val().trim());
                interactionModel.setStatus(inputStatus.val().trim());
                interactionModel.setExecutionContext(inputExecutionContext.val().trim());
            }
        }

        function bindUpdateInteractionModel() {
            if (interactionModel.getName() === 'definition') {
                interactionModel.setType(inputType_update.val().trim());
                interactionModel.setApplication(inputApplication_update.val().trim());
                interactionModel.setAlreadyExecutedBody(inputAlreadyExecutedBody_update.val().trim());
                interactionModel.setCallbackRestService(inputCallbackRestService_update.val().trim());
            }
            if (interactionModel.getName() === 'instance') {
                interactionModel.setBody(inputBody_update.val().trim());
                interactionModel.setStatus(inputStatus_update.val().trim());
                interactionModel.setExecutionContext(inputExecutionContext_update.val().trim());
            }
        }

        function createInteractionDefinitionObject() {
            var interactionOject = {
                appbundle_interactiondefinitiontype: {
                    type: interactionModel.type,
                    application: interactionModel.application,
                    alreadyExecutedBody: interactionModel.alreadyExecutedBody,
                    callbackRestService: interactionModel.callbackRestService
                }
            };

            return interactionOject;
        }

        function createInteractionInstanceObject() {
            var interactionOject = {
                appbundle_interactioninstancetype: {
                    body: interactionModel.body,
                    status: interactionModel.status,
                    interactionInstanceActions: [
                        {
                            executionContext: interactionModel.executionContext
                        }
                    ]
                }
            };

            return interactionOject;
        }

        function createNewInteractionObjectType() {
            if (interactionModel.getName() === 'definition') {
                return createInteractionDefinitionObject();
            }
            if (interactionModel.getName() === 'instance') {
                return createInteractionInstanceObject();
            }
        }

        function bindFormUpdate(elementParent) {
            if (interactionModel.getName() === 'definition') {
                var field_type = elementParent.find('.interaction_type').text();
                var field_application = elementParent.find('.interaction_application').text();
                var field_alreadyExecutedBody = elementParent.find('.interaction_alreadyExecutedBody').text();
                var field_callbackRestService = elementParent.find('.interaction_callbackRestService').text();

                inputType_update.val(field_type);
                inputApplication_update.val(field_application);
                inputAlreadyExecutedBody_update.val(field_alreadyExecutedBody);
                inputCallbackRestService_update.val(field_callbackRestService);
            }
            if (interactionModel.getName() === 'instance') {
                var field_body = elementParent.find('.interaction_body').text();
                var field_status = elementParent.find('.interaction_status').text();
                var field_execution_context = elementParent.find('.interaction_execution_context').text();

                inputBody_update.val(field_body);
                inputStatus_update.val(field_status);
                inputExecutionContext_update.val(field_execution_context);
            }
        }

        $('#modalUpdate').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var elementParent = button.parent().parent();

            formUpdateInteraction.find('input').val('');
            formUpdateInteraction.attr('data-id', '');
            formUpdateInteraction.attr('data-id', button.attr('data-id'));

            bindFormUpdate(elementParent);
        });

        buttonSubmit.click(function () {
            hideErrorMessages();
            bindNewInteractionModel();

            if (interactionModel.isFormValid()) {
                var url = $(this).parent().attr('data-url');
                var interactionOject = createNewInteractionObjectType();
                buttonSubmit.addClass("disabled");
                buttonSubmit.html("Adding");
                interactionAPI.saveInteraction(interactionOject, url, callbackPostInteraction, 'POST');
            } else {
                formInteraction.find('.errorForm').removeClass('hide');
            }
        });

        buttonUpdate.click(function () {
            hideErrorMessages();
            bindUpdateInteractionModel();

            if (interactionModel.isFormValid()) {
                var url = formUpdateInteraction.attr('data-url') + 'update/' + formUpdateInteraction.attr('data-id');
                var interactionOject = createNewInteractionObjectType();
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                interactionAPI.saveInteraction(interactionOject, url, callbackUpdateInteraction, 'PUT');
            } else {
                formUpdateInteraction.find('.errorForm_update').removeClass('hide');
            }
        });
    };
})(jQuery);
function InteractionAPI() {

    this.saveInteraction = function (data, url, callback, method) {
        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $("#createInteraction").removeClass("disabled");
                $("#createInteraction").html("Create");
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                $('.errorSystem').removeClass('hide');
            }
        });
    };

    this.deleteInteraction = function (url, callback, elementHtmlToRemove) {
        $.ajax({
            type: "DELETE",
            url: url,
            contentType: "application/json",
            dataType: "html",
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (resultat, statut, erreur) {
                $(".deleteInteraction").removeClass("disabled");
                $(".deleteInteraction").html("Delete");
                $('.table-responsive').find('.errorAlreadyDeleted').removeClass('hide');
                elementHtmlToRemove.parent().parent().remove();
            }
        });
    };
}
$(function () {

    'use strict';

    var interactionAPI = new InteractionAPI();
    var interactionInstanceModel = new InteractionInstanceModel();
    
    $("#interactionInsContent").interactionPrototype(interactionAPI, interactionInstanceModel);
});