$(function () {

    'use strict';

    function ApplicationAPI() {

        var contentType = 'application/json',
            dataTypeHTML = 'html';

        this.saveApplication = function (method, data, url, callback) {
            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify(data),
                contentType: contentType,
                dataType: dataTypeHTML,
                cache: false,
                success: function (data, status) {
                    callback(data, status);
                },
                error: function (result, status, error) {
                    callback(error, result.status);
                }
            });
        };

        this.deleteApplication = function (url, callback) {
            $.ajax({
                type: "DELETE",
                url: url,
                contentType: contentType,
                dataType: dataTypeHTML,
                cache: false,
                success: function (data, status) {
                    callback(data, status);
                },
                error: function (result, status, error) {
                    callback(error, result.status);
                }
            });
        };
    }

    $(".applicationContent").applicationPrototype(new ApplicationAPI());
});