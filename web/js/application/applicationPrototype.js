(function ($) {

    'use strict';

    $.fn.applicationPrototype = function (appliactionAPI) {

        var applicationAPI = appliactionAPI,
            container = this,
            errorMessage = $('.errorSystem'),
            tableApplications = container.find('.table-responsive'),
            entryTable = container.find('.table-responsive tbody tr'),
            buttonSubmit = container.find("#createApplication"),
            buttonUpdate = container.find('#updateApplication'),
            buttonDelete = container.find('#deleteApplication'),
            buttonCancel = container.find('#cancelApplication'),
            buttonsAction = container.find('.buttonsActions div button'),
            formApplication = container.find('#formNewApplication'),
            $searchInput = $('#searchInput');

        function initializeButtonsActions() {
            buttonSubmit.removeClass('hide');
            buttonsAction.parent().addClass('hide');
            buttonsAction.attr('data-id', '');
        }

        function initializeButtonCreate() {
            buttonSubmit.removeClass("disabled");
            buttonSubmit.html("Create new Application");
        }

        function initializeButtonDelete() {
            buttonDelete.removeClass("disabled");
            buttonDelete.html("Delete");
        }

        function initializeButtonUpdate() {
            buttonUpdate.removeClass("disabled");
            buttonUpdate.html("Save");
        }

        function hideErrorMessages() {
            formApplication.find('.errorForm').addClass('hide');
            errorMessage.addClass('hide');
        }

        function bindFormUpdate(elementParent) {
            var fields = ['name', 'url', 'author', 'password'],
                i;
            for (i = 0; i < fields.length; i++) {
                container.find('#applicationtype_' + fields[i]).val(elementParent.find('.application_' + fields[i]).text());
            }
        }

        function bindContentInformations(elementParent) {
            container.find('#informationsApplication').removeClass('hide');
            container.find('.nameApplication').text(elementParent.find('.application_name').text());

            var fields = ['name', 'url', 'author', 'created'],
                i;

            for (i = 0; i < fields.length; i++) {
                container.find('.information_' + fields[i] + ' span').text(elementParent.find('.application_' + fields[i]).text());
            }
        }

        function formUpdateAction() {
            entryTable.click(function () {
                hideErrorMessages();

                var elementParent = $(this);

                entryTable.removeClass('rowSelected');
                elementParent.addClass('rowSelected');

                buttonSubmit.addClass('hide');
                buttonsAction.parent().removeClass('hide');
                buttonsAction.attr('data-id', '');
                buttonsAction.attr('data-id', $(this).attr('data-id'));

                bindFormUpdate(elementParent);
                bindContentInformations(elementParent);
            });
        }

        function initialize() {
            initializeButtonsActions();
            formApplication.find('input').val('');
            container.find('#informationsApplication').addClass('hide');
            entryTable = $('.table-responsive tbody tr');
            formUpdateAction();
            $searchInput.val('');
        }

        initialize();

        function updateContent(data, status) {
            if (status === 400 || status === 404 || status === 500) {
                errorMessage.html(data).removeClass('hide');
                buttonSubmit.html("Create new Application").removeClass("disabled");
                buttonUpdate.removeClass("disabled");
                buttonDelete.removeClass("disabled");
            } else {
                tableApplications.html(data);
                initialize();
            }
        }

        function callbackPostApplication(data, status) {
            initializeButtonCreate();
            updateContent(data, status);
        }

        function callbackUpdateApplication(data, status) {
            initializeButtonUpdate();
            updateContent(data, status);
        }

        function callbackDeleteApplication(data, status) {
            initializeButtonDelete();
            if (status === 400 || status === 404 || status === 500) {
                errorMessage.html(data).removeClass('hide');
            } else {
                tableApplications.html(data);
                initialize();
            }
        }

        function createApplicationObject() {
            return {
                application: {
                    name: container.find('#applicationtype_name').val().trim().toLowerCase(),
                    url: container.find('#applicationtype_url').val().trim().toLowerCase(),
                    username: container.find('#applicationtype_author').val().trim(),
                    password: container.find('#applicationtype_password').val().trim()
                }
            };
        }

        function isFormValid() {
            var fields = ['name', 'url', 'author', 'password'],
                i;

            for (i = 0; i < fields.length; i++) {
                if (container.find('#applicationtype_' + fields[i]).val().trim().length <= 0) {
                    return false;
                }
            }

            return true;
        }

        buttonSubmit.click(function () {
            hideErrorMessages();

            if (isFormValid()) {
                var url = $(this).attr('data-url'),
                    applicationObject = createApplicationObject();

                buttonSubmit.html("Adding").addClass("disabled");
                applicationAPI.saveApplication('POST', applicationObject, url, callbackPostApplication);
            } else {
                formApplication.find('.errorForm').removeClass('hide');
            }
        });

        buttonUpdate.click(function () {
            hideErrorMessages();

            if (isFormValid()) {
                var url = $(this).attr('data-url') + 'update/' + $(this).attr('data-id'),
                    applicationObject = createApplicationObject();

                buttonUpdate.html("Saving").addClass("disabled");
                applicationAPI.saveApplication('PUT', applicationObject, url, callbackUpdateApplication);
            } else {
                formApplication.find('.errorForm').removeClass('hide');
            }
        });

        buttonDelete.click(function () {
            var url = $(this).attr('data-url') + 'delete/' + $(this).attr('data-id');
            buttonDelete.html("Deleting...").addClass("disabled");
            applicationAPI.deleteApplication(url, callbackDeleteApplication);
        });

        buttonCancel.click(function () {
            initialize();
        });

        $(".displaysNewForm").click(function () {
            initialize();
        });

        function filterApplication(value) {
            entryTable.each(function () {
                var application = $(this).find('.application_name').text().toLowerCase().match(value),
                    author = $(this).find('.application_author').text().toLowerCase().match(value);

                if (application !== null || author !== null) {
                    $(this).removeClass('hide');
                } else {
                    $(this).addClass('hide');
                }
            });
        }

        $searchInput.keyup(function () {
            var value = $(this).val().toLowerCase();
            if (value.length > 0) {
                filterApplication(value);
            } else {
                entryTable.removeClass('hide');
            }
        });

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1),
                vars = query.split("&"),
                i,
                pair;

            for (i = 0; i < vars.length; i++) {
                pair = vars[i].split("=");
                if (pair[0] === variable) {
                    return pair[1];
                }
            }
            return false;
        }

        var name = getQueryVariable('name');

        if (name) {
            $searchInput.val(name);
            filterApplication(name);
        }

        $(window).scroll(function () {
            if ($(window).scrollTop() === 0) {
                $('.page-header').css('box-shadow', '0px 0px 0px #ffffff');
            } else {
                $('.page-header').css('box-shadow', '2px 7px 14px #e8e8e8');
            }
        });
    };
})(jQuery);