(function ($) {
    $.fn.interactionDef = function (interactionDefinitionService) {

        'use strict';

        var interactionService = interactionDefinitionService;
        var container = this;

        var tableInteractionsDefinitions = container.find('.table-responsive');

        var buttonSubmit = container.find("#createInteractionDefinition");
        var buttonDelete = container.find(".deleteInteractionDefinition");
        var buttonUpdate = container.find('#updateInteractionDefinition');

        var formInteractionDefinition = container.find('#formNewInteractionDefinition');
        var formUpdateInteractionDefinition = container.find('#formUpdateInteractionDefinition');

        var inputType = container.find('#appbundle_interactiondefinitiontype_type');
        var inputApplication = container.find('#appbundle_interactiondefinitiontype_application');
        var inputAlreadyExecutedBody = container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody');
        var inputCallbackRestService = container.find('#appbundle_interactiondefinitiontype_callbackRestService');

        var inputType_update = container.find('#appbundle_interactiondefinitiontype_type_update');
        var inputApplication_update = container.find('#appbundle_interactiondefinitiontype_application_update');
        var inputAlreadyExecutedBody_update = container.find('#appbundle_interactiondefinitiontype_alreadyExecutedBody_update');
        var inputCallbackRestService_update = container.find('#appbundle_interactiondefinitiontype_callbackRestService_update');

        function Interaction(_type, _application, _alreadyExecutedBody, _callbackRestService) {
            this.type = _type;
            this.application = _application;
            this.alreadyExecutedBody = _alreadyExecutedBody;
            this.callbackRestService = _callbackRestService;

            this.isFormValid = function () {
                if (this.type.length > 0 && this.application.length > 0 && this.alreadyExecutedBody.length > 0 && this.callbackRestService.length > 0) {
                    return true;
                } else {
                    return false;
                }
            };
        }

        initialize();
        function initialize() {
            actiondeleteInteractionDefinition();
            formInteractionDefinition.find('input').val('');
        }

        function initializeButtonCreate() {
            buttonSubmit.removeClass("disabled");
            buttonSubmit.html("Create");
        }

        function initializeButtonDelete() {
            buttonDelete.removeClass("disabled");
            buttonDelete.html("Delete");
        }

        function initializeButtonUpdate() {
            buttonUpdate.removeClass("disabled");
            buttonUpdate.html("Saving");
        }

        function hideModalFormUpdate() {
            container.find('#modalUpdate').modal('hide');
            container.find('body').removeClass('modal-open');
        }

        function createNewInteractionObjectType(interaction) {
            var interactionOject = {
                appbundle_interactiondefinitiontype: {
                    type: interaction.type,
                    application: interaction.application,
                    alreadyExecutedBody: interaction.alreadyExecutedBody,
                    callbackRestService: interaction.callbackRestService
                }
            };

            return interactionOject;
        }

        function hideErrorMessages() {
            formInteractionDefinition.find('.errorForm').addClass('hide');
            formInteractionDefinition.find('.errorAlreadyExists').addClass('hide');
            formUpdateInteractionDefinition.find('.errorForm_update').addClass('hide');
            formUpdateInteractionDefinition.find('.errorAlreadyExists_update').addClass('hide');
            $('.errorSystem').addClass('hide');
        }

        buttonSubmit.click(function () {
            hideErrorMessages();

            var interaction = new Interaction(
                    inputType.val().trim(),
                    inputApplication.val().trim(),
                    inputAlreadyExecutedBody.val().trim(),
                    inputCallbackRestService.val().trim()
                    );

            if (interaction.isFormValid()) {
                var url = $(this).parent().attr('data-url');
                var interactionOject = createNewInteractionObjectType(interaction);
                buttonSubmit.addClass("disabled");
                buttonSubmit.html("Adding");
                interactionService.saveInteractionDefinition(interactionOject, url, callbackPostInteraction, 'POST');
            } else {
                formInteractionDefinition.find('.errorForm').removeClass('hide');
            }
        });

        function callbackPostInteraction(data) {
            initializeButtonCreate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
            } else if (data === 'exists') {
                formInteractionDefinition.find('.errorAlreadyExists').removeClass('hide');
            } else {
                tableInteractionsDefinitions.html(data);
                initialize();
            }
        }

        $('#modalUpdate').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var elementParent = button.parent().parent();
            var field_type = elementParent.find('.interaction_type').text();
            var field_application = elementParent.find('.interaction_application').text();
            var field_alreadyExecutedBody = elementParent.find('.interaction_alreadyExecutedBody').text();
            var field_callbackRestService = elementParent.find('.interaction_callbackRestService').text();

            formUpdateInteractionDefinition.find('input').val('');
            formUpdateInteractionDefinition.attr('data-id', '');
            formUpdateInteractionDefinition.attr('data-id', button.attr('data-id'));
            inputType_update.val(field_type);
            inputApplication_update.val(field_application);
            inputAlreadyExecutedBody_update.val(field_alreadyExecutedBody);
            inputCallbackRestService_update.val(field_callbackRestService);
        });

        buttonUpdate.click(function () {
            hideErrorMessages();

            var interaction = new Interaction(
                    inputType_update.val().trim(),
                    inputApplication_update.val().trim(),
                    inputAlreadyExecutedBody_update.val().trim(),
                    inputCallbackRestService_update.val().trim()
                    );

            if (interaction.isFormValid()) {
                var url = formUpdateInteractionDefinition.attr('data-url') + 'update/' + formUpdateInteractionDefinition.attr('data-id');
                var interactionOject = createNewInteractionObjectType(interaction);
                $('#btnUpdateInteraction').addClass("disabled");
                $('#btnUpdateInteraction').html("Saving");
                interactionService.saveInteractionDefinition(interactionOject, url, callbackUpdateInteraction, 'PUT');
            } else {
                formUpdateInteractionDefinition.find('.errorForm_update').removeClass('hide');
            }
        });

        function callbackUpdateInteraction(data) {
            initializeButtonUpdate();
            if (data === 'error') {
                $('.errorSystem').removeClass('hide');
                hideModalFormUpdate();
            } else if (data === 'exists') {
                formUpdateInteractionDefinition.find('.errorAlreadyExists_update').removeClass('hide');
            } else {
                tableInteractionsDefinitions.html(data);
                initialize();
                hideModalFormUpdate();
            }
        }

        function actiondeleteInteractionDefinition() {
            $(".deleteInteractionDefinition").click(function () {
                var url = $(this).attr('data-url');
                var button = $(this);
                button.addClass("disabled");
                button.html("Deleting...");
                interactionService.deleteInteractionDefinition(url, callbackDeleteInteraction, button);
            });
        }

        function callbackDeleteInteraction(data) {
            tableInteractionsDefinitions.html(data);
            initialize();
        }
    };
})(jQuery);
$(function () {

    'use strict';

    function InteractionDefinition() {

        this.saveInteractionDefinition = function (data, url, callback, method) {
            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify(data),
                contentType: "application/json; charset=UTF-8",
                dataType: "html",
                cache: false,
                success: function (data) {
                    callback(data);
                },
                error: function (resultat, statut, erreur) {
                    $("#createInteractionDefinition").removeClass("disabled");
                    $("#createInteractionDefinition").html("Create");
                    $('#btnUpdateInteraction').addClass("disabled");
                    $('#btnUpdateInteraction').html("Saving");
                    $('.errorSystem').removeClass('hide');
                }
            });
        };

        this.deleteInteractionDefinition = function (url, callback, elementHtmlToRemove) {
            $.ajax({
                type: "DELETE",
                url: url,
                contentType: "application/json; charset=UTF-8",
                dataType: "html",
                cache: false,
                success: function (data) {
                    callback(data);
                },
                error: function (resultat, statut, erreur) {
                    $(".deleteInteractionDefinition").removeClass("disabled");
                    $(".deleteInteractionDefinition").html("Delete");
                    $('.table-responsive').find('.errorAlreadyDeleted').removeClass('hide');
                    elementHtmlToRemove.parent().parent().remove();
                }
            });
        };
    }

    var interactionDefinition = new InteractionDefinition();

    $("#interactionDefContent").interactionDef(interactionDefinition);
});