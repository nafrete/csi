$(function () {

    'use strict';

    var $gridEmails = $("#gridEmails"),
        url = $gridEmails.attr('data-url'),
        wnd,
        detailsTemplate,
        total,
        dataSource;

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json"
            }
        },
        schema: {
            parse: function (response) {
                var emails = [];
                emails.push(response.emails);
                total = response.total;

                return emails[0];
            },
            total: function () {
                return total;
            },
            model: {
                id: "id",
                fields: {
                    application: {type: "string"},
                    status: {type: "string"},
                    body: {type: "string"},
                    to: {
                        type: "string"
                    },
                    cc: {
                        type: "string"
                    },
                    bcc: {
                        type: "string"
                    },
                    from: {
                        type: "string"
                    },
                    priority: {
                        type: "number"
                    },
                    nb_of_retry: {
                        type: "number"
                    },
                    attachments: {
                        type: "string"
                    },
                    last_update_timestamp: {
                        type: "date",
                        parse: function (value) {
                            if (value === undefined) {
                                return null;
                            }
                            return new Date(value.sec * 1000);
                        }
                    },
                    creation_timestamp: {
                        type: "date",
                        parse: function (value) {
                            return new Date(value.sec * 1000);
                        }
                    }
                }
            }
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    });

    $gridEmails.kendoGrid({
        dataSource: dataSource,
        groupable: false,
        reorderable: true,
        resizable: true,
        filterable: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        editable: "popup",
        columns: [
            {
                field: "application",
                title: "Application",
                width: 250
            },
            {
                field: "status",
                title: "Status",
                width: 250
            },
            {
                field: "last_update_timestamp",
                title: "Updated at",
                format: "{0:d/MM/yyyy}",
                width: 250
            },
            {
                field: "to",
                title: "To",
                width: 300
            },
            {
                command: {
                    text: "details",
                    click: showDetails
                },
                title: " ",
                width: "180px"
            }
        ]
    }).data("kendoGrid");

    function showDetails(e) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        wnd.content(detailsTemplate(dataItem));
        wnd.center().open();
    }

    wnd = $("#details").kendoWindow({
        title: "Email details",
        modal: true,
        visible: false,
        resizable: true,
        width: 700
    }).data("kendoWindow");

    detailsTemplate = kendo.template($("#template").html());

    $("#searchInput").keyup(function (e) {
        if (e.which === 13) {
            e.preventDefault();

            var selectedArray,
                orfilter,
                andfilter,
                selecteditem,
                kgrid = $("#gridEmails").data("kendoGrid");

            selecteditem = $('#searchInput').val().toLowerCase();
            selectedArray = selecteditem.split(" ");
            selectedArray = selectedArray.slice(0, 2);

            if (selecteditem) {
                orfilter = {logic: "or", filters: []};
                andfilter = {logic: "and", filters: []};

                $.each(selectedArray, function (i, v1) {
                    if (v1.trim() !== "") {
                        orfilter.filters.push(
                            {
                                field: "application",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "to",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "from",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "subject",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "status",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "body",
                                operator: "contains",
                                value: v1
                            }
                        );
                        andfilter.filters.push(orfilter);
                        orfilter = {logic: "or", filters: []};
                    }
                });
                kgrid.dataSource.filter(andfilter);
            } else {
                kgrid.dataSource.filter({});
            }
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() === 0) {
            $('.page-header').css('box-shadow', 'none');
        } else {
            $('.page-header').css('box-shadow', '2px 7px 14px #c7c8c7');
        }
    });
});