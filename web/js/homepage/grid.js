$(function () {

    'use strict';

    var url = $("#gridActions").attr('data-url'),
        urlApplications = $("#gridActions").attr('data-applications'),
        total,
        interactionAPI,
        options = [],
        dataSource;

    function checkButton(optionButton, optionButtonToCompare) {
        if (optionButton.attr('data-external') === optionButtonToCompare.attr('data-external')) {
            return 1;
        }
        return 0;
    }

    function getValuesOptionsButtons(optionButton) {
        $('#contentOptions .optionButton').each(function () {
            var option = {};

            if ($('#body').attr('data-application') === "service now") {
                if (checkButton(optionButton, $(this)) === 1) {
                    option = {
                        value: $(this).attr('data-external')
                    };
                    options.push(option);
                }
            } else {
                option = {
                    externalId: $(this).attr('data-external'),
                    value: checkButton(optionButton, $(this))
                };
                options.push(option);
            }
        });

        return options;
    }

    function getValuesOptionsComment() {
        $('#contentOptions .optionComment').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: $(this).find('textarea').val()
            };
            options.push(option);
        });

        return options;
    }

    function isChecked(inputCheckbox) {
        if (inputCheckbox.is(':checked')) {
            return 1;
        }
        return 0;
    }

    function getValuesMultipleChoices(optionElement) {
        var values = [];
        optionElement.find('input').each(function () {
            var value = {
                choice: $(this).val(),
                value: isChecked($(this))
            };
            values.push(value);
        });

        return values;
    }

    function getValuesOptionsCheckbox() {
        $('#contentOptions .optionCheckbox').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: getValuesMultipleChoices($(this))
            };
            options.push(option);
        });

        return options;
    }

    function getValuesOptionsRadio() {
        $('#contentOptions .optionRadio').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: getValuesMultipleChoices($(this))
            };
            options.push(option);
        });

        return options;
    }

    function executeAction() {
        $('.optionButton button').click(function () {
            $('#modalPreventAction').show();
            $(this).find('span').removeClass('hide');
            var optionButton = $(this).parent(),
                urlApi = $('#body').attr('data-url'),
                data;
            options = [];

            getValuesOptionsButtons(optionButton);
            getValuesOptionsComment();
            getValuesOptionsCheckbox();
            getValuesOptionsRadio();

            data = {
                options: options
            };

            interactionAPI.postAction(urlApi, data);
        });
    }

    function InteractionAPI() {
        this.getTemplateUser = function (url) {
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                beforeSend: function (xhr) {
                    $('#modalNotification .modal-body').html('<div>loading <i class="fa fa-spinner fa-pulse"></i></div>');
                },
                success: function (data) {
                    $('#modalNotification .modal-body').html(data);
                    $("table.serviceNow").addClass('hide');
                    $("table.serviceNow").clone().appendTo(".variablesSN");
                    $(".variablesSN table.serviceNow").removeClass('hide');
                    $(".bodyInstance table.serviceNow").remove();
                    executeAction();
                }
            });
        };

        this.getTemplateInstance = function (url) {
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                beforeSend: function (xhr) {
                    $('.popover-content').html('<div>loading <i class="fa fa-spinner fa-pulse"></i></div>');
                },
                success: function (data) {
                    $('.popover-content').html(data);
                }
            });
        };

        this.postAction = function (url, data) {
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                contentType: "application/json",
                dataType: "json",
                cache: false,
                beforeSend: function () {
                    $('.optionButton button').addClass('disabled');
                },
                success: function () {
                    $('.optionButton button').removeClass('disabled');
                    $('.optionButton button span').addClass('hide');
                    $('#modalPreventAction').hide();
                    $('#modalNotification').modal('hide');
                    $('#gridActions').data('kendoGrid').dataSource.read();
                    if ($('#clearFilterResp').is(':visible')) {
                        $('.flash-message').animate({"right": "5px"}, "slow").delay(1800).animate({"right": "-250px"}, "slow");
                    } else {
                        $('.flash-message').animate({"right": "65px"}, "slow").delay(1800).animate({"right": "-250px"}, "slow");
                    }
                },
                error: function () {
                    $('.optionButton button').removeClass('disabled');
                    $('.optionButton button span').addClass('hide');
                    $('.optionButtonInline').parent().find('button').removeClass('disabled');
                    $('.optionButtonInline').parent().find('.loader').addClass('hide');
                    $('#modalPreventAction').hide();
                    $('#modalNotification').modal('hide');
                    $('#modalErrorNotification').modal('show');
                }
            });
        };
    }

    interactionAPI = new InteractionAPI();

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                complete: function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('[data-toggle="popover"]').popover();
                    var idCurrentElement,
                        urlApi;
                    $('.detail-popover').on('show.bs.popover', function () {
                        idCurrentElement = $(this).attr('data-id');
                        if($('body').attr('data-language') === "fr") {
                            urlApi = $('#modalNotification').attr('data-url') + '/interactioninstance/' + idCurrentElement + '/overview';
                        } else {
                            urlApi = $('#modalNotification').attr('data-url') + 'en/interactioninstance/' + idCurrentElement + '/overview';
                        }
                    });
                    $(".detail-popover").popover({
                        html : true,
                        content: function () {
                            return interactionAPI.getTemplateInstance(urlApi);
                        },
                        title: function () {
                            return "Overview";
                        },
                        container: "body"
                    });
                }
            }
        },
        schema: {
            parse: function (response) {
                var instances = [];
                instances.push(response.instances);
                total = response.total;

                return instances[0];
            },
            total: function () {
                return total;
            },
            model: {
                id: "id",
                fields: {
                    application: {type: "string"},
                    userEmail: {type: "string"},
                    priority: {
                        type: "string",
                        parse: function (value) {
                            var priorityString = "normal";
                            if (value === 1 || value === 2) {
                                priorityString = "hight";
                            }
                            if (value === 4 || value === 5) {
                                priorityString = "hight";
                            }
                            return priorityString;
                        }
                    },
                    instance: {
                        interactionDefinitionId: {type: "string"},
                        externalParams: {type: "array"},
                        status: {
                            type: "string",
                            parse: function (value) {
                                if (value.status === "PUBLISHED") {
                                    return "OPEN";
                                }
                                return value.status;
                            }
                        },
                        userEmail: {
                            type: "string",
                            parse: function (value) {
                                return value.userEmail;
                            }
                        }
                    },
                    last_update_timestamp: {
                        type: "date",
                        parse: function (value) {
                            if (value === undefined || value === null) {
                                return null;
                            }
                            return new Date(value.sec * 1000);
                        }
                    },
                    creation_timestamp: {
                        type: "date",
                        parse: function (value) {
                            return new Date(value.sec * 1000);
                        }
                    }
                }
            }
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    });

    $("#gridActions").kendoGrid({
        dataSource: dataSource,
        groupable: false,
        reorderable: true,
        resizable: true,
        allowCopy: true,
        toolbar: kendo.template($("#templateSearchApplication").html()),
        filterable: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        editable: "popup",
        columns: [
            {
                field: "application",
                title: "Application",
                attributes: {
                    "data-title": "Application"
                },
                filterable: {
                    cell: {
                        showOperators: false
                    }
                },
                template: '<span class="instanceId" data-id="#= id#" >#= application #</span>',
                width: 150
            },
            {
                field: "subject",
                title: "Description",
                attributes: {
                    "data-title": "Description"
                },
                filterable: false,
                template: '<span class="detail-popover" data-id="#= id#" data-trigger="click hover focus"><i class="fa fa-info-circle infoInstance"></i></span> <span>#= subject #</span>',
                width: 400
            },
            {
                field: "last_update_timestamp",
                title: "Sent Date (UTC)",
                attributes: {
                    "data-title": "Sent Date"
                },
                format: "{0:d MMM yyyy H:mm}",
                filterable: false,
                width: 300
            },
            {
                title: "Actions",
                attributes: {
                    "data-title": "Actions"
                },
                template: kendo.template($("#command-template").html()),
                width: 200
            }
        ]
    }).data("kendoGrid");

    function kendoFilterSimple(field, value) {
        $("#gridActions").data("kendoGrid").dataSource.filter(
            {
                field: field,
                operator: "eq",
                value: value
            }
        );
    }

    function kendoFilterMultiple(applicationValue, statusValue) {
        $("#gridActions").data("kendoGrid").dataSource.filter(
            [
                {
                    field: "application",
                    operator: "eq",
                    value: applicationValue
                },
                {
                    field: "instance.status",
                    operator: "eq",
                    value: statusValue
                }
            ]
        );
    }

    $('#clearFilter').click(function () {
        $('#application').val('');
        $('#searchInput').val('');
        $('.k-input').empty();
        $("#gridActions").data("kendoGrid").dataSource.filter({});
    });

    $('#clearFilterResp').click(function () {
        $('#application').val('');
        $('#searchInput').val('');
        $('.k-input').empty();
        $("#gridActions").data("kendoGrid").dataSource.filter({});
    });

    $('.statusFilter ul li').click(function () {
        var value = $(this).attr('data-status');
        if (value) {
            $('.statusFilter ul li').css('text-decoration', 'none');
            $('.statusFilter ul li').removeClass('statusSelected');
            $(this).css('text-decoration', 'underline');
            $(this).addClass('statusSelected');
            if ($("#application").val()) {
                kendoFilterMultiple($("#application").val(), value);
            } else {
                kendoFilterSimple("instance.status", value);
            }
        } else {
            $("#gridActions").data("kendoGrid").dataSource.filter({});
        }
    });

    $("#application").kendoDropDownList({
        dataTextField: "application.name",
        dataValueField: "application.name",
        autoBind: false,
        dataSource: {
            transport: {
                read: {
                    url: urlApplications,
                    dataType: "json"
                }
            }
        },
        change: function () {
            var value = this.value(),
                status;
            if (value) {
                status = $('.statusFilter ul li.statusSelected').text();
                if (status) {
                    kendoFilterMultiple(value, status);
                } else {
                    kendoFilterSimple("application", value);
                }
            } else {
                $("#gridActions").data("kendoGrid").dataSource.filter({});
            }
        }
    });

    $("#gridActions").on("click", "tbody tr", function (e) {
        if (($(e.target).is('button.inlineAction.btn') || $(e.target).is('i')) && !$(e.target).is('i.infoInstance')) {
            $('#modalPreventAction').show();
            $(e.target).parent().parent().find('button').addClass('disabled');

            var data,
                optionButton,
                urlPost;

            if ($(e.target).is('i')) {
                $(e.target).parent().parent().find('.loader').removeClass('hide');
                optionButton = $(e.target).parent().parent();
            } else {
                $(e.target).find('.loader').removeClass('hide');
                optionButton = $(e.target);
            }

            urlPost = optionButton.parent().attr("data-url");
            options = [];

            optionButton.parent().parent().find('span button').each(function () {
                if ($(this).attr('data-application').toLowerCase() === "service now") {
                    if (checkButton(optionButton, $(this)) === 1) {
                        var option = {
                            value: $(this).attr('data-external')
                        };
                        options.push(option);
                    }
                } else {
                    var option = {
                        externalId: $(this).attr('data-external'),
                        value: checkButton(optionButton, $(this))
                    };
                    options.push(option);
                }
            });

            data = {
                options: options
            };

            interactionAPI.postAction(urlPost, data);
        } else {
            var idCurrentElement = $(this).find('.instanceId').attr('data-id'),
                urlApi;

            if($('body').attr('data-language') === "fr") {
                urlApi = $('#modalNotification').attr('data-url') + '/interactioninstance/' + idCurrentElement + '/response';
            } else {
                urlApi = $('#modalNotification').attr('data-url') + 'en/interactioninstance/' + idCurrentElement + '/response';
            }

            $('#modalNotification').modal('show');
            interactionAPI.getTemplateUser(urlApi);
        }
    });

    $('#modalNotification').on('hide.bs.modal', function () {
        $('#modalNotification .modal-body').empty();
    });

    $("#searchInput").keyup(function (e) {
        if (e.which === 13) {
            e.preventDefault();

            var selectedArray,
                orfilter,
                andfilter,
                selecteditem = $('#searchInput').val(),
                kgrid = $("#gridActions").data("kendoGrid");

            selecteditem = selecteditem.toUpperCase();
            selectedArray = selecteditem.split(" ");

            if (selecteditem) {
                orfilter = {logic: "or", filters: []};
                andfilter = {logic: "and", filters: []};
                $.each(selectedArray, function (i, v1) {
                    if (v1.trim() !== "") {
                        orfilter.filters.push(
                            {
                                field: "instance.userEmail",
                                operator: "contains",
                                value: v1
                            }
                        );
                        andfilter.filters.push(orfilter);
                        orfilter = {logic: "or", filters: []};
                    }
                });
                kgrid.dataSource.filter(andfilter);
            } else {
                kgrid.dataSource.filter({});
            }
        }
    });

    $('#clearSearch').click(function () {
        $('#application').val('');
        $('#searchInput').val('');
        $('.k-input').empty();
        $("#gridActions").data("kendoGrid").dataSource.filter({});
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() === 0) {
            $('.page-header').css('box-shadow', 'none');
        } else {
            $('.page-header').css('box-shadow', '2px 7px 14px #c7c8c7');
        }
    });
});