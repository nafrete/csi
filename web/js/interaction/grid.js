$(function () {

    'use strict';

    var url = $("#gridActions").attr('data-url'),
        wnd,
        detailsTemplate,
        total,
        dataSource,
        interactionAPI,
        buttonCancel = $('.cancelInteraction'),
        buttonArchive = $('.archiveInteraction');

    function InteractionAPI() {
        this.updateStatusInteraction = function (url, callback) {
            $.ajax({
                type: "PUT",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                success: function (data) {
                    callback(data);
                },
                error: function () {
                    var $cancelInteraction = $(".cancelInteraction"),
                        $archiveInteraction = $(".archiveInteraction");

                    $cancelInteraction.removeClass("disabled");
                    $cancelInteraction.html("Cancel");
                    $archiveInteraction.removeClass("disabled");
                    $archiveInteraction.html("Archive");
                }
            });
        };

        this.getActions = function (url, callback) {
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                success: function (data) {
                    callback(data);
                }
            });
        };
    }

    interactionAPI = new InteractionAPI();

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json"
            }
        },
        schema: {
            parse: function (response) {
                var instances = [];
                instances.push(response.instances);
                total = response.total;

                return instances[0];
            },
            total: function () {
                return total;
            },
            model: {
                id: "id",
                fields: {
                    application: {type: "string"},
                    userEmail: {type: "string"},
                    instance: {
                        interactionDefinitionId: {type: "string"},
                        externalParams: {type: "array"},
                        status: {
                            type: "string",
                            parse: function (value) {
                                return new Date(value.status);
                            }
                        },
                        userEmail: {
                            type: "string",
                            parse: function (value) {
                                return new Date(value.userEmail);
                            }
                        }
                    },
                    last_update_timestamp: {
                        type: "date",
                        parse: function (value) {
                            if (value === undefined || value === null) {
                                return null;
                            }
                            return new Date(value.sec * 1000);
                        }
                    },
                    creation_timestamp: {
                        type: "date",
                        parse: function (value) {
                            return new Date(value.sec * 1000);
                        }
                    }
                }
            }
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    });

    function showDetails(e) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        wnd.content(detailsTemplate(dataItem));
        wnd.center().open();
    }

    function statusFilter(element) {
        element.removeClass("k-textbox")
            .kendoDropDownList({
                dataSource: new kendo.data.DataSource({
                    data: [
                        {title: "PUBLISHED"},
                        {title: "CANCELLED"},
                        {title: "ARCHIVED"}
                    ]
                }),
                dataTextField: "title",
                dataValueField: "title"
            });
    }

    $("#gridActions").kendoGrid({
        dataSource: dataSource,
        groupable: false,
        reorderable: true,
        resizable: true,
        allowCopy: true,
        filterable: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        editable: "popup",
        columns: [
            {
                field: "id",
                title: "Id",
                filterable: {
                    cell: {
                        operator: "contains"
                    }
                }
            },
            {
                field: "application",
                title: "Application",
                filterable: {
                    cell: {
                        operator: "contains"
                    }
                },
                width: 200
            },
            {
                field: "instance.status",
                title: "Status",
                filterable: {
                    ui: statusFilter
                },
                width: 200
            },
            {
                field: "instance.userEmail",
                title: "From",
                filterable: {
                    cell: {
                        operator: "contains"
                    }
                }
            },
            {
                field: "last_update_timestamp",
                title: "Updated at",
                format: "{0:d/MM/yyyy}",
                filterable: false
            },
            {
                command: {
                    text: "details",
                    click: showDetails
                },
                title: " ",
                width: "180px"
            }
        ]
    }).data("kendoGrid");

    function hideErrorMessages() {
        $('.errorSystem').addClass('hide');
    }

    function initializeButtonCancel() {
        buttonCancel.removeClass('disabled');
        buttonCancel.text('Cancel the action');
    }

    function initializeButtonArchive() {
        buttonArchive.removeClass('disabled');
        buttonArchive.text('Archive the action');
    }

    function callbackUpdateStatus(data) {
        initializeButtonCancel();
        initializeButtonArchive();
        if (data === 'error') {
            $('.errorSystem').removeClass('hide');
        } else {
            $("#details").data("kendoWindow").close();
            $('#gridActions').data('kendoGrid').dataSource.read();
        }
    }

    function onOpen() {
        $('.cancelInteraction').click(function () {
            hideErrorMessages();
            var idCurrentElement = $(this).parent().parent().find('.idAction').attr('data-id'),
                urlApi = $(this).attr('data-url') + "update/" + idCurrentElement + '/CANCELLED';

            $(this).addClass("disabled");
            $(this).html("Cancelling...");
            interactionAPI.updateStatusInteraction(urlApi, callbackUpdateStatus);
        });

        $('.archiveInteraction').click(function () {
            hideErrorMessages();
            var idCurrentElement = $(this).parent().parent().find('.idAction').attr('data-id'),
                urlApi = $(this).attr('data-url') + "update/" + idCurrentElement + '/ARCHIVED';

            $(this).addClass("disabled");
            $(this).html("Archiving...");
            interactionAPI.updateStatusInteraction(urlApi, callbackUpdateStatus);
        });
    }

    wnd = $("#details")
        .kendoWindow({
            title: "Actions details",
            modal: true,
            visible: false,
            resizable: true,
            width: 500,
            open: onOpen
        }).data("kendoWindow");

    detailsTemplate = kendo.template($("#template").html());

    function scrollShawdow() {
        $(window).scroll(function () {
            if ($(window).scrollTop() === 0) {
                $('.page-header').css('box-shadow', 'none');
            } else {
                $('.page-header').css('box-shadow', '2px 7px 14px #c7c8c7');
            }
        });
    }

    scrollShawdow();

    $("#searchInput").keyup(function (e) {
        if (e.which === 13) {
            e.preventDefault();

            var selectedArray,
                orfilter,
                andfilter,
                selecteditem = $('#searchInput').val(),
                kgrid = $("#gridActions").data("kendoGrid");

            selecteditem = selecteditem.toUpperCase();
            selectedArray = selecteditem.split(" ");

            if (selecteditem) {
                orfilter = {logic: "or", filters: []};
                andfilter = {logic: "and", filters: []};
                $.each(selectedArray, function (i, v1) {
                    if (v1.trim() !== "") {
                        orfilter.filters.push(
                            {
                                field: "application",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "instance.status",
                                operator: "contains",
                                value: v1
                            },
                            {
                                field: "instance.userEmail",
                                operator: "contains",
                                value: v1
                            }
                        );
                        andfilter.filters.push(orfilter);
                        orfilter = {logic: "or", filters: []};
                    }
                });
                kgrid.dataSource.filter(andfilter);
            } else {
                kgrid.dataSource.filter({});
            }
        }
    });
});