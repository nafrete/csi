$(function () {

    'use strict';

    function InteractionAPI() {

        var contentType = 'application/json',
            dataTypeHTML = 'html';

        this.saveInteraction = function (method, data, url, callback) {
            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify({
                    interaction: data
                }),
                contentType: contentType,
                dataType: dataTypeHTML,
                cache: false,
                success: function (data, status) {
                    callback(data, status);
                },
                error: function (result, status, error) {
                    callback(error, result.status);
                }
            });
        };

        this.deleteInteraction = function (url, callback) {
            $.ajax({
                type: "DELETE",
                url: url,
                contentType: contentType,
                dataType: dataTypeHTML,
                cache: false,
                success: function (data, status) {
                    callback(data, status);
                },
                error: function (result, status, error) {
                    callback(error, result.status);
                }
            });
        };
    }

    $(".interactionDefContent").interactionDefinitionPrototype(new InteractionAPI());
});