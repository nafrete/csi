(function ($) {

    'use strict';

    $.fn.interactionDefinitionPrototype = function (interaction_API) {

        var interactionAPI = interaction_API,
            container = this,
            errorMessage = $('.errorSystem'),
            entryTable = container.find('.table-responsive tbody tr'),
            tableInteractions = container.find('.table-responsive'),
            buttonSubmit = container.find("#createInteraction"),
            buttonUpdate = container.find('#updateInteraction'),
            buttonDelete = container.find('#deleteInteraction'),
            buttonsAction = container.find('.buttonsActions div button'),
            formInteraction = container.find('#formNewInteraction'),
            $searchInput = $('#searchInput');

        function hideErrorMessages() {
            formInteraction.find('.errorForm').addClass('hide');
            errorMessage.addClass('hide');
        }

        function initializeButtonsActions() {
            buttonSubmit.removeClass('hide');
            buttonsAction.parent().addClass('hide');
            buttonsAction.attr('data-id', '');
        }

        function initializeChoices(elementParent) {
            console.log(elementParent.find('.typeInput').val());
            if (elementParent.find('.typeInput').val() !== 'checkbox' && elementParent.find('.typeInput').val() !== 'radio') {
                elementParent.find('.isMultipleChoices').addClass('hide');
                elementParent.find('.isMultipleChoicesTitle').addClass('hide');
            } else {
                elementParent.find('.isMultipleChoices').removeClass('hide');
                elementParent.find('.isMultipleChoicesTitle').removeClass('hide');
            }
        }

        function enableActionRemoveChoice() {
            $('.choice i').click(function () {
                $(this).parent().remove();
            });
        }

        function enableActionChangeTypeOption() {
            container.find('.typeInput').on('change', function () {
                initializeChoices($(this).parent().parent());
            });
        }

        function enableAddChoiceButton() {
            $('.addChoice i').unbind('click');
            container.find('.addChoice i').click(function () {
                var cloneElement = $(".choices .choice").last().clone(),
                    elementParent = $(this).parent().parent().parent().find('.choices');

                $(cloneElement).find('input').val('');
                $(cloneElement).find('i').removeClass('hide');
                cloneElement.appendTo(elementParent);
                enableActionRemoveChoice();
            });
        }

        function removeOptionsAdded() {
            $(".contentButtons .contentDefinitionOptions").each(function () {
                if ($(this).find('.numeroPosition').text() !== '1') {
                    $(this).remove();
                }
            });
        }

        function removeFieldsNewButton() {
            $('.deleteButton').click(function () {
                $(this).parent().parent().remove();
            });
        }

        function checkTypeInteraction() {
            if (container.find('#_type').val() === 'interaction') {
                container.find('.isInteraction').removeClass('hide');
            } else {
                container.find('.isInteraction').addClass('hide');
            }
        }

        function checkTypeChoice() {
            var fieldChoice = container.find('.choices .choice').first().clone();
            container.find('.choices .choice').remove();
            container.find('.choices').html(fieldChoice);
            initializeChoices(container);
        }

        function getMultipleChoices(elementParent) {
            var elementRef = elementParent.find('.choices .choice'),
                choices = [];

            $(elementRef).each(function () {
                if ($(this).find('input').val().trim().length > 0) {
                    choices.push($(this).find('input').val());
                }
            });

            return choices;
        }

        function createObjectMultipleChoices(elementRef) {
            return {
                externalId: elementRef.find('.externalIdInput').val(),
                type: elementRef.find('.typeInput').val(),
                choices: getMultipleChoices(elementRef),
                css: elementRef.find('.cssStyleInput').val(),
                position: elementRef.find('.positionInput').val(),
                label: [
                    {
                        language: container.find('#_language').val(),
                        text: elementRef.find('.textInput').val()
                    }
                ],
                feedbackResource: [
                    {
                        language: container.find('#_language').val(),
                        text: elementRef.find('.textFeedbackInput').val()
                    }
                ]
            };
        }

        function createObjectButtons(elementRef) {
            return {
                externalId: elementRef.find('.externalIdInput').val(),
                type: elementRef.find('.typeInput').val(),
                css: elementRef.find('.cssStyleInput').val(),
                position: elementRef.find('.positionInput').val(),
                label: [
                    {
                        language: container.find('#_language').val(),
                        text: elementRef.find('.textInput').val()
                    }
                ],
                feedbackResource: [
                    {
                        language: container.find('#_language').val(),
                        text: elementRef.find('.textFeedbackInput').val()
                    }
                ]
            };
        }

        function createObjectOption() {
            var options = [],
                elementRef = $("#formNewInteraction").find(".contentButtons .contentDefinitionOptions");

            $(elementRef).each(function () {
                var option = {};

                if ($(this).find('.typeInput').val() === 'checkbox' || $(this).find('.typeInput').val() === 'radio') {
                    option = createObjectMultipleChoices($(this));
                } else {
                    option = createObjectButtons($(this));
                }

                if (option.type.length > 0 && option.css.length > 0 && option.position.length > 0) {
                    options.push(option);
                }
            });

            return options;
        }

        function createInteractionDefinitionObject() {
            return {
                type: container.find('#_type').val().trim(),
                application: container.find('#_application').val().trim(),
                description: container.find('#_description').val().trim(),
                alreadyExecutedBody: {
                    language: container.find('#_language').val(),
                    text: container.find('#_alreadyExecutedBody').val().trim()
                },
                callbackRestService: container.find('#_callbackRestService').val().trim(),
                options: createObjectOption()
            };
        }

        function completeTheForm(elementParent) {
            container.find('.idInteraction').text(elementParent.attr('data-id'));
            container.find('#_application').val(elementParent.find('.interaction_application span').attr('data-id'));
            container.find('#_type').val(elementParent.find('.interaction_type').text().toLowerCase());
            container.find('#_alreadyExecutedBody').val(elementParent.find('.interaction_alreadyExecutedBody').text());
            container.find('#_callbackRestService').val(elementParent.find('.interaction_callbackRestService').text());
            container.find('#_description').val(elementParent.find('.interaction_description').text());
            container.find('#_language').val(elementParent.find('.interaction_language').text());
        }

        function bindFormUpdate(elementParent) {
            removeOptionsAdded();
            completeTheForm(elementParent);

            var count = 1;

            elementParent.find('.resourceOption').each(function () {
                var element = formInteraction.find('.contentButtons .contentDefinitionOptions').last(),
                    that = this,
                    fieldChoice = $(element).find('.choices .choice').first().clone();

                $(element).find('.numeroPosition').text(count);
                $(element).find('input').val('');
                $(element).find('.choices .choice').remove();
                fieldChoice.appendTo($(element).find('.isMultipleChoices'));

                $(element).each(function () {
                    var fields = ['cssStyle', 'externalId', 'position', 'text', 'textFeedback'],
                        i;

                    for (i = 0; i < fields.length; i++) {
                        $(this).find('.' + fields[i] + 'Input').val($(that).find('.interaction_' + fields[i]).text());
                    }
                    $(this).find('.typeInput').val($(that).find('.interaction_typeOption').text());

                    var self = this;

                    if ($(this).find('.typeInput').val() === 'checkbox' || $(this).find('.typeInput').val() === 'radio') {
                        $(that).find('.choicesAvailable').each(function () {
                            var elementCloned = $(self).find('.isMultipleChoices div.choice').last().clone(),
                                elementParentChoice = $(self).find('.isMultipleChoices div.choice').last().parent();

                            $(elementCloned).find('input').val($(this).text());
                            $(elementCloned).find('i').removeClass('hide');
                            $(elementCloned).appendTo(elementParentChoice);
                        });
                        $(self).find('.isMultipleChoices').removeClass('hide');
                        $(self).find('.isMultipleChoicesTitle').removeClass('hide');
                        $(self).find('.isMultipleChoices div.choice').first().remove();
                        $(self).find('.isMultipleChoices div.choice').first().find('i').addClass('hide');
                    } else {
                        $(this).find('.isMultipleChoices').addClass('hide');
                        $(this).find('.isMultipleChoicesTitle').addClass('hide');
                    }
                });

                if (count < elementParent.find('.resourceOption').length) {
                    var elementCloned = $('#formNewInteraction').find('.contentButtons .contentDefinitionOptions').last().clone(),
                        elementChoice = $(elementCloned).find('.choices .choice').first().clone();

                    $(elementCloned).find('.choices .choice').remove();
                    $(elementCloned).find('.choices').html(elementChoice);
                    elementCloned.appendTo('#formNewInteraction .contentButtons');
                    $(elementCloned).find('.deleteButton').removeClass('hide');
                    initializeChoices(elementCloned);
                    removeFieldsNewButton();
                }
                count = count + 1;
            });

            enableAddChoiceButton();
            enableActionChangeTypeOption();
            checkTypeInteraction();
            enableActionRemoveChoice();
        }

        function formUpdateAction() {
            entryTable.click(function () {
                hideErrorMessages();

                formInteraction.find('input').val('');
                entryTable.removeClass('rowSelected');

                $(this).addClass('rowSelected');
                buttonSubmit.addClass('hide');
                buttonsAction.parent().removeClass('hide');
                buttonsAction.attr('data-id', '');
                buttonsAction.attr('data-id', $(this).attr('data-id'));

                bindFormUpdate($(this));
            });
        }

        function isFormValid() {
            var fieldsToCheck = ["type", "application", "description", "callbackRestService", "language", "alreadyExecutedBody"],
                i;

            for (i = 0; i < fieldsToCheck.length; i++) {
                if (container.find('#_' + fieldsToCheck[i]).val().length <= 0 || container.find('#_' + fieldsToCheck[i]).val().length === null) {
                    return false;
                }
            }
            return true;
        }

        function initialize() {
            entryTable = $('.table-responsive tbody tr');
            formInteraction.find('input').val('');
            container.find('.idInteraction').text('-');
            removeOptionsAdded();
            initializeButtonsActions();
            formUpdateAction();
            hideErrorMessages();
            checkTypeInteraction();
            checkTypeChoice();
            enableAddChoiceButton();
            enableActionChangeTypeOption();
        }

        initialize();

        function updateDOM(data, status) {
            if (status === 400 || status === 404 || status === 500) {
                errorMessage.html(data).removeClass('hide');
                buttonUpdate.removeClass("disabled");
                buttonDelete.removeClass("disabled");
            } else {
                tableInteractions.html(data);
                initialize();
            }
        }

        function callbackPostInteraction(data, status) {
            buttonSubmit.html("Create new Interaction Definition").removeClass("disabled");
            updateDOM(data, status);
        }

        function callbackUpdateInteraction(data, status) {
            buttonUpdate.html("Save changes").removeClass("disabled");
            updateDOM(data, status);
        }

        function callbackDeleteInteraction(data, status) {
            buttonDelete.html("Delete").removeClass("disabled");
            updateDOM(data, status);
        }

        buttonDelete.click(function () {
            var url = $(this).attr('data-url') + 'delete/' + $(this).attr('data-id');
            $(this).html("Deleting...").addClass("disabled");
            interactionAPI.deleteInteraction(url, callbackDeleteInteraction);
        });

        buttonSubmit.click(function () {
            hideErrorMessages();
            if (isFormValid()) {
                var url = $(this).attr('data-url'),
                    interactionObject = createInteractionDefinitionObject();
                buttonSubmit.html("Adding").addClass("disabled");
                interactionAPI.saveInteraction('POST', interactionObject, url, callbackPostInteraction);
            } else {
                formInteraction.find('.errorForm').removeClass('hide');
            }
        });

        buttonUpdate.click(function () {
            hideErrorMessages();
            if (isFormValid()) {
                var url = $(this).attr('data-url') + 'update/' + $(this).attr('data-id'),
                    interactionObject = createInteractionDefinitionObject();
                buttonUpdate.html("Saving").addClass("disabled");
                interactionAPI.saveInteraction('PUT', interactionObject, url, callbackUpdateInteraction);
            } else {
                formInteraction.find('.errorForm').removeClass('hide');
            }
        });

        container.find('#cancelInteraction').click(function () {
            initialize();
        });

        container.find('#formNewInteraction .addNewButton').click(function () {
            var $formNewInteraction = $("#formNewInteraction"),
                position = $formNewInteraction.find(".contentButtons .contentDefinitionOptions").last().find('.numeroPosition').text(),
                cloneElement = $formNewInteraction.find(".contentButtons .contentDefinitionOptions").last().clone(),
                fieldChoice = $(cloneElement).find('.choices .choice').first().clone();

            $(cloneElement).find('input').val('');
            $(cloneElement).find('.deleteButton').removeClass('hide');
            $(cloneElement).find('.choices .choice').remove();
            $(cloneElement).find('.choices').html(fieldChoice);
            $(cloneElement).find('.languageInput').val($formNewInteraction.find(".contentButtons .contentDefinitionOptions").first().find('.languageInput').val());
            $(cloneElement).find('.numeroPosition').text(parseInt(position, 10) + 1);
            $(cloneElement).appendTo('#formNewInteraction .contentButtons');

            removeFieldsNewButton();
            enableAddChoiceButton();
            enableActionChangeTypeOption();
            initializeChoices(cloneElement);
        });

        $(window).scroll(function () {
            if ($(window).scrollTop() === 0) {
                container.find('ul.list-inline').css('box-shadow', '0px 0px 0px #ffffff');
            } else {
                container.find('ul.list-inline').css('box-shadow', '2px 7px 14px #e8e8e8');
            }
        });

        $('#_type').on('change', function () {
            checkTypeInteraction();
        });

        $searchInput.val('');
        $searchInput.keyup(function () {
            var value = $(this).val().toLowerCase();

            if (value.length > 0) {
                entryTable.each(function () {
                    var application = $(this).find('.interaction_application').text().toLowerCase().match(value),
                        author = $(this).find('.interaction_author').text().toLowerCase().match(value),
                        type = $(this).find('.interaction_type').text().toLowerCase().match(value),
                        createdAt = $(this).find('.interaction_created').text().toLowerCase().match(value);

                    if (application !== null || author !== null || type !== null || createdAt !== null) {
                        $(this).removeClass('hide');
                    } else {
                        $(this).addClass('hide');
                    }
                });
            } else {
                entryTable.removeClass('hide');
            }
        });
    };
})(jQuery);