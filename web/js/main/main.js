$(function () {

    'use strict';

    $('.language').removeClass('selected-language');

    if ($('body').attr('data-language') === "fr") {
        $('.language-fr').addClass('selected-language');
    } else {
        $('.language-en').addClass('selected-language');
    }
});