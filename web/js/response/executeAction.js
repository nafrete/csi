$(function () {

    'use strict';

    $("table.serviceNow").addClass('hide');
    $("table.serviceNow").clone().appendTo(".variablesSN");
    $(".variablesSN table.serviceNow").removeClass('hide');
    $(".bodyInstance table.serviceNow").remove();

    var options = [];

    function checkButton(optionButton, optionButtonToCompare) {
        if (optionButton.attr('data-external') === optionButtonToCompare.attr('data-external')) {
            return 1;
        }

        return 0;
    }

    function isChecked(inputCheckbox) {
        if (inputCheckbox.is(':checked')) {
            return 1;
        }

        return 0;
    }

    function getValuesOptionsButtons(optionButton) {
        $('#contentOptions .optionButton').each(function () {
            var option = {};

            if ($('#body').attr('data-application') === "service now") {
                if (checkButton(optionButton, $(this)) === 1) {
                    option = {
                        value: $(this).attr('data-external')
                    };
                    options.push(option);
                }
            } else {
                option = {
                    externalId: $(this).attr('data-external'),
                    value: checkButton(optionButton, $(this))
                };
                options.push(option);
            }
        });

        return options;
    }

    function getValuesOptionsComment() {
        $('#contentOptions .optionComment').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: $(this).find('textarea').val()
            };
            options.push(option);
        });

        return options;
    }

    function getValuesMultipleChoices(optionElement) {
        var values = [];
        optionElement.find('input').each(function () {
            var value = {
                choice: $(this).val(),
                value: isChecked($(this))
            };
            values.push(value);
        });

        return values;
    }

    function getValuesOptionsCheckbox() {
        $('#contentOptions .optionCheckbox').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: getValuesMultipleChoices($(this))
            };
            options.push(option);
        });

        return options;
    }

    function getValuesOptionsRadio() {
        $('#contentOptions .optionRadio').each(function () {
            var option = {
                externalId: $(this).attr('data-external'),
                value: getValuesMultipleChoices($(this))
            };
            options.push(option);
        });

        return options;
    }

    $('.optionButton button').click(function () {
        $('#modalLoading').show();
        var optionButton = $(this).parent();

        getValuesOptionsButtons(optionButton);
        getValuesOptionsComment();
        getValuesOptionsCheckbox();
        getValuesOptionsRadio();

        var data = {
            options: options
        };

        $.ajax({
            type: "GET",
            url: $('#body').attr('data-url'),
            data: data,
            contentType: "application/json",
            dataType: "json",
            cache: false,
            beforeSend: function () {
                $('.optionButton button').addClass('disabled');
            },
            success: function (data) {
                $('#modalLoading').hide();
                $('.optionButton button').removeClass('disabled');
                $('.optionButton button span').addClass('hide');
                $('#body').html(data.message);
            },
            error: function () {
                $('#modalLoading').hide();
                $('#body').html('The system is not available, please retry in few minutes.<br /> If the problem persist, contact the administrator.');
            }
        });
    });
});