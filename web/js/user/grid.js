$(function () {

    'use strict';

    var url = $("#gridUsers").attr('data-url'),
        wnd,
        detailsTemplate;

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json"
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    username: {type: "string"},
                    email: {type: "string"},
                    last_login: {type: "date"},
                    enabled: {
                        type: "boolean"
                    },
                    locked: {
                        type: "boolean"
                    },
                    expired: {
                        type: "string"
                    },
                    roles: {
                        type: "string"
                    },
                    credentials_expired: {
                        type: "boolean"
                    },
                    dn: {
                        type: "string"
                    }
                }
            }
        },
        pageSize: 10
    });

    function showDetails(e) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        wnd.content(detailsTemplate(dataItem));
        wnd.center().open();
    }

    $("#gridUsers").kendoGrid({
        dataSource: dataSource,
        groupable: true,
        reorderable: true,
        resizable: true,
        filterable: {
            extra: false,
        },
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        editable: "popup",
        columns: [
            {
                field: "username",
                title: "Username",
                filterable: {
                    cell: {
                        operator: "contains"
                    }
                },
                width: 250
            },
            {
                field: "email",
                title: "Email",
                width: 250
            },
            {
                field: "last_login",
                title: "Last login",
                format: "{0:d/MM/yyyy}",
                width: 250
            },
            {
                command: {
                    text: "details",
                    click: showDetails
                },
                title: " ",
                width: "180px"
            }
        ]
    }).data("kendoGrid");


    wnd = $("#details").kendoWindow({
        title: "User details",
        modal: true,
        visible: false,
        resizable: true,
        width: 900
    }).data("kendoWindow");

    detailsTemplate = kendo.template($("#template").html());

    function scrollShawdow() {
        $(window).scroll(function () {
            if ($(window).scrollTop() === 0) {
                $('.page-header').css('box-shadow', 'none');
            } else {
                $('.page-header').css('box-shadow', '2px 7px 14px #c7c8c7');
            }
        });
    }

    scrollShawdow();

    $("#searchInput").keyup(function () {
        var selecteditem = $('#searchInput').val();
        var kgrid = $("#gridUsers").data("kendoGrid");
        selecteditem = selecteditem.toUpperCase();
        var selectedArray = selecteditem.split(" ");
        if (selecteditem) {
            var orfilter = {logic: "or", filters: []};
            var andfilter = {logic: "and", filters: []};
            $.each(selectedArray, function (i, v) {
                if (v.trim() == "") {
                }
                else {
                    $.each(selectedArray, function (i, v1) {
                        if (v1.trim() == "") {
                        }
                        else {
                            orfilter.filters.push(
                                {
                                    field: "username",
                                    operator: "contains",
                                    value: v1
                                },
                                {
                                    field: "email",
                                    operator: "contains",
                                    value: v1
                                },
                                {
                                    field: "last_login",
                                    operator: "contains",
                                    value: v1
                                }
                            );
                            andfilter.filters.push(orfilter);
                            orfilter = {logic: "or", filters: []};
                        }
                    });
                }
            });
            kgrid.dataSource.filter(andfilter);
        }
        else {
            kgrid.dataSource.filter({});
        }
    });
});