(function () {

    'use strict';

    // Localize jQuery variable
    var jQuery;

    function main() {
        jQuery(document).ready(function ($) {
            //JS WIDGET
            var url = "https://msr-uat-cis.ubisoft.org/api/interactions/instances/user?take=10&skip=0&page=1&pageSize=10",
                total,
                interactionAPI,
                options = [],
                dataSource;

            addActionsButtonTemplate();
            addKendoTemplate();
            addModal();

            function checkButton(optionButton, optionButtonToCompare) {
                if (optionButton.attr('data-external') === optionButtonToCompare.attr('data-external')) {
                    return 1;
                }
                return 0;
            }

            function getValuesOptionsButtons(optionButton) {
                $('#contentOptions .optionButton').each(function () {
                    var option = {};

                    if ($('#body').attr('data-application') === "service now") {
                        if (checkButton(optionButton, $(this)) === 1) {
                            option = {
                                value: $(this).attr('data-external')
                            };
                            options.push(option);
                        }
                    } else {
                        option = {
                            externalId: $(this).attr('data-external'),
                            value: checkButton(optionButton, $(this))
                        };
                        options.push(option);
                    }
                });

                return options;
            }

            function getValuesOptionsComment() {
                $('#contentOptions .optionComment').each(function () {
                    var option = {
                        externalId: $(this).attr('data-external'),
                        value: $(this).find('textarea').val()
                    };
                    options.push(option);
                });

                return options;
            }

            function isChecked(inputCheckbox) {
                if (inputCheckbox.is(':checked')) {
                    return 1;
                }
                return 0;
            }

            function getValuesMultipleChoices(optionElement) {
                var values = [];
                optionElement.find('input').each(function () {
                    var value = {
                        choice: $(this).val(),
                        value: isChecked($(this))
                    };
                    values.push(value);
                });

                return values;
            }

            function getValuesOptionsCheckbox() {
                $('#contentOptions .optionCheckbox').each(function () {
                    var option = {
                        externalId: $(this).attr('data-external'),
                        value: getValuesMultipleChoices($(this))
                    };
                    options.push(option);
                });

                return options;
            }

            function getValuesOptionsRadio() {
                $('#contentOptions .optionRadio').each(function () {
                    var option = {
                        externalId: $(this).attr('data-external'),
                        value: getValuesMultipleChoices($(this))
                    };
                    options.push(option);
                });

                return options;
            }

            function executeAction() {
                $('.optionButton button').click(function () {
                    //$('#modalPreventAction').show();
                    $(this).find('span').removeClass('hide');
                    var optionButton = $(this).parent(),
                        urlApi = $('#body').attr('data-url'),
                        data;
                    options = [];

                    getValuesOptionsButtons(optionButton);
                    getValuesOptionsComment();
                    getValuesOptionsCheckbox();
                    getValuesOptionsRadio();

                    data = {
                        options: options
                    };

                    interactionAPI.postAction(urlApi, data);
                });
            }

            function InteractionAPI() {
                this.getTemplateUser = function (url) {
                    $.ajax({
                        type: "GET",
                        url: url,
                        contentType: "application/json",
                        dataType: "html",
                        xhrFields: {
                            withCredentials: true
                        },
                        cache: false,
                        beforeSend: function (xhr) {
                            $('#modalNotification .modal-body').html('<div>loading <i class="fa fa-spinner fa-pulse"></i></div>');
                        },
                        success: function (data) {
                            $('#modalNotification .modal-body').html(data);
                            $("table.serviceNow").addClass('hide');
                            $("table.serviceNow").clone().appendTo(".variablesSN");
                            $(".variablesSN table.serviceNow").removeClass('hide');
                            $(".bodyInstance table.serviceNow").remove();
                            executeAction();
                        }
                    });
                };

                this.getTemplateInstance = function (url) {
                    $.ajax({
                        type: "GET",
                        url: url,
                        contentType: "application/json",
                        dataType: "html",
                        xhrFields: {
                            withCredentials: true
                        },
                        cache: false,
                        beforeSend: function (xhr) {
                            $('.popover-content').html('<div>loading <i class="fa fa-spinner fa-pulse"></i></div>');
                        },
                        success: function (data) {
                            $('.popover-content').html(data);
                        }
                    });
                };

                this.postAction = function (url, data) {
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        contentType: "application/json",
                        dataType: "json",
                        xhrFields: {
                            withCredentials: true
                        },
                        cache: false,
                        beforeSend: function () {
                            $('.optionButton button').addClass('disabled');
                        },
                        success: function () {
                            $('.optionButton button').removeClass('disabled');
                            $('.optionButton button span').addClass('hide');
                            $('#modalNotification').modal('hide');
                            $('#gridActions').data('kendoGrid').dataSource.read();
                        },
                        error: function () {
                            $('.optionButton button').removeClass('disabled');
                            $('.optionButton button span').addClass('hide');
                            $('.optionButtonInline').parent().find('button').removeClass('disabled');
                            $('.optionButtonInline').parent().find('.loader').addClass('hide');
                            $('#modalNotification').modal('hide');
                            $('#modalErrorNotification').modal('show');
                        }
                    });
                };
            }

            interactionAPI = new InteractionAPI();

            dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        xhrFields: {
                            withCredentials: true
                        },
                        complete: function () {
                            $('[data-toggle="tooltip"]').tooltip();
                            $('[data-toggle="popover"]').popover();

                            var idCurrentElement,
                                urlApi;

                            $('.detail-popover').on('show.bs.popover', function () {
                                idCurrentElement = $(this).attr('data-id');
                                urlApi = 'https://msr-uat-cis.ubisoft.org/en/interactioninstance/' + idCurrentElement + '/overview';
                            });

                            $(".detail-popover").popover({
                                html: true,
                                content: function () {
                                    return interactionAPI.getTemplateInstance(urlApi);
                                },
                                title: function () {
                                    return "Overview";
                                },
                                container: "body"
                            });
                        }
                    }
                },
                schema: {
                    parse: function (response) {
                        var instances = [];
                        instances.push(response.instances);
                        total = response.total;

                        return instances[0];
                    },
                    total: function () {
                        return total;
                    },
                    model: {
                        id: "id",
                        fields: {
                            application: {type: "string"},
                            userEmail: {type: "string"},
                            priority: {
                                type: "string",
                                parse: function (value) {
                                    var priorityString = "normal";
                                    if (value === 1 || value === 2) {
                                        priorityString = "hight";
                                    }
                                    if (value === 4 || value === 5) {
                                        priorityString = "low";
                                    }
                                    return priorityString;
                                }
                            },
                            instance: {
                                interactionDefinitionId: {type: "string"},
                                externalParams: {type: "array"},
                                status: {
                                    type: "string",
                                    parse: function (value) {
                                        if (value.status === "PUBLISHED") {
                                            return "OPEN";
                                        }
                                        return value.status;
                                    }
                                },
                                userEmail: {
                                    type: "string",
                                    parse: function (value) {
                                        return value.userEmail;
                                    }
                                }
                            },
                            last_update_timestamp: {
                                type: "date",
                                parse: function (value) {
                                    if (value === undefined || value === null) {
                                        return null;
                                    }
                                    return new Date(value.sec * 1000);
                                }
                            },
                            creation_timestamp: {
                                type: "date",
                                parse: function (value) {
                                    return new Date(value.sec * 1000);
                                }
                            }
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            });

            $("#gridActions").kendoGrid({
                dataSource: dataSource,
                groupable: false,
                reorderable: true,
                resizable: true,
                allowCopy: true,
                toolbar: kendo.template($("#templateSearchApplication").html()),
                filterable: false,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                editable: "popup",
                columns: [
                    {
                        field: "application",
                        title: "Application",
                        attributes: {
                            "data-title": "Application"
                        },
                        filterable: {
                            cell: {
                                showOperators: false
                            }
                        },
                        template: '<span class="instanceId" data-id="#= id#" >#= application #</span>',
                        width: 150
                    },
                    {
                        field: "subject",
                        title: "Description",
                        attributes: {
                            "data-title": "Description"
                        },
                        filterable: false,
                        template: '<span class="detail-popover" data-id="#= id#" data-trigger="click hover focus"><i class="fa fa-info-circle infoInstance"></i></span> <span>#= subject #</span>',
                        width: 400
                    },
                    {
                        field: "last_update_timestamp",
                        title: "Sent Date (UTC)",
                        attributes: {
                            "data-title": "Sent Date"
                        },
                        format: "{0:d MMM yyyy H:mm}",
                        filterable: false,
                        width: 300
                    },
                    {
                        title: "Actions",
                        attributes: {
                            "data-title": "Actions"
                        },
                        template: kendo.template($("#command-template").html()),
                        width: 200
                    }
                ]
            }).data("kendoGrid");

            $("#application").kendoDropDownList({
                dataTextField: "application.name",
                dataValueField: "application.name",
                autoBind: false,
                dataSource: {
                    transport: {
                        read: {
                            url: "https://msr-uat-cis.ubisoft.org/api/applications",
                            dataType: "json",
                            xhrFields: {
                                withCredentials: true
                            }
                        }
                    }
                },
                change: function () {
                    var value = this.value(),
                        status;
                    if (value) {
                        status = $('.statusFilter ul li.statusSelected').text();
                        if (status) {
                            kendoFilterMultiple(value, status);
                        } else {
                            kendoFilterSimple("application", value);
                        }
                    } else {
                        $("#gridActions").data("kendoGrid").dataSource.filter({});
                    }
                }
            });

            function kendoFilterSimple(field, value) {
                $("#gridActions").data("kendoGrid").dataSource.filter(
                    {
                        field: field,
                        operator: "eq",
                        value: value
                    }
                );
            }

            function kendoFilterMultiple(applicationValue, statusValue) {
                $("#gridActions").data("kendoGrid").dataSource.filter(
                    [
                        {
                            field: "application",
                            operator: "eq",
                            value: applicationValue
                        },
                        {
                            field: "instance.status",
                            operator: "eq",
                            value: statusValue
                        }
                    ]
                );
            }

            $('#clearFilter').click(function () {
                $('#application').val('');
                $('#searchInput').val('');
                $('.k-input').empty();
                $("#gridActions").data("kendoGrid").dataSource.filter({});
            });

            $('#clearFilterResp').click(function () {
                $('#application').val('');
                $('#searchInput').val('');
                $('.k-input').empty();
                $("#gridActions").data("kendoGrid").dataSource.filter({});
            });

            $('.statusFilter ul li').click(function () {
                var value = $(this).attr('data-status');
                if (value) {
                    $('.statusFilter ul li').css('text-decoration', 'none');
                    $('.statusFilter ul li').removeClass('statusSelected');
                    $(this).css('text-decoration', 'underline');
                    $(this).addClass('statusSelected');
                    if ($("#application").val()) {
                        kendoFilterMultiple($("#application").val(), value);
                    } else {
                        kendoFilterSimple("instance.status", value);
                    }
                } else {
                    $("#gridActions").data("kendoGrid").dataSource.filter({});
                }
            });

            $("#searchInput").keyup(function (e) {
                if (e.which === 13) {
                    e.preventDefault();

                    var selectedArray,
                        orfilter,
                        andfilter,
                        selecteditem = $('#searchInput').val(),
                        kgrid = $("#gridActions").data("kendoGrid");

                    selecteditem = selecteditem.toUpperCase();
                    selectedArray = selecteditem.split(" ");

                    if (selecteditem) {
                        orfilter = {logic: "or", filters: []};
                        andfilter = {logic: "and", filters: []};
                        $.each(selectedArray, function (i, v1) {
                            if (v1.trim() !== "") {
                                orfilter.filters.push(
                                    {
                                        field: "instance.userEmail",
                                        operator: "contains",
                                        value: v1
                                    }
                                );
                                andfilter.filters.push(orfilter);
                                orfilter = {logic: "or", filters: []};
                            }
                        });
                        kgrid.dataSource.filter(andfilter);
                    } else {
                        kgrid.dataSource.filter({});
                    }
                }
            });

            $('#clearSearch').click(function () {
                $('#application').val('');
                $('#searchInput').val('');
                $('.k-input').empty();
                $("#gridActions").data("kendoGrid").dataSource.filter({});
            });

            $("#gridActions").on("click", "tbody tr", function (e) {
                if (($(e.target).is('button.inlineAction.btn') || $(e.target).is('i')) && !$(e.target).is('i.infoInstance')) {
                    //$('#modalPreventAction').show();
                    $(e.target).parent().parent().find('button').addClass('disabled');

                    var data,
                        optionButton,
                        urlPost;

                    if ($(e.target).is('i')) {
                        $(e.target).parent().parent().find('.loader').removeClass('hide');
                        optionButton = $(e.target).parent().parent();
                    } else {
                        $(e.target).find('.loader').removeClass('hide');
                        optionButton = $(e.target);
                    }

                    urlPost = optionButton.parent().attr("data-url");
                    options = [];

                    optionButton.parent().parent().find('span button').each(function () {
                        if ($(this).attr('data-application').toLowerCase() === "service now") {
                            if (checkButton(optionButton, $(this)) === 1) {
                                var option = {
                                    value: $(this).attr('data-external')
                                };
                                options.push(option);
                            }
                        } else {
                            var option = {
                                externalId: $(this).attr('data-external'),
                                value: checkButton(optionButton, $(this))
                            };
                            options.push(option);
                        }
                    });

                    data = {
                        options: options
                    };

                    interactionAPI.postAction(urlPost, data);
                } else {
                    var idCurrentElement = $(this).find('.instanceId').attr('data-id'),
                        urlApi = "https://msr-uat-cis.ubisoft.org/api/instances/" + idCurrentElement + "/view";

                    $('#modalNotification').modal('show');
                    interactionAPI.getTemplateUser(urlApi);
                }
            });

            $('#modalNotification').on('hide.bs.modal', function () {
                $('#modalNotification .modal-body').empty();
            });
            // ! JS WIDGET
        });
    }

    // Called once jQuery has loaded
    // Restore $ and window.jQuery to their previous values and store the new jQuery in our local jQuery variable
    function scriptLoadHandler() {
        console.log("Kendo loaded");
        jQuery = window.jQuery.noConflict(true);

        // Load CSS
        var cssLinkGrid = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://msr-uat-cis.ubisoft.org/css/grid.css"
            }),
            cssLinkHomepage = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://msr-uat-cis.ubisoft.org/css/homepage.css"
            }),
            cssLinkMain = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://msr-uat-cis.ubisoft.org/css/main.css"
            }),
            cssLinkIcon = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
            }),
            cssLinkBootstrap = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
            }),
            cssLinkKendo1 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.common-material.min.css"
            }),
            cssLinkKendo2 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.rtl.min.css"
            }),
            cssLinkKendo3 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.material.min.css"
            }),
            cssLinkKendo4 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.material.mobile.min.css"
            }),
            cssLinkKendo5 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.dataviz.min.css"
            }),
            cssLinkKendo6 = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/styles/kendo.dataviz.material.min.css"
            }),
            cssLinkKendoFont = jQuery("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,500italic,400italic,300italic,100italic|Oswald:400,700,300"
            });

        cssLinkKendo1.appendTo('head');
        cssLinkKendo2.appendTo('head');
        cssLinkKendo3.appendTo('head');
        cssLinkKendo4.appendTo('head');
        cssLinkKendo5.appendTo('head');
        cssLinkKendo6.appendTo('head');
        cssLinkIcon.appendTo('head');
        cssLinkBootstrap.appendTo('head');
        cssLinkMain.appendTo('head');
        cssLinkHomepage.appendTo('head');
        cssLinkGrid.appendTo('head');
        cssLinkKendoFont.appendTo('head');

        main();
    }

    //Load jQuery if not present
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.11.2') {
        var scriptTag = document.createElement('script');
        scriptTag.setAttribute("type", "text/javascript");
        scriptTag.setAttribute("src", "https://code.jquery.com/jquery-1.11.3.min.js");

        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(scriptTag);

        scriptTag.onload = function () {
            addResources();
        };
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        addResources();
    }

    function addResources() {
        var scriptTagBootstrap = document.createElement('script');
        scriptTagBootstrap.setAttribute("type", "text/javascript");
        scriptTagBootstrap.setAttribute("src", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js");

        var scriptTagKendoWeb = document.createElement('script');
        scriptTagKendoWeb.setAttribute("type", "text/javascript");
        scriptTagKendoWeb.setAttribute("src", "https://da7xgjtj801h2.cloudfront.net/2014.3.1411/js/kendo.all.min.js");

        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(scriptTagBootstrap);
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(scriptTagKendoWeb);

        scriptTagBootstrap.onload = function () {
            console.log("bootstrap loaded");
            scriptTagKendoWeb.onload = scriptLoadHandler;
        }
    }

    function addModal() {
        var modalPreventAction = '<div id="modalPreventAction"></div>';

        var modalErrorNotification = '<div class="modal fade" id="modalErrorNotification" tabindex="-1" role="dialog" aria-labelledby="modalErrorNotificationLabel" aria-hidden="true">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<h4 class="modal-title" id="modalNotificationLabel" style="color:#e74c3c;">Error</h4></div>' +
            '<div class="modal-body">The system is not available, please retry in few minutes.<br /> If the problem persist, contact the administrator.</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
            '</div></div></div></div>';

        var modalNotification = '<div class="modal fade" id="modalNotification" tabindex="-1" role="dialog" aria-labelledby="modalNotificationLabel" aria-hidden="true" data-backdrop="static">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<div class="modal-body"></div>' +
            '</div></div></div>';

        jQuery(modalPreventAction).insertAfter("#gridActions");
        jQuery(modalErrorNotification).insertAfter("#gridActions");
        jQuery(modalNotification).insertAfter("#gridActions");
    }

    function addKendoTemplate() {
        var templateSearchApplication = '<script type="text/x-kendo-template" id="templateSearchApplication">' +
            '<div id="searchItem" class="searchToolbar">' +
            '<div class="row searchItem searchInteraction">' +
            '<div class="form-group">' +
            '<div class="input-group">' +
            '<div class="input-group-addon"><i class="fa fa-search"></i></div>' +
            '<input type="text" class="form-control" id="searchInput" placeholder="Search an instance by keywords...">' +
            '<div class="input-group-addon"><button class="btn btn-default" id="clearSearch"><i class="fa fa-times"></i></button></div>' +
            '</div></div></div></div>' +
            '<div class="toolbar">' +
            '<label class="application-label" for="application">Application </label>' +
            '<input type="search" id="application" style="width: 150px"/>' +
            '<button class="btn btn-default" id="clearFilter"><i class="fa fa-times"></i></button>' +
            '<div class="statusFilter">' +
            '<span class="status-label" for="status">Status:</span>' +
            '<ul class="list-inline list-unstyled">' +
            '<li class="status" data-status="all">All</li>' +
            '<li>|</li>' +
            '<li class="status" data-status="published">Pending Approval</li>' +
            '<li>|</li>' +
            '<li class="status" data-status="executed">Executed</li></ul> </div>' +
            '<button class="btn btn-default" id="clearFilterResp">Clear filter</button>' +
            '</div>' +
            '</script>' +
            '</script>';
        jQuery(templateSearchApplication).insertAfter("#gridActions");
    }

    function addActionsButtonTemplate() {
        var commandTemplate = '<script id="command-template" type="text/x-kendo-template">' +
            '#var count = 0; if (instance.status === "PUBLISHED") {' +
            "for (var i = 0,len=interaction.options.length; i < len; i++) { if (interaction.options[i]['type'] === 'button' && count < 2) { #" +
            '<span class="optionButtonInline _option" id="#= i #" data-url="https://msr-uat-cis.ubisoft.org/api/executes/#= id #">' +
            '<button class="inlineAction btn #= interaction.options[i]["css"] #" data-external="#= interaction.options[i]["externalId"] #" data-toggle="tooltip" data-placement="left" title="#= interaction.options[i]["label"][0]["text"] #" data-application="#= application #">' +
            '#if(interaction.options[i]["label"][0]["text"] === "Approve") { #' +
            '<span><i class="fa fa-check"></i></span>' +
            '<span class="hide loader"><i class="fa fa-spinner fa-spin"></i></span>' +
            '#} else if(interaction.options[i]["label"][0]["text"] === "Reject") { #' +
            '<span><i class="fa fa-times"></i></span>' +
            '<span class="hide loader"><i class="fa fa-spinner fa-spin"></i></span>#}else { #' +
            '<span>#= interaction.options[i]["label"][0]["text"] #</span>' +
            '<span class="hide loader"><i class="fa fa-spinner fa-spin"></i></span>#}#</button></span>' +
            "# count++;}else {if(interaction.options[i]['type'] === 'button' && count >= 2) { #" +
            '<button class="btn btn-info" data-toggle="tooltip" data-placement="left" title="See more">' +
            '<i class="fa fa-plus" data-toggle="modal" data-target="\\#modalNotification" ></i></button>' +
            '# }}}} else { # <span style="font-size:10px;color:gray;">#= instance.status #</span> # } #' +
            '</script>';
        jQuery(commandTemplate).insertAfter("#gridActions");
    }
})();
