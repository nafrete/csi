$(function () {

    'use strict';

    var url = "https://msr-uat-cis/api/interactions/instances/user?take=10&skip=0&page=1&pageSize=10",
        total,
        interactionAPI,
        dataSource;

    function InteractionAPI() {
        this.getTemplateUser = function (url) {
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                success: function (data) {
                    $('#modalNotification .modal-body').html(data);
                    $("table.serviceNow").addClass('hide');
                    $("table.serviceNow").clone().appendTo(".variablesSN");
                    $(".variablesSN table.serviceNow").removeClass('hide');
                    $(".bodyInstance table.serviceNow").remove();
                }
            });
        };

        this.getTemplateInstance = function (url) {
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "html",
                cache: false,
                success: function (data) {
                    $('.popover-content').html(data);
                }
            });
        };

        this.postAction = function (url, data) {
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                contentType: "application/json",
                dataType: "json",
                cache: false,
                beforeSend: function () {
                    $('.optionButton button').addClass('disabled');
                },
                success: function () {
                    $('.optionButton button').removeClass('disabled');
                    $('.optionButton button span').addClass('hide');
                    $('#modalNotification').modal('hide');
                    $('#gridActions').data('kendoGrid').dataSource.read();
                },
                error: function () {
                    $('.optionButton button').removeClass('disabled');
                    $('.optionButton button span').addClass('hide');
                    $('.optionButtonInline').parent().find('button').removeClass('disabled');
                    $('.optionButtonInline').parent().find('.loader').addClass('hide');
                    $('#modalNotification').modal('hide');
                    $('#modalErrorNotification').modal('show');
                }
            });
        };
    }

    interactionAPI = new InteractionAPI();

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                },
                complete: function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('[data-toggle="popover"]').popover();

                    var idCurrentElement,
                        urlApi;

                    $('.detail-popover').on('show.bs.popover', function () {
                        idCurrentElement = $(this).attr('data-id');
                        urlApi = 'https://msr-uat-cis.ubisoft.org/en/interactiondefinition/en/interactioninstance/' + idCurrentElement + '/overview';
                    });

                    $(".detail-popover").popover({
                        html : true,
                        content: function () {
                            return interactionAPI.getTemplateInstance(urlApi);
                        },
                        title: function () {
                            return "Overview";
                        },
                        container: "body"
                    });
                }
            }
        },
        schema: {
            parse: function (response) {
                var instances = [];
                instances.push(response.instances);
                total = response.total;

                return instances[0];
            },
            total: function () {
                return total;
            },
            model: {
                id: "id",
                fields: {
                    application: {type: "string"},
                    userEmail: {type: "string"},
                    priority: {
                        type: "string",
                        parse: function (value) {
                            var priorityString = "normal";
                            if (value === 1 || value === 2) {
                                priorityString = "hight";
                            }
                            if (value === 4 || value === 5) {
                                priorityString = "low";
                            }
                            return priorityString;
                        }
                    },
                    instance: {
                        interactionDefinitionId: {type: "string"},
                        externalParams: {type: "array"},
                        status: {
                            type: "string",
                            parse: function (value) {
                                if (value.status === "PUBLISHED") {
                                    return "OPEN";
                                }
                                return value.status;
                            }
                        },
                        userEmail: {
                            type: "string",
                            parse: function (value) {
                                return value.userEmail;
                            }
                        }
                    },
                    last_update_timestamp: {
                        type: "date",
                        parse: function (value) {
                            if (value === undefined || value === null) {
                                return null;
                            }
                            return new Date(value.sec * 1000);
                        }
                    },
                    creation_timestamp: {
                        type: "date",
                        parse: function (value) {
                            return new Date(value.sec * 1000);
                        }
                    }
                }
            }
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    });

    $("#gridActions").kendoGrid({
        dataSource: dataSource,
        groupable: false,
        reorderable: true,
        resizable: true,
        allowCopy: true,
        //toolbar: kendo.template($("#templateSearchApplication").html()),
        filterable: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        editable: "popup",
        columns: [
            {
                field: "application",
                title: "Application",
                attributes: {
                    "data-title": "Application"
                },
                filterable: {
                    cell: {
                        showOperators: false
                    }
                },
                template: '<span class="instanceId" data-id="#= id#" >#= application #</span>',
                width: 150
            },
            {
                field: "subject",
                title: "Description",
                attributes: {
                    "data-title": "Description"
                },
                filterable: false,
                template: '<span class="detail-popover" data-id="#= id#" data-trigger="click hover focus"><i class="fa fa-info-circle infoInstance"></i></span> <span>#= subject #</span>',
                width: 400
            },
            {
                field: "last_update_timestamp",
                title: "Sent Date (UTC)",
                attributes: {
                    "data-title": "Sent Date"
                },
                format: "{0:d MMM yyyy H:mm}",
                filterable: false,
                width: 300
            },
            {
                title: "Actions",
                attributes: {
                    "data-title": "Actions"
                },
                //template: kendo.template($("#command-template").html()),
                width: 200
            }
        ]
    }).data("kendoGrid");

    console.log("js is running");
});